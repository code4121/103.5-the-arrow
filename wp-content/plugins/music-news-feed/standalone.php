<?php

include_once('simplehtmldom_1_5/simple_html_dom.php');

function load_html_page($link) {
	
	$html = file_get_html($link);
	
	foreach ($html->find('a[name]') as $id) {
	
	    $ids[] = str_replace('news_','',$id->name);
	
	};
	
	foreach($ids as $id){
		
		$find_excerpt = 'div[id=summary_' . $id . ']';
	
		$find_body = 'div[id=body_' . $id . ']';
		
		foreach($html->find($find_excerpt) as $excerpt){
	
			$excerpts[] = strip_tags($excerpt);
	
		}
		
		foreach($html->find($find_body) as $body){
	
			$bodies[] = strip_tags($body);
	
		}
	
	}
	
	foreach ($html->find('div.newsItem div.newsHed') as $title) {
	
	    $titles[] = strip_tags($title);
	
	};
	
	foreach ($html->find('div.newsItem div.newsHed img[src]') as $img) {
            
            $images[] = $img->src;
	
	};
	
	$post = format_array($ids, $images, $titles, $excerpts, $bodies);
	
	$verification = insert_post($post,$images);
		
}

function format_array($ids,$images,$titles,$excerpts,$bodies){
	
	for($i=0;$i<sizeof($images);$i++){
		
		$h[$i]['post_title'] = $titles[$i];
		$h[$i]['post_content'] = $bodies[$i];
		$h[$i]['post_status'] = 'publish';
		$h[$i]['post_excerpt'] = $excerpts[$i];
		$h[$i]['post_category'] = '4';
		$h[$i]['post_author'] = '1';
                $h[$i]['post_image'] = $images[$i];
	
	}
	
	print_r($h);
	
	return $h;
	
}

function insert_post($post,$images){
	
	for($i=0;$i<sizeof($images);$i++){
		
	 	$post_id = wp_insert_post( $post[$i] );
		
		echo "POST CREATED: " . $post_id;
	
		$attachment_id = insert_image( $post_id,$images[$i] );
		
		echo " | ATTACHMENT ADDED: " . $attachment_id . "<br>";
		
	}
	 	
}

function insert_image($post_id,$image){
	
	//upload=======================================
	
	$downloaded_file = file_get_contents($image);

	if($downloaded_file != ""){	
		// These files need to be included as dependencies when on the front end.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );
		
		$attachment_id = media_handle_upload( $downloaded_file, $post_id );
		
		if ( is_wp_error( $attachment_id ) ) {
			echo "There was an error uploading the image.<br>";
		} else {
			echo "The image was uploaded successfully!<br>";
		}
	
	} else {
	
		echo "The security check failed, maybe show the user an error.<br>";
	}
	//upload=======================================
	return $attachment_id;
}

function music_news() {
	
	load_html_page('http://www.rockdaily.com/cps/today.html?call=KRSP');
	
}

music_news();

?>