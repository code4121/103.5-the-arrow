<?php
/**
 * @package Music_News
 * @version 3.0
 */
/*
Plugin Name: Music News Feed
Plugin URI: 
Description: A plugin that will scrape a page provided by Triton Digital Media for our music news. This plugin is very specifically coded for this page and will not work on other "feeds".
Author: Doug Bailey
Version: 3.0
Author URI: http://bonneville.com
*/

include_once('simplehtmldom_1_5/simple_html_dom.php');

function load_html_page($options) {
	
	$link = $options['music_news_url'];
	
	if(!empty($options['music_news_filter'])){
	
		$filter = explode(",",$options['music_news_filter']);
	
	}else{
		
		$filter = array();
		
	}
	
	//echo "<!--FILTERS "; var_dump($filter); echo "-->";
	
	$html = file_get_html($link);
	
	foreach ($html->find('a[name]') as $id) {
	
	    $ids[] = str_replace('news_','',$id->name);
	
	};
	
	foreach($ids as $id){
		
		$find_excerpt = 'div[id=summary_' . $id . ']';
	
		$find_body = 'div[id=body_' . $id . ']';
		
		foreach($html->find($find_excerpt) as $excerpt){
	
			$excerpts[] = strip_tags($excerpt);
	
		}
		
		foreach($html->find($find_body) as $body){
	
			$bodies[] = strip_tags($body);
	
		}
	
	}
	
	foreach ($html->find('div.newsItem div.newsHed') as $title) {
	
	    $titles[] = strip_tags($title);
	
	};
	
	foreach ($html->find('div.newsItem div.newsHed img[src]') as $img) {
            
            $images[] = $img->src;
	
	};
	
	$post = format_array($ids, $images, $titles, $excerpts, $bodies, $filter);
	
        $verification = insert_post($post,$images);
		
}

function filter_content($filters,$text){
	
	foreach ($filters as $filter) {
//you must use !== to check if the result from strpos() is of the right type

	if(stripos($text, $filter) !== false)

		return false;

	}

	return true;

}

function format_array($ids,$images,$titles,$excerpts,$bodies,$filter){
	
	for($i=0;$i<sizeof($images);$i++){
		
		if(filter_content($filter,$excerpts[$i])){
		
			$h[$i]['post_title'] = $titles[$i];
			$h[$i]['post_content'] = $bodies[$i];
			$h[$i]['post_status'] = 'publish';
			$h[$i]['post_excerpt'] = $excerpts[$i];
			$h[$i]['post_category'] = array('4');
			$h[$i]['post_author'] = '1';
			$h[$i]['post_image'] = $images[$i];
		
		}else{
			
			$log = plugin_dir_path( __FILE__ ) . "rejected.log";
			file_put_contents($log, $titles[$i], FILE_APPEND | LOCK_EX);
			
		}
	
	}
	
	return $h;
	
}

function new_post_exists($post){
	
	$slug = 'name=' . sanitize_title($post['post_title']);
	
	$results = new WP_Query($slug);
	
	$count = $results->post_count;
	
	if($count >= 1){
		
		return true;
	
	}else{
	
		return false;
	
	}
	
}

function insert_post($post,$images){
	
	for($i=0;$i<sizeof($images);$i++){
	
		//$post_id = post_exists( sanitize_title($post[$i]['title']));
	
		//if(!$post_id){
		if(!new_post_exists($post[$i])){
		
			$post_id = wp_insert_post( $post[$i] );
		
			$attachment_id = insert_image( $post_id,$images[$i] );
		
		} 
		
	}
        
        return $post;
	 	
}

function insert_image($post_id,$image){
       
        
        $upload_dir = wp_upload_dir();
	
	$image_data = file_get_contents($image);

	$filename = basename($image);
	
	if(wp_mkdir_p($upload_dir['path'])) {
    
		$file = $upload_dir['path'] . '/' . $filename;
		
	} else {
		
		$file = $upload_dir['basedir'] . '/' . $filename;
		
	}
    
	if(!file_exists($file)){

		file_put_contents($file, $image_data);

	}

	$wp_filetype = wp_check_filetype($filename, null );
	
	$attachment = array(
		'post_mime_type' => $wp_filetype['type'],
		'post_title' => sanitize_file_name($filename),
		'post_content' => '',
		'post_status' => 'inherit');
	
	$attach_id = wp_insert_attachment( $attachment, $file, $post_id );

	require_once(ABSPATH . 'wp-admin/includes/image.php');

	$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

	wp_update_attachment_metadata( $attach_id, $attach_data );

	set_post_thumbnail( $post_id, $attach_id );

	return $attach_id;
}

function music_news() {
	
	$options = get_option('music_news_plugin_options');
	
	load_html_page($options);
	
}

//settings page
function music_news_options_page() {
?>
	<div>
		<h2>Triton KRSP/KSFI Music News Plugin</h2>
		This plugin should only be used by KRSP and KSFI to scrape a page produced by Triton Digital Media (with which we have a license to do so).
		<form action="options.php" method="post">
			<?php settings_fields('music_news_plugin_options'); ?>
			<?php do_settings_sections('plugin'); ?>
 			<input name="Submit" type="submit" value="<?php esc_attr_e('Save Changes'); ?>" />
		</form>
	</div>
 
<?php }

add_action('admin_init', 'plugin_admin_init');

function plugin_admin_init(){

	register_setting( 'music_news_plugin_options', 'music_news_plugin_options', 'music_news_plugin_options_validate' );

	add_settings_section('plugin_main', 'Main Settings', 'plugin_section_text', 'plugin');

	add_settings_field('music_news_plugin_url', 'Music News URL', 'plugin_setting_string', 'plugin', 'plugin_main');

}

function plugin_section_text() {

	echo '<p>Enter the URL in the first field and optionally add any terms you wish to filter out in the second field separated by a comma (i.e. Badword1,Badword2,...).</p>';

}

function plugin_setting_string() {

	$options = get_option('music_news_plugin_options');

	echo "<input id='plugin_text_string' name='music_news_plugin_options[music_news_url]' size='40' type='text' value='{$options['music_news_url']}' />";
	echo "<br><br><i>Filter out stories with the following words in the story: (comma delimited list)</i><br>";
	echo "<input id='plugin_text_string' name='music_news_plugin_options[music_news_filter]' size='40' type='text' value='{$options['music_news_filter']}' />";

}

function music_news_plugin_options_validate($input) {

	$newinput['music_news_url'] = trim($input['music_news_url']);
	
	if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $newinput['music_news_url'])) {
		
		$newinput['music_news_url'] = '';

	}

	if(!$input['music_news_filter']){
		
		$newinput['music_news_filter'] = "";
		
	}else{
		
		$newinput['music_news_filter'] = mysql_escape_string($input['music_news_filter']); 
		
	}
	
	return $newinput;
}

//add the configs page
add_action('admin_menu', 'music_news_admin_add_page');

function music_news_admin_add_page() {
	
	add_options_page('Music News Settings', 'Music News', 'manage_options', 'music_news', 'music_news_options_page');

}

register_activation_hook(__FILE__, 'music_news_activation');

add_action('music_news_hourly_event', 'music_news');

function music_news_activation() {
	wp_schedule_event(time(), 'hourly', 'music_news_hourly_event');
}

register_deactivation_hook(__FILE__, 'music_news_deactivation');

function music_news_deactivation() {
	wp_clear_scheduled_hook('music_news_hourly_event');
}

/*function do_this_hourly() {
	// do something every hour
}*/

//add_action('wp_loaded','music_news');

?>
