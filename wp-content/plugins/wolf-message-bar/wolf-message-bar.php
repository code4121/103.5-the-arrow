<?php
/**
 * Plugin Name: Wolf Message Bar
 * Plugin URI: http://www.wpwolf.com/plugin/wolf-message-bar
 * Description: Display a jquery message bar at the top of your page
 * Version: 1.1.4
 * Author: WpWolf
 * Author URI: http://wpwolf.com/about/
 * Requires at least: 3.5
 * Tested up to: 3.8.1
 *
 * Text Domain: wolf
 * Domain Path: /lang/
 *
 * @package WolfMessageBar
 * @author WpWolf
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Wolf_Message_Bar' ) ) {
/**
 * Main Wolf_Message_Bar Class
 *
 * Contains the main functions for Wolf_Message_Bar
 *
 * @class Wolf_Message_Bar
 * @version 1.1.4
 * @since 1.0.0
 * @package WolfMessageBar
 * @author WpWolf
 */
class Wolf_Message_Bar {


	/**
	 * @var string
	 */
	public $version = '1.1.4';

	/**
	 * @var string
	 */
	private $update_url = 'http://plugins.wpwolf.com/update';


	/**
	 * Wolf_Message_Bar Constructor.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		define( 'WOLF_MESSAGE_BAR_URL', plugins_url( '/' . basename( dirname( __FILE__ ) ) ) );
		define( 'WOLF_MESSAGE_BAR_DIR', dirname( __FILE__ ) );
		

		// Load plugin text domain
		add_action( 'init', array( $this, 'plugin_textdomain' ) );

		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_action( 'admin_menu',  array( $this, 'add_menu' ) );

		add_action( 'admin_print_styles', array( $this, 'admin_style' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_script' ) );
		
		add_action( 'admin_init', array( $this, 'plugin_update' ) );

		add_action( 'after_setup_theme', array($this, 'options_init' ) );
		add_action( 'wp_print_styles', array($this, 'print_styles' ));
		add_action( 'wp_enqueue_scripts', array($this, 'enqueue_scripts' ));
 
		add_action( 'wp_head', array($this, 'cookies' ) );
	}


	/**
	 * Loads the plugin text domain for translation
	 */
	public function plugin_textdomain() {

		$domain = 'wolf';
		$locale = apply_filters( 'wolf', get_locale(), $domain );
		load_textdomain( $domain, WP_LANG_DIR.'/'.$domain.'/'.$domain.'-'.$locale.'.mo' );
		load_plugin_textdomain( $domain, FALSE, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );

	}



	/**
	 * Print Styles
	 */
	public function print_styles() {
		wp_enqueue_style( 'wolf-message-bar', WOLF_MESSAGE_BAR_URL . '/assets/css/message-bar.css', array(), $this->version, 'all' );
	}


	/**
	 * Enqueue Script
	 */
	public function enqueue_scripts() {
		if ( ! wp_script_is( 'jquery' ) )
			wp_enqueue_script( 'jquery' );
		
		
		wp_register_script( 'cookie', WOLF_MESSAGE_BAR_URL . '/assets/js/min/jquery.memo.min.js', 'jquery', '1.0', true );
		wp_register_script( 'wolf-message-bar', WOLF_MESSAGE_BAR_URL . '/assets/js/min/message-bar.min.js', 'jquery', $this->version, true );

	}


	/**
	 * Print Admin Styles
	 */
	public function admin_style() {
		
		if ( isset( $_GET['page'] ) && $_GET['page'] == basename( __FILE__ ) ) {
			wp_enqueue_style( 'wp-color-picker' );
		}
	}


	/**
	 * Enqueue admin script
	 */		
	public function admin_script() {
		
		if ( isset( $_GET['page'] ) && $_GET['page'] == basename( __FILE__ ) ) {
			wp_enqueue_script( 'wolf-message-bar-colorpicker', WOLF_MESSAGE_BAR_URL . '/assets/js/min/colorpicker.min.js' , array( 'wp-color-picker' ), false, true );
		}
	}


	/**
	 * Get message bar option
	 */
	public function get_option( $value ) {
		global $options;
		$settings = get_option( 'wolf_message_bar_settings' );
		
		if ( isset($settings[$value]) )
			return $settings[$value];

	}

	
	/**
	 * Set default settings
	 */
	public function options_init() {
		global $options;

		if ( false ===  get_option( 'wolf_message_bar_settings' )  ) {

			$default = array(
				'message' => '<strong>Howdy!</strong> You can edit this message in the Wolf Mesage Bar <a href="' . esc_url( admin_url( 'admin.php?page=wolf-message-bar.php' ) ) . '">admin panel</a>',
				'premade_color'  => '#D65253',
				'display'     => 'cookie'
			);

			add_option( 'wolf_message_bar_settings', $default );
		}
	}



	/**
	* Add menu
	*/
	public function add_menu() {
		
		// Add Contextual menu
		add_menu_page(__( 'Message Bar', 'wolf' ), __( 'Message Bar', 'wolf' ), 'activate_plugins', basename( __FILE__ ), array($this, 'wolf_message_bar_settings' ), 'dashicons-minus' );

	}



	/**
	* Add Settings
	*/
	public function admin_init() {
		register_setting( 'wolf-message-bar', 'wolf_message_bar_settings', array($this, 'settings_validate' ) );
		add_settings_section( 'wolf-message-bar', '', array($this, 'section_intro' ), 'wolf-message-bar' );
		add_settings_field( 'message', __( 'Message', 'wolf' ), array($this, 'setting_message' ), 'wolf-message-bar', 'wolf-message-bar' );
		add_settings_field( 'display', __( 'Display', 'wolf' ), array($this, 'setting_display' ), 'wolf-message-bar', 'wolf-message-bar' );
		add_settings_field( 'pre-made-color', __( 'Pre made color', 'wolf' ), array($this, 'setting_pre_made_color' ), 'wolf-message-bar', 'wolf-message-bar' );
		add_settings_field( 'color', __( 'Custom color', 'wolf' ), array($this, 'setting_color' ), 'wolf-message-bar', 'wolf-message-bar' );
		add_settings_field( 'instruction', __( 'Instruction', 'wolf' ), array($this, 'setting_instruction' ), 'wolf-message-bar', 'wolf-message-bar' );
	
	}



	/**
	 * Validation
	 */
	public function settings_validate( $input ) {
		$input['display'] = sanitize_text_field( $input['display'] );
		$input['premade_color'] = sanitize_text_field( $input['premade_color'] );
		return $input;
	}


	public function section_intro() {
		// global $options;
		// echo "<pre>";
		// print_r(get_option( 'wolf_message_bar_settings' ));
		// echo "</pre>";
	}


	public function setting_message() {
		?>
		<textarea cols="55" rows="5" name="wolf_message_bar_settings[message]"><?php echo stripslashes($this->get_option( 'message' )) ?></textarea>
		<p>
			<em><?php _e( 'You message can contain text, html and shortcodes.', 'wolf' ) ?></em>
		</p>
		<?php

	}


	public function setting_display() {
		?>
		<select name="wolf_message_bar_settings[display]">
			<option <?php if ( $this->get_option( 'display' ) == 'cookie' ) echo 'selected="selected"'; ?> value="cookie"><?php _e( 'Use a Cookie', 'wolf' ); ?></option>
			<option <?php if ( $this->get_option( 'display' ) == 'open' ) echo 'selected="selected"'; ?> value="open"><?php _e( 'Always Open', 'wolf' ); ?></option>
			<option <?php if ( $this->get_option( 'display' ) == 'close' ) echo 'selected="selected"'; ?> value="close"><?php _e( 'Always Close', 'wolf' ); ?></option>
		</select>
		<em><?php _e( 'You can use a cookie for the user\'s browser to remember if the message is opened or closed regardless of page changes.', 'wolf' ); ?></em>
		<?php
	}


	public function setting_pre_made_color() {
		?>
		<select name="wolf_message_bar_settings[premade_color]">
			<option <?php if ( $this->get_option( 'premade_color' ) == '#D65253' ) echo 'selected="selected"'; ?> value="#D65253"><?php _e( 'Red', 'wolf' ); ?></option>
			<option <?php if ( $this->get_option( 'premade_color' ) == '#7DD071' ) echo 'selected="selected"'; ?> value="#7DD071"><?php _e( 'Green', 'wolf' ); ?></option>
			<option <?php if ( $this->get_option( 'premade_color' ) == '#2C2C2C' ) echo 'selected="selected"'; ?> value="#2C2C2C"><?php _e( 'Grey', 'wolf' ); ?></option>

		</select>
		<?php
	}


	public function setting_color() {
		echo '<input type="text" name="wolf_message_bar_settings[color]" class="wolf-message-bar-color" value="'. $this->get_option( 'color' ) .'">';
		?>
		<br>
		<em><?php _e( 'You can set your own custom color that will override the color settings above', 'wolf' ); ?></em>
		<?php
	}


	public function setting_instruction() {
		?>
		<p><?php _e( 'To display the message bar at the top of your website, paste the following public function right after you body open tag in your header.php file:', 'wolf' ); ?></p>
		<p>
			<code>&lt;?php if ( function_exists( 'wolf_message_bar' ) ) wolf_message_bar(); ?&gt;</code>
		</p>
		<br>
		<p><img src="<?php echo esc_url( WOLF_MESSAGE_BAR_URL . '/assets/help.jpg' ); ?>" alt="screenshot"></p>
		<?php
	}


	/**
	* Form
	*/
	public function wolf_message_bar_settings() {
		?>
		<div class="wrap">
			<div id="icon-options-general" class="icon32"></div>
			<h2><?php _e( 'Message Bar Settings', 'wolf' ); ?></h2>
			<?php if ( isset($_GET['settings-updated']) && $_GET['settings-updated'] ) { ?>
			<div id="setting-error-settings_updated" class="updated settings-error"> 
				<p><strong><?php _e( 'Settings saved.', 'wolf' ); ?></strong></p>
			</div>
			<?php } ?>
			<form action="options.php" method="post">
				<?php settings_fields( 'wolf-message-bar' ); ?>
				<?php do_settings_sections( 'wolf-message-bar' ); ?>
				<p class="submit">
					<input name="save" type="submit" class="button-primary" value="<?php _e( 'Save Changes', 'wolf' ); ?>" />
				</p>
			</form>
		</div>
		<?php
	}


	/**
	 * Remove spaces in inline CSS
	 */
	public function compact_css( $css  ) {

		return preg_replace( '/\s+/', ' ', $css );

	}


	/**
	 * Add CSS to display top bar message or not depending on plugin options
	 */
	public function cookies() {

		$display = $this->get_option( 'display' );

		$output = "\n";

		$open = '#wolf-message-bar-container #wolf-message-bar {display:block;}';

		$close = '#wolf-message-bar-container #wolf-message-bar {display:none;} #wolf-message-bar-container #wolf-message-bar-ribbon {top : 0}';

		if ( ! $this->get_option( 'color' ) )
			$color = $this->get_option( 'premade_color' );
		else
			$color = $this->get_option( 'color' );


		if ( $this->get_option( 'message' ) ) {
			
			$output .=  '<style type="text/css">';

			if ( $display == 'open' ) {

				$output .= $open;

			} elseif ( $display == 'close' ) {

				$output .= $close;

			} elseif ( $display == 'cookie' ) {

				if ( ! isset( $_COOKIE["wolf-message-bar"] ) ) {

					//setcookie("message-bar", "open", 3600, '/' );
					$output .= $open;
				}else{

					if ( $_COOKIE["wolf-message-bar"] == 'open' ) {

						$output .= $open;

					} elseif ( $_COOKIE["wolf-message-bar"] == 'close' ) {

						$output .= $close;

					}

				}

			}

			$output .= $this->compact_css( "#wolf-message-bar-container #wolf-message-bar, #wolf-message-bar-container #wolf-message-bar-ribbon {background:$color;}" );

			$output .= '</style>';
		}

		echo $output;

	}

	/**
	 * Output message bar
	 */
	public function display_bar() {
		if ( $this->get_option( 'message' ) ) : 
			
			if ( ! wp_script_is( 'cookie' ) )
				wp_enqueue_script( 'cookie' );

			wp_enqueue_script( 'wolf-message-bar' );
		?>
		<div id="wolf-message-bar-container">
			<div id="wolf-message-bar">
				<div id="wolf-message-bar-wrap">
					<div id="wolf-message-bar-inner"><?php echo do_shortcode( stripslashes( $this->get_option( 'message' ) ) ); ?></div>
					<span id="wolf-message-bar-close"><?php _e( 'close', 'bd' ); ?> <span id="wolf-message-bar-cross">&times;</span></span>
				</div>
			</div>
			<span id="wolf-message-bar-ribbon">&#43;</span>
		</div>
		<?php endif;
	}


	/**
	 * Plugin update
	 */
	public function plugin_update() {
		
		$plugin_data = get_plugin_data( __FILE__ );

		$current_version = $plugin_data['Version'];
		$plugin_slug = plugin_basename( dirname( __FILE__ ) );
		$plugin_path = plugin_basename( __FILE__ );
		$remote_path = $this->update_url . '/' . $plugin_slug;
		
		if ( ! class_exists( 'Wolf_WP_Update' ) )
			include_once( 'classes/class-wp-update.php' );
		
		new Wolf_WP_Update( $current_version, $remote_path, $plugin_path );
	}
	

} // end class

global $wolf_message_bar;
$wolf_message_bar = new Wolf_Message_Bar;

if ( ! function_exists( 'wolf_message_bar' ) ) {
	function wolf_message_bar() {
		global $wolf_message_bar;
		$wolf_message_bar->display_bar();
	}
}

} // end class  check