;( function( $ ) {
	
	var wolfMessageBar = $( '#wolf-message-bar' );

	$( '#wolf-message-bar #wolf-message-bar-close' ).click( function() {
		$( '#wolf-message-bar' ).slideUp().addClass( 'close' ).removeClass( 'open' );
		$( '#wolf-message-bar-container #wolf-message-bar-ribbon' ).animate( { top : 0 } );
		$.cookie( 'wolf-message-bar', 'close', { path: '/', expires: 3 } );
	} );

	$( '#wolf-message-bar-container #wolf-message-bar-ribbon' ).click( function() {
		wolfMessageBar.slideDown().addClass( 'open' ).removeClass( 'close' );
		$(this).animate( { top : '-30px' } );
		$.cookie( 'wolf-message-bar', 'open', { path: '/', expires: 3 } );
	} );

} )( jQuery );