<?php
/**
 * Class Bonneville Contests Lib
 */


Class bonneville_contests_lib 
{
	protected $ch;

	protected $baseURL;

	public function __construct()
	{
		$this->ch = curl_init();
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
	}
	public function __destruct()
	{
		curl_close($this->ch);
	}
	protected function url($path)
	{
		return $this->baseURL . $path;
	}

	/**
	 * LIBRARY METHODS
	 */
	
	public function allFromShowID($id){
		global $wpdb;
		$post_type='contest';
		$arrayOfContests = [];

		$args = array(
		    'post_type' => 'contest',
		    'tax_query' => array(
		        array(
		            'taxonomy' => 'show',
		            'field' => $id,
		            'terms' => 'locked'
		        )
		    )
		);
		$your_query = new WP_Query( $args );

		while ( $your_query->have_posts() ) {
		    $your_query->the_post();
		    $arrayOfContests .= the_post();
		    $the_title = get_the_title(); // variable $the_title now holds your title
		}
		return $arrayOfContests;

		
	}



}