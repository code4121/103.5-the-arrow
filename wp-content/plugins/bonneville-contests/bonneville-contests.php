<?php
/*
Plugin Name: Bonneville Contest Plugin
Plugin URI: 
Description: A plugin that will allow the creation, organization, and manipulation of a contest custom post type.
Version: 1.0
Author: Cody Nelson
Author URI:
*/


//Prevent loading directly.
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/**
 * Include the Contests Library
 */
require_once dirname( __FILE__ ) . '/src/bonneville-contests-lib.php';
//use Contests as lib;


function createContests(){

}



	$Contests = new bonneville_contests_lib();




add_action( 'init', 'bonneville_register_contest_pt');
/**
 * Registes Post type with the following attributes:
 * @return post_type
 */
function bonneville_register_contest_pt(){


	register_post_type( 'contest', 
		array(
			'labels' => array(
				'name' => 'Contests',
				'singular_name' => 'Contest',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New Contest',
				'edit' => 'Edit',
				'edit_item' => 'Edit Contest',
				'new_item' => 'New Contest',
				'view' => 'View',
				'view_item' => 'View Contest',
				'all_items' => 'All Contests',
				'search_items' => 'Search Contest',
				'not_found' => 'No Contest Found',
				'not_found_in_trash' => 'No Contest Found in Trash',
				'parent' => 'Parent Contest'
			),

			'public' => true,
			'menu_position' => 7,
			'supports' => array ('title', 'editor', 'thumbnail', 'custom-fields' ),
			'taxonomies' => array( 'show' ),
			'menu_icon' => plugins_url( 'images/icon.png', __FILE__ ),
			'has_archive' => true
		)

	);
}

add_action( 'init', 'register_custom_shows_tax');
/**
 * Registers the taxonomy to organize the contest.
 */
function register_custom_shows_tax(){

	//create new Shows Taxonomy
	register_taxonomy(
		'show',
		'contest',
		array(
			'labels' => array(
			'name' => 'Shows',
			'singular_name' => 'Show',
			'search_items' => 'Search Shows',
			'all_items' => 'All Shows',
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => 'Edit Show',
			'update_item' => 'Update Show',
			'add_new_item' => 'Add New Show',
			'new_item_name' => 'Show Name',
			'separate_items_with_commas' => 'Separate Shows With Commas',
			'add_or_remove_items' => 'Add or Remove Shows',
			'not_found' => 'No shows found.',
			'menu_name' => 'Shows',
		),
		'rewrite' => array( 'slug' => 'show' ),
		'hierarchical' => true,
		//'sort' => true,
		'capabilities' => array(
			'assign_terms' => 'edit_posts',
			'edit_terms' => 'publish_posts',
			'manage_terms' => 'manage_categories'
			)
		)
	);
	$is_safely_registered = register_taxonomy_for_object_type( 'contest' , 'show');
}


/**
 * Add Featured Image to RSS feed
 *
**/
add_filter( 'the_content', 'ld_featured_image_in_feed' );
function ld_featured_image_in_feed( $content ) {
    global $post;
    if( is_feed() ) {
        if ( has_post_thumbnail( $post->ID ) ){
            $feat_image_output = get_the_post_thumbnail( $post->ID, 'full', array( 'style' => 'height: auto;margin-bottom:2em;max-width: 600px !important;padding-top: 0.75em;width: 100% !important;' ) );
            $content = $feat_image_output . $content;
        }
    }
    return $content;
}

add_shortcode('default_contest_logo_url', 'default_image_shortcode');
function default_image_shortcode(){
	$url = plugins_url( 'images/default_contest.png', __FILE__ );
	return $url;
	//"<img src='" . $url . "' alt='default image'/>";
}

/**
* Custom Image on Contest Admin Page.
**/
add_filter('manage_posts_columns', 'add_img_column');
add_filter('manage_posts_custom_column', 'manage_img_column', 10, 2);

function add_img_column($columns) {
    $columns['img'] = 'Featured Image';
    return $columns;
}

function manage_img_column($column_name, $post_id) {
    if( $column_name == 'img' ) {
        echo get_the_post_thumbnail($post_id, 'thumbnail');
    }
}
