<?php
/*
Plugin Name: Gravity Limits 
Description: Adds a feild to gravity forms that can be hidden and not allow duplicate
Version: 1.0
Author: Nathan Knowles
*/

Class GravityLimit {

    function __construct() {
        add_filter( 'gform_form_settings', array( $this, 'user_limit_form_setting' ), 10, 2);
        add_filter( 'gform_pre_form_settings_save', array( $this, 'save_user_limit_form_setting' ) );
        add_filter( 'gform_pre_render', array( $this, 'validate_user_submission_limit' ) );
    }
    
    function user_limit_form_setting( $settings, $form ) {
        $options = array( 'unlimited' => 'Unlimted',
                          'once_per_user' => 'Once Per User',
                          'once_per_day'  => 'Once Per Day',
                          'once_per_week'  => 'Once Per Week',
                          'once_per_month'  => 'Once Per Month'
        );

        $selected = rgar( $form, 'user_limit_setting' );
        $message = rgar( $form, 'user_limit_message' );
        if ( empty( $message ) )
            $message = "&lt;h1&gt;You have already completed this form. &lt;/h1&gt;";
        
        $user_limit_setting = '
            <tr>
                <th><label for="user_limit_setting">Limit User Entries</label></th>
                <td>
                <select id="user_limit_setting" name="user_limit_setting">';
                foreach( $options as $val => $label ) {
                    if( $selected === $val ) {
                        $select = 'selected="selected"';
                    } else {
                        $select = '';
                    }

                    $user_limit_setting .= "<option value='{$val}' {$select}>{$label}</option>";
                }
        $user_limit_setting .= '</select>';
                $user_limit_setting .= '
                    <tr id="user_limit_message_setting" class="child_setting_row" style="">
                    <td colspan="2" class="gf_sub_settings_cell">
                       <div class="gf_animate_sub_settings">
                        <table>
                        <tbody><tr>
                        <th>
                            User Limit Message <a href="#" onclick="return false;" class="tooltip tooltip_form_user_limit_message" tooltip="&lt;h6&gt;User Limit Message&lt;/h6&gt;Enter a message to be displayed to users who have already entered (shortcodes and HTML are supported)">(?)</a>
                        </th>
                        <td>
                            <textarea id="user_limit_message" name="user_limit_message" class="fieldwidth-3">'.$message .'</textarea>
                        </td>
            
                        </tr>
                    </tbody></table>
                </div>
            </td>';
        $user_limit_setting .= '</td></tr>';
        $settings['Restrictions']['user_limit_setting'] = $user_limit_setting;

        return $settings;
    }

    // save your custom form setting
    public function save_user_limit_form_setting( $form ) {
        $form['user_limit_setting'] = rgpost( 'user_limit_setting' );
        $form['user_limit_message'] = htmlspecialchars( rgpost( 'user_limit_message' ) );
        return $form;
    }
 
    public function validate_user_submission_limit( $form ) {

        global $wpdb;

        $limit = rgar( $form, 'user_limit_setting' );
        $message = rgar( $form, 'user_limit_message' );
        
        switch ( $limit ) {
            case 'unlimited':
                return $form;
                break;

            case 'once_per_user':
                $limit_time = false;
                break;

            case 'once_per_day':
                $limit_time = "+1 day";
                break;

            case 'once_per_week':
                $limit_time = "+1 week";
                break;

            case 'once_per_month':
                $limit_time = "+1 month";
                break;

            default: 
                return $form;
                break;
        }

        $current_user = wp_get_current_user();
        $last_submission = $wpdb->get_var( $wpdb->prepare( "SELECT date_created FROM {$wpdb->prefix}rg_lead WHERE created_by = %d and form_id = %d ORDER BY date_created DESC", $current_user->ID, $form['id'] ) );
        if( empty( $last_submission ) )
            return $form;
        
        $last_submission = strtotime( $last_submission );
        
        $time_out = strtotime( $limit_time, $last_submission );

        $current_time = time();

        if( $limit_time and $current_time > $time_out ) 
             return $form;

         $is_submit = rgpost( "is_submit_{$form['id']}" );

         if( !$is_submit ) {
             add_filter( 'gform_get_form_filter', function( $message ) use ($message) {
                    $messages =  "<div class='limit-message'>". htmlspecialchars_decode($message) . "</div>";
                    return $messages;
             });
         }

        return $form; 
    }

}
$GravityLimit = new GravityLimit();
