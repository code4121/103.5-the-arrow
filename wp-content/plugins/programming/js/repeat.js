jQuery( document ).ready(function($) {
    
    $( '.repeatable-add' ).click( function( ) {
        console.log('click');
        var currentField = $( this ).closest( "li" );
        var field = currentField.clone( true );
        
        $( 'input', field ).val( '' ).attr('name', function( index, name ) {
            return name.replace( /(\d+)/, function( fullMatch, n ) {
                return $('.custom_repeatable li').size();//Number( n ) + 1;

            });
        }).removeClass('stub');
        

        $( '.repeatable-add', field ).html( '&ndash;' )
                                     .removeClass( 'repeatable-add' )
                                      .addClass( 'repeatable-remove' )
                                      .unbind( 'click' )
                                      .click( removeField );


        $('.custom_repeatable').append( field );
        return false;
    });

    $( '.repeatable-remove' ).click( removeField );

    function removeField( ){
        $( this ).closest( "li" ).remove( );
        return false;
    }
        
   /* $( '.custom_repeatable' ).sortable({
        opacity: 0.6,
        revert: true,
        cursor: 'move',
        //handle: '.sort',
        update: function(event, ui)
        {
            var order = $( '.custom_repeatable' ).sortable('toArray');

            console.log(order);    
            for(var key in order) {
                var val = order[key];
                var part = val.split("_");
                //update each hidden field used to store the list item position
                console.log("order"+part[1]);
                //document.getElementById("order"+part[1]).value = key;
           }
        }
    });*/
});

