<?php
/*
Plugin Name: Weekly Programming Custom post type
Description: Creates a Special post type for programming
Version: 1.0
Author: Nathan Knowles
*/

class programming 
{
    protected $pluginPath;

    function __construct()
    {
        // Set Plugin Path  
        $this->pluginPath = dirname( __FILE__ );
         
        // Register plugin
        add_action( 'init', array( $this, 'custom_post_programming' ) );
        add_action('save_post', array ( $this, 'save' ) , 1, 2); // save the custom fields

    }
    
  
    public function custom_post_programming() {
        $labels = array(
            'name'               => _x( 'On air Schedule', 'post type general name' ),
            'singular_name'      => _x( 'Schedule', 'post type singular name' ),
            'add_new'            => _x( 'Add New', 'book' ),
            'add_new_item'       => __( 'Add New Scheduled Show' ),
            'edit_item'          => __( 'Edit Scheduled Show' ),
            'new_item'           => __( 'New Scheduled Show' ),
            'all_items'          => __( 'All Scheduled Programming' ),
            'view_item'          => __( 'View Show' ),
            'search_items'       => __( 'Search Show' ),
            'not_found'          => __( 'No scheduled shows found' ),
            'not_found_in_trash' => __( 'No scheduled shows found in the Trash' ), 
            'parent_item_colon'  => '',
            'menu_name'          => 'Schedule'
        );
        $args = array(
            'labels'        => $labels,
            'description'   => 'Holds our Scheduled Programs',
            'public'        => true,
            'menu_position' => 5,
            'register_meta_box_cb' => array( &$this, 'add_schedule_metaboxes'),
            'supports'      => array( 'title', 'author' ),
            'has_archive'   => true,
        );
        register_post_type( 'Schedule', $args );	
    }

    public function add_schedule_metaboxes() {
       add_meta_box('la_schedules', 'Today On the Sound', array( &$this, 'admin' ), 'Schedule', 'side', 'default');
    }

    public function admin() {
        global $post;
	    $test = wp_enqueue_script(
		    'repeat',
		    plugins_url( 'js/repeat.js', __FILE__ ),
		    array( 'jquery' )
        );
        
	    // Noncename needed to verify where the data originated
	    echo '<input type="hidden" name="today_meta_noncename" id="today_meta_noncename" value="' . 
	    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	
	    // Get the schedule data if its already been entered
	    $schedule = get_post_meta($post->ID, '_wp_sound_schedule', false);
               
        echo "<style>.stub{ display:none }</style>
            <ul class='custom_repeatable'>";
            echo "<li id='sort_0'><input placeholder='Title' type='text' name='thesound_today[0][_title]' value='' class='stub' /></span><span><input placeholder='Item' type='text' name='thesound_today[0][_item]' value='' class='stub' /></span><span><a class='repeatable-add button' href='#'>+</a></span></li>";
        if( $schedule ) {
           foreach ( $schedule[0] as $key => $value ) {	
               // Echo out the field
            $title = trim(esc_textarea($value['_title']));
            $item = trim(esc_textarea($value['_item']));
            //$key = $key + 1;
            echo "<li id='sort_0'><input placeholder='Title' type='text' name='thesound_today[{$key}][_title]' value='{$title}' class='' /></span><span><input placeholder='Item' type='text' name='thesound_today[$key][_item]' value=' " . $item  . "' class='' /></span><span><a class='repeatable-remove button' href='#'>&ndash;</a></span></li>";
            }

        } /*else {
            echo "<li id='sort_0'><input placeholder='Title' type='text' name='thesound_today[0][_title]' value='' class='' /></span><span><input placeholder='Item' type='text' name='thesound_today[0][_item]' value='' class='' /></span><span><a class='repeatable-add button' href='#'>+</a></span></li>";
        }*/
        echo "</ul>";
    }
    
    
    public function save( $post_id, $post ) {
        global $custom_meta_fields;
        
        if ( ! isset( $_POST['today_meta_noncename'] ) ) {
            return $post->ID;
        } 
        // verify this came from the our screen and with proper authorization,
	    // because save_post can be triggered at other times
	    if ( !wp_verify_nonce( $_POST['today_meta_noncename'], plugin_basename(__FILE__) )) {
	        return $post->ID;
        }
        // Is the user allowed to edit the post or page?
	    if ( !current_user_can( 'edit_post', $post->ID ))
            return $post->ID;

        $thesound_today = $_POST['thesound_today'];
        array_walk_recursive( $thesound_today, 'esc_textarea' );
        update_post_meta($post->ID, '_wp_sound_schedule', $thesound_today); 
    }


}

class onairWidget extends WP_Widget {

	function onairWidget() {
		// Instantiate the parent object
		parent::__construct( false, 'On Air Schedule' );
	}

	function widget( $args, $instance ) {
        global $wpdb;
        $query = new WP_Query( array( 'post_type' => 'Schedule', 'post_status' => 'publish' ) );
        $outbody="";
        while ( $query->have_posts() ) {
            $query->the_post(); 

            /*$meta = $wpdb->get_results( $wpdb->prepare("SELECT meta_key, meta_value, meta_id, post_id
                        FROM $wpdb->postmeta WHERE post_id = %d
                        ORDER BY meta_key,meta_id". get_the_id()), ARRAY_A );*/
	    $schedule = get_post_meta($query->post->ID, '_wp_sound_schedule', false);
        $meta = array();
        $outbody .= "
            <div style='text-size:2; width:274;'>
            <h4>Today On The Sound</h4><br>
            <table class='onairschedule-style' width='274' border='0' cellspacing='2'>
            <tbody>
        ";
    foreach ( $schedule[0] as $entry ) {
        $outbody.="
            <tr>
                <td cellpadding='5' class='onairschedule-cell'>
                <span class='onair-text'>{$entry['_title']}</span><br><b>{$entry['_item']}</b>
                </td>
            </tr>
        ";
    }

        $outbody .= "
            </tbody></table>
            </div>
        ";
    
        }
       echo $outbody;
	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
	}

	function form( $instance ) {
		// Output admin widget options form
	}
}

function onairWidget_register() {
	return register_widget( 'onairWidget' );
}

add_action( 'widgets_init', 'onairWidget_register' );

$Programming = new Programming();  
