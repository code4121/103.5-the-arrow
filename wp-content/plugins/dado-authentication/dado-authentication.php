<?php
/*
Plugin Name: CMS-DADO to WP Migrated Users Authentication Plugin
Description: Authenticate users migrated from The CMS/DADO and update their WP passwords
Version: 1.0
Author: Nathan Knowles
*/

class DadoAuthentication
{
    protected $pluginPath;

    function __construct()
    {
        // Set Plugin Path
        $this->pluginPath = dirname( __FILE__ );

        // Register plugin
        add_filter( 'authenticate', array( $this, 'migrate_auth' ), 1, 3 );
        add_action( 'password_reset', array( $this, 'disable_dado' ), 10, 2 );

    }

    public function migrate_auth( $user, $username, $password )
    {
        if ( is_a( $user, 'WP_User' ) ) {
            return $user;
        }

	    if ( empty($username) || empty($password) ) return $user;

        // retrieve user data
	    $userdata = get_user_by('login', $username);

        if ( !$userdata )
        {
	        $userdata = get_user_by('email', $username);
            if ( !$userdata ) return $user;
            $username = $userdata->user_login;
        }

        if ( !$userdata->dado_pass )
        {
            return wp_authenticate_username_password( null, $username, $password );
        }

        if ( $this->auth_dado( $username, $password, $userdata->dado_pass ) )
        {
             // Update User password
             wp_update_user( array ('ID' => $userdata->ID, 'user_pass' => $password) );
             // Move Dado_pass to
             update_user_meta( $userdata->ID, 'dado_pass_backup', $userdata->dado_pass );
             delete_user_meta( $userdata->ID, 'dado_pass' );
        }
        return wp_authenticate_username_password( null, $username, $password );
    }

    private function auth_dado( $username, $password, $dado_password  )
    {
        if ( $this->comparePasswords( $password, $dado_password) )
        {
            return true;
        }

        return false;
    }

    private function hash( $password, $salt = false )
	{
        if( $salt === false )
        {
			$salt = substr( md5( microtime() ), 0, 10 );
        }
        else if (!is_string( $salt ) )
        {
			return false;
        }
		// hash the password a bunch of times to make it much harder
		// to brute force
		$hash = $password;
        for( $i = 0; $i < 1000; $i++ )
        {
			$hash = md5( $hash . $salt );
		}

		// format is such:
		// $<algorithm>$<salt>$<hash>
		$output = "$" . "A$" . "$salt$" . $hash;

		return $output;
	}

    // ---------------------------------------------------------------
	private function comparePasswords( $plainPassword, $hashedPassword )
    {
		$d = explode( '$', $hashedPassword );
        if( $d === false )
        {
			return false;
		}

		$algorithm = $d[1];
		$salt = $d[2];
		$password = $d[3];

        if ($algorithm != 'A' )
        {
            return false;
        }

		$hash = $this->hash( $plainPassword, $salt );
        if( $hash != $hashedPassword )
        {
			return false;
        }
        return true;
    }

    public function disable_dado( $user, $new_pass )
    {
        $dado_pass = get_user_meta($user->ID, 'dado_pass', true);
        if ( !$dado_pass ) {
            return;
        }

        update_user_meta( $user->ID, 'dado_pass_backup', $dado_pass );
        delete_user_meta( $user->ID, 'dado_pass' );

        return;
    }


}

$DadoAuthentication = new DadoAuthentication();
