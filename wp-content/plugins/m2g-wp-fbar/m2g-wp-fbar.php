<?php
/*
Plugin Name: TuneGenie Media Player Bar
Plugin URI: http://www.tunegenie.com/
Description: Add the TuneGenie Media Player Bar to your wordpress site.
Version: 1.0.5
Author: defmtog
Author URI: http://www.tunegenie.com
Copyright: (C) 2014-2016,  by MusicToGo, LLC ("TuneGenie").  All rights reserved.

*/

define('M2G_BAR_PLUGIN_URL', plugins_url().'/m2g-wp-fbar');
define('M2G_BAR_PLUGIN_PATH', dirname(__FILE__).DIRECTORY_SEPARATOR);

require_once(M2G_BAR_PLUGIN_PATH . 'inc/bar.php');

/* check if in wp-vip */
if ( defined( 'WPCOM_IS_VIP_ENV' ) && true === WPCOM_IS_VIP_ENV ) {
    // Running on WordPress.com
    wpcom_vip_load_plugin('m2g-wp-fbar');
} else {
    // Not running on WordPress.com
    register_activation_hook(__FILE__, array('M2G_Bar', 'm2g_activate_plugin'));
    register_deactivation_hook( __FILE__, array('M2G_Bar', 'm2g_deactivate_plugin'));
}

$bar = new M2G_Bar();

?>
