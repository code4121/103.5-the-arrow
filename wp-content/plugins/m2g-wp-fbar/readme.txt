=== Plugin Name ===
Contributors: dflesner
Tags: M2G media bar
Requires at least: 4.0
Tested up to: 4.0
Stable tag: 1.0.5
Copyright: (C) 2016 by MusicToGo, LLC ("TuneGenie").  All rights reserved.

Install the m2g media bar into wordpress.
