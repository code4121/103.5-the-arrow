<?php
/*
  The m2g media bar
*/

class M2G_Bar {

    /* remote url for enqueue style */
    var $plugin_url;
    /* local path to views files */
    var $views_dir;
    // options
    static $option_group = 'm2g_fbar_options';
    static $option_names = array('m2g_bar_brand','m2g_bar_zindex','m2g_bar_theme','m2g_bar_position','m2g_bar_ios_frame','m2g_bar_debug','m2g_bar_autostart','m2g_bar_info_tray_on_load','m2g_bar_tgmplibs','m2g_bar_tritonplayer','m2g_bar_zag');
    static $defaults = array(
        'm2g_bar_brand'=> '',
        'm2g_bar_zindex'=> 5000,
        'm2g_bar_theme'=> '["#000000"]',
        'm2g_bar_position' => 'bottom',
        'm2g_bar_ios_frame' => '1',
        'm2g_bar_debug'=> 'false',
        'm2g_bar_autostart'=> 'false',
        'm2g_bar_info_tray_on_load'=> 'true',
        'm2g_bar_tgmplibs'=> '',
        'm2g_bar_tritonplayer'=> 'false',
        'm2g_bar_zag'=> 'false'
        );

    public function __construct() {

        $this->plugin_url = plugins_url() . '/m2g-wp-fbar/';
        $this->views_dir = plugin_dir_path(dirname(__FILE__)) . 'views/';

        add_action('wp_footer', array(&$this, 'm2g_load_js'), 100);

        # add admin menu
        add_action('admin_init', array( &$this, 'm2g_admin_init'));
        add_action('admin_menu', array( &$this, 'm2g_admin_menu'));

        # add setup notification
        add_action('update_option_m2g_bar_brand', array( &$this, 'm2g_notify_brand_change'), 11, 2);
        add_action('update_option_m2g_bar_tritonplayer', array( &$this, 'm2g_notify_tritonplayer_change'), 12, 2);
        add_action('update_option_m2g_bar_zag', array( &$this, 'm2g_notify_zag_change'), 12, 2);

        # filters
        add_filter('sanitize_option_m2g_bar_brand', array( &$this, 'sanitize_option_m2g_bar_brand'));
        add_filter('sanitize_option_m2g_bar_zindex', array( &$this, 'sanitize_option_m2g_bar_zindex'));
        add_filter('sanitize_option_m2g_bar_theme', array( &$this, 'sanitize_option_m2g_bar_theme'));
        add_filter('sanitize_option_m2g_bar_tgmplibs', array( &$this, 'sanitize_option_m2g_bar_tgmplibs'));
    }

    static function m2g_activate_plugin() {
        /* Creates new database fields */
        foreach(self::$option_names as $name) {
            add_option($name, self::$defaults[$name], '', 'yes');
        }
    }

    static function m2g_deactivate_plugin() {
        /* Deletes the database fields */
        foreach(self::$option_names as $name) {
            delete_option($name);
        }
    }

    function m2g_load_js() {
        if (get_option('m2g_bar_brand') !== '') {
            include $this->views_dir . 'bar.js.php';
        }
    }

    function m2g_debug_info() {
        echo 'DEF: ' . get_option('m2g_bar_debug') . ':<br>';
    }

    function m2g_admin_init() {
        foreach (self::$option_names as $option) {
            register_setting(self::$option_group, $option);
        }
    }

    function m2g_checkbox($input) {
        return  isset( $input ) && 'true' == $input ? 'true' : 'false';
    }

    function m2g_admin_menu() {
        add_options_page('TuneGenie Media Player Bar Options', 'TuneGenie Media Player Bar', 'manage_options', self::$option_group, array( &$this, 'm2g_admin_pages'));
    }

    function m2g_admin_pages() {
        include $this->views_dir . 'admin.js.php';
    }

    function get_option($name) {
        echo get_option($name);
    }

    function sanitize_option_m2g_bar_brand($brand) {
        $filtered = strtolower($brand);
        return apply_filters('sanitize_text_field', $filtered, $brand, null);
    }

    function sanitize_option_m2g_bar_zindex($zindex) {
        $filtered = intval($zindex);
        if ( ! $filtered ) {
            return apply_filters('sanitize_option_m2g_bar_zindex', 5000, $zindex, 'zindex_not_an_integer');
        }
        return abs($filtered);
    }

    function sanitize_option_m2g_bar_theme($theme) {
        $default = '["#000000"]';
        if (strpos($theme, "[") !== 0 || strrpos($theme, "]") !== 10 || strpos($theme, "#") !== 2) {
            return apply_filters('sanitize_option_m2g_bar_theme', $default, $theme, 'invalid_color_format');
        }
        return strtoupper($theme);
    }

    function sanitize_option_m2g_bar_tgmplibs($libs) {
        return apply_filters('sanitize_file_name', $libs, $libs, null);
    }

    /* notify on brand change */
    function m2g_notify_brand_change($old, $new) {
        $multiple_recipients = array(
             'wp-config@tunegenie.com'
             );
        if (defined('PROVIDER_EMAIL')) {
            array_push($multiple_recipients, PROVIDER_EMAIL);
            error_log('adding provider email ' . PROVIDER_EMAIL);
        }
        $subject = '[WP] TuneGenie Brand Change for ' . get_site_url() ;
        $body = get_site_url() . ' changed their callsign from ' . $old . ' to ' . $new;
        $headers = array('From: <nobody@tunegenie.com>', 'Content-Type: text/plain; charset=UTF-8');

        wp_mail( $multiple_recipients, $subject, $body, $headers );
    }

    /* notify on tritonplayer change */
    function m2g_notify_tritonplayer_change($old, $new) {
        $multiple_recipients = array(
             'wp-config@tunegenie.com'
             );
        if (defined('PROVIDER_EMAIL')) {
            array_push($multiple_recipients, PROVIDER_EMAIL);
            error_log('adding provider email ' . PROVIDER_EMAIL);
        }
        $subject = '[WP] TuneGenie Triton Player Change for ' . get_site_url() ;
        $body = get_site_url() . ' changed their Triton Player setting from ' . $old . ' to ' . $new;
        $headers = array('From: <nobody@tunegenie.com>', 'Content-Type: text/plain; charset=UTF-8');

        wp_mail( $multiple_recipients, $subject, $body, $headers );
    }

    /* notify on zag change */
    function m2g_notify_zag_change($old, $new) {
        $multiple_recipients = array(
             'wp-config@tunegenie.com'
             );
        if (defined('PROVIDER_EMAIL')) {
            array_push($multiple_recipients, PROVIDER_EMAIL);
            error_log('adding provider email ' . PROVIDER_EMAIL);
        }
        $subject = '[WP] TuneGenie ZAG Form Change for ' . get_site_url() ;
        $body = get_site_url() . ' changed their ZAG Form setting from ' . $old . ' to ' . $new;
        $headers = array('From: <nobody@tunegenie.com>', 'Content-Type: text/plain; charset=UTF-8');

        wp_mail( $multiple_recipients, $subject, $body, $headers );
    }
}
?>