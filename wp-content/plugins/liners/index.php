<?php

/**
 * @package Bonneville Liners
 * @version 1.6
 */
/*
Plugin Name: Bonneville Liners	
Plugin URI: 
Description: Bonneville liners
Author: Doug Bailey
Version: 1.0.1
Author URI: http://bonneville.com
*/


function bonneville_liners_fetch_data( $atts ) {
    //echo "shortcode";
    global $wpdb;
    
    $id = 0;
    
    if( isset( $atts['custom_post_type'] ) ) {
 
	$sql = "SELECT * FROM $wpdb->posts WHERE post_type = '" . $atts['custom_post_type'] . "' AND post_status = 'publish' ORDER BY `menu_order` ASC";

	$results = $wpdb->get_results($sql);
	
	/*echo "-------------------------------";
	var_dump($results);
	echo "-------------------------------";*/
	
	foreach( $results as $result ) {
	
	    $data[$id]['post'] = $result;
	
	    $meta_sql = "SELECT `meta_value` FROM $wpdb->postmeta WHERE `post_id` = " . $result->ID . " AND `meta_key` = '_expiration-date'";
	
	    $meta_results = $wpdb->get_results($meta_sql);
	
	    foreach( $meta_results as $meta ) {   
	    
	        $data[$id]['meta'] = $meta;

	    }
	    
	    $id++;
	
	}
	
	
	
	return $data;
	
    }else{

	return false;

    }
    
}

function bonneville_liners_output($data){
    
    $output = "";
    //echo "<pre>";
    //print_r($data);
    //echo "</pre>";
    foreach($data as $dat){
	
	$output .= "<a href='#".  urlencode($dat['post']->post_title) ."'><strong style='color:#fdc583'>" . $dat['post']->post_title . "</strong></a><br><br>";
	
    }
    for($i=0;$i<=sizeof($data);$i++){
    //foreach($data as $d){
	
	$anchor = $i+1;
	
	if($anchor == 1){
	    
	    $output .= "<a name='".  urlencode($data[$i]['post']->post_title) ."'></a>";
	    
	}
	
	$output .= "<h5><strong style='color:#fdc583'>" . $data[$i]['post']->post_title . "</strong></h5>";
	
	$output .= $data[$i]['post']->post_content;
	
	$output .= "<a name='".  urlencode($data[$anchor]['post']->post_title) ."'></a>";
	
	if(isset($data[$i]['meta']->meta_value) && $data[$i]['meta']->meta_value > 0){
	
	    $output .= "<br><br><strong style='color:#fdc583'>KILL: ".date('m/d/y',$data[$i]['meta']->meta_value) . "</strong>";

	}else{
	    
	    $output .= "<br><br><strong style='color:#fdc583'>KILL: ONGOING</strong>";
	    
	}
	
	$output .= "<br><hr><br>";
	
    }

    return $output;
    
}

    function bonneville_liners_shortcode ( $atts ){
    
	$data = bonneville_liners_fetch_data( $atts );
  
	$output = bonneville_liners_output( $data );

	return $output;

    }

add_shortcode( 'bonneville_liners', 'bonneville_liners_shortcode');

?>
