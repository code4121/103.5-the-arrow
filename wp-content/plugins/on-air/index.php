<?php
/**
 * @package On_Air
 * @version 1.6
 */
/*
Plugin Name: On Air	
Plugin URI: 
Description: This is a plugin that pulls the current on-air host.
Author: Doug Bailey
Version: 1.0.1
Author URI: http://bonneville.com
*/
/*KRSP*/

function on_air(){

	$output = "<script type='text/javascript'>";
	
	$output .= "jQuery(document).ready(function() { ";
		
	$json_url = plugins_url( 'schedule/schedule.json', __FILE__ );
	$default_host_image = plugins_url( 'default_host.jpg', __FILE__ );
			
	$output .= "jQuery.ajax({
				url:'" . $json_url . "',
				dataType:'json',
				success: function(data){
					process_host(data);
				},
				error: function(){
					console.log('no data retrieved from the json file please check the file " . $json_url . "');
				}
		});
		
	});
	
	function process_host(data){
		
		for(i=0; i<data.hosts.length; i++) {
		
			var current_host = find_current_host(data.hosts[i]);
			
			if(current_host != false){
				break;
			}
			
		}
		
		display_host(current_host);
		
	}
	
	function find_current_host(data){

		var date = new Date();
		var hour = addZero(date.getHours());
		var mins = addZero(date.getMinutes());
		var now_time = hour + ':' + mins;
		var host_start_time = data.start_hour + ':' + data.start_min;
		var host_end_time = data.end_hour + ':' + data.end_min;
		var on_air = is_on_air_now(now_time, host_start_time, host_end_time);

		if(data.start_hour != '00' && on_air == true ){
		
			var host = {host_name: data.host_name, host_image: data.host_image, host_url: data.host_url};
		
			return host;
			
		}else{

			return false;
			
		}
			
	}
	
	function addZero(i) {
		if (i < 10) {
			i = '0' + i;
		}
		return i;
	}
	
	function is_on_air_now(now_time, start_time, end_time){
				
		if(now_time >= start_time && now_time <= end_time){
			
			return true;
			
		}else{
			
			return false;
			
		}
		
	}
	
	function display_host(host){
		
		jQuery('#on-air-plugin-url').attr('href',host.host_url);
		jQuery('#on-air-plugin-host-name').text(host.host_name);
		jQuery('#on-air-plugin-image').attr('src',host.host_image);
		jQuery('#on-air-plugin-image').attr('alt','103.5 The Arrow - ' + host.host_name);
		jQuery('#on-air-plugin-image-url').attr('href',host.host_url);
		
		jQuery('#on-air-image').fadeToggle('slow','linear');
		
	}
	";
	
	$output .= "</script>";
    
	$output .= '<h4 class="listen-header">On-Air - <span style="font-size: 16px;"><a id="on-air-plugin-url" href="/musicjocks/"><span id="on-air-plugin-host-name">103.5 The Arrow</span></a></span></h4>
<span id="on-air-image" style="display:none;"><a id="on-air-plugin-image-url" href="/musicjocks/"><img id="on-air-plugin-image" style="height: 175px;" src="'.$default_host_image.'" alt="103.5 The Arrow" /></a></span>';

	return $output;
	
}

add_shortcode( 'on_air', 'on_air' );





?>
