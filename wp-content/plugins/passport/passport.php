<?php
/*
Plugin Name: Passport to Family Fun
Plugin URI: http://fm100.com
Description: Plugin that imports and displays posts from the custom post type passport.
Author: Doug Bailey
Version: 1.0.1
Author URI: http://bonneville.com
*/
class passport extends WP_Widget {
    
    function display( $atts ) {
            
        global $wpdb;
            
        $query = $wpdb->get_results("SELECT * FROM wp_posts WHERE `post_type` = 'passport' AND `post_status` = 'publish'");
        
        $thirds = 1;
        $i=0;
        
        foreach($query as $post){
		
            $post->meta = get_post_meta($post->ID);
            $post->terms = get_the_terms($post->ID, 'passports');
	    
	    	unset($terms);

	    	foreach($post->terms as $term){
		
				$terms[] = $term->name;	
		
	    	}

            if(in_array($atts['category'], $terms)){
                
                $i++;
                
                if ( has_post_thumbnail($post->ID) ) {
                    $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                    $image = '<a href="' . $large_image_url[0] . '" title="' . $post->post_title . '" >';
                    $image .= get_the_post_thumbnail( $post->ID, 'thumbnail' ); 
                    $image .= '</a>';
                }
                
                $content[$i] .= $image . "<h2><a href='".$post->meta['fm100_passport_link'][0]."'>" . $post->post_title . "</a></h2>" . $post->post_content . "<br><br><br>";
                
                if($thirds == 1){
                    $outbody .= do_shortcode("[wolf_col_4 class='first']" . $content[$i] . "[/wolf_col_4]");
                }elseif($thirds == 2){
                    $outbody .= do_shortcode("[wolf_col_4]" . $content[$i] . "[/wolf_col_4]");
                }elseif($thirds == 3){
                    $outbody .= do_shortcode("[wolf_col_4 class='last']" . $content[$i] . "[/wolf_col_4]");
                }
                
                if($thirds == 3){
                    $thirds = 1;
                }else{
                    $thirds++;
                }
                
            }
         
        }
        
        $outbody .= "<div style='clear:both'>&nbsp;</div>";
        
        return $outbody;
	
    }
 

}
 
    add_shortcode( 'passport_display', array( 'passport' , 'display' ) );
 
 ?>