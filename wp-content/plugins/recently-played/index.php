<?php
/**
 * @package Recently_Played
 * @version 1.6
 */
/*
Plugin Name: Recently Played
Plugin URI: 
Description: This is a plugin that pulls the last 3 songs played and creates a page of recently played songs
Author: Doug Bailey
Version: 1.2.1
Author URI: http://bonneville.com
*/


function get_recently_played_header(){
    
   //global $wpdb;
   
   $str = file_get_contents('http://wires.bonnint.com/slc/playlist.php?station=krsp');
   
   //$now_playing = $wpdb->get_results("SELECT * FROM `wp_playlist` WHERE `title` != 'ARROW TRAFFIC' AND `artist` != 'BC' AND `artist` != 'VC' AND `artist` != 'GT' AND `artist` != 'KRSP(HD)' AND `artist` != 'BC/VC' AND `artist` != 'ED' AND `artist` != 'ST' ORDER BY playlist_id DESC LIMIT 4", ARRAY_A);
   
   //die(json_encode($now_playing));
   die($str);
    
}

function get_recently_played_page(){
    
   //global $wpdb;
   
   $str = file_get_contents('http://wires.bonnint.com/slc/playlist.php?station=krsp&page=1');
   
   //$now_playing = json_decode($str);
   
   //$now_playing = $wpdb->get_results("SELECT * FROM `wp_playlist` WHERE `title` != 'ARROW TRAFFIC' AND `artist` != 'BC' AND `artist` != 'VC' AND `artist` != 'GT' AND `artist` != 'KRSP(HD)' AND `artist` != 'BC/VC' AND `artist` != 'ED' AND `artist` != 'ST' ORDER BY playlist_id DESC LIMIT 500", ARRAY_A);
   
   
   
   /*foreach($now_playing as $song){
    
	if($song['hour'] > 12){
	    
	    $new_hour = $song['hour'] - 12;
	    $am_pm = 'PM';
	    
	}
	
	$display = $song['month'] . "/" . $song['day'] . "/" . $song['year'] . " " . $new_hour . ":" . $song['minute'] . " " . $am_pm;
	    
	$song['display_time'] = $display;
    
	$new_data[] = $song;
    
   }*/
   
   
   
   die($str);
    
}

function recently_played_header_output(){
	
	$output = "<style>.title-list li{ list-style-type:none; font-family: 'OstrichSansMedium'; font-size:18px; }
	.small-title{ font-size:16px; }
	</style>";
	
	$output .= "<script type='text/javascript'>";
	
	$output .= "jQuery(document).ready(function() { ";
	
	$ajax_url = admin_url( 'admin-ajax.php' );
	
	$output .= "jQuery.ajax({
				url:'" . $ajax_url . "',
				dataType:'json',
				method: 'POST',
				data: {
					action: 'bonnevilleRecentlyPlayedHeader'
				},
				success: function(data){
					process_recently_played(data);
				},
				error: function(){
					console.log('no data retrieved from ".$ajax_url."');
				}
		});
		
	});";
	
	$output .= "function process_recently_played(data){

		for(i = 0; i < 3; i++){
		    s = i+1;
		    jQuery('#song-title-' + i).text(data[s].title);
		    jQuery('#song-artist-' + i).text(data[s].artist);
		
		}
		
		jQuery('.title-list').fadeToggle('slow','linear');
	}";
	
	$output .= "</script>";
	
	$output .= "<a href='/recently-played/'><h4 class='listen-header'>Recently Played</h4></a>";
	
	$output .= "<ul class='title-list' style='display:none;'>";
	
	for($i=0;$i<=2;$i++){
	
		$output .= "<li><span id='song-title-$i'>TITLE</span> - <span class='small-title' id='song-artist-$i'>ARTIST</span></li>";
	
	}
	
	$output .= "<li><a href='/recently-played/'>More...</a></li>";
	
	$output .= "</ul>";

    return $output;
    
}

function recently_played_page_output($songs){
    
	$current_day = date('d');
	
	$output = "<style>.title-list li{ list-style-type:none; font-family: 'OstrichSansMedium'; font-size:22px; }
	.small-title{ font-size:16px; }
	</style>";
	
	$output .= "<script type='text/javascript'>";
	
	$output .= "jQuery(document).ready(function() { ";
	
	$ajax_url = admin_url( 'admin-ajax.php' );
	
	$output .= "jQuery.ajax({
				url:'" . $ajax_url . "',
				dataType:'json',
				method: 'POST',
				data: {
					action: 'bonnevilleRecentlyPlayedPage'
				},
				success: function(data){
					process_recently_played_page(data);
				},
				error: function(){
					console.log('no data retrieved from ".$ajax_url."');
				}
		});
		
	});";
	
	$output .= "function process_recently_played_page(data){
	
		for(n = 0; n < data.length; n++){
		    //console.log(data[n]);
		    jQuery('#song-title-page-' + n).text(data[n].title);
		    jQuery('#song-artist-page-' + n).text(data[n].artist);
		    jQuery('#song-time-page-' + n).text(data[n].display_time);
		
		}
		
	}";
	
	$output .= "</script>";
	
	$output .= "<h3 class='listen-header'>Recently Played</h3>";
	
	$output .= "<ul class='title-list' style='list-style-type:none;'>";

	for($i=0;$i<500;$i++){
	
	    $output .= "<li style='list-style-type:none;'><span id='song-time-page-".$i."'>time</span> - <span id='song-title-page-".$i."'>title</span> - <span class='small-title' id='song-artist-page-".$i."'>artist</span></li>";
	
	}
	
	$output .= "</ul>";

    return $output;
    
}

function recently_played_header_shortcode ( ){

    $output = recently_played_header_output();
    
    return $output;

}

function recently_played_page_shortcode ( ){

    $output = recently_played_page_output($data);
    
    return $output;

}

add_action( 'wp_ajax_bonnevilleRecentlyPlayedHeader', 'get_recently_played_header' );
add_action( 'wp_ajax_nopriv_bonnevilleRecentlyPlayedHeader', 'get_recently_played_header' );

add_action( 'wp_ajax_bonnevilleRecentlyPlayedPage', 'get_recently_played_page' );
add_action( 'wp_ajax_nopriv_bonnevilleRecentlyPlayedPage', 'get_recently_played_page' );

add_shortcode( 'recently_played_list', 'recently_played_page_shortcode');
add_shortcode( 'recently_played', 'recently_played_header_shortcode' );

?>
