<?php
/*
Plugin Name: Media Upload Filter
Description: Filters upload url path to allow for a custom image url
Version: 0.1
Author: Nathan Knowles
*/

/**
 * @version 0.1
 */
add_filter( 'pre_option_upload_url_path', 'media_lib_upload_url', 1 );

function media_lib_upload_url()
{
    if( defined( 'MEDIA_UPLOAD_URL' ) )
        return MEDIA_UPLOAD_URL;
}
