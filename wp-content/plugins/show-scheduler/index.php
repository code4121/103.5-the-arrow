<?php
/*
Plugin Name: Show Scheduler 
Plugin URI: http://docs.bonnint.com/listinator/
Description: A Plugin that allows for a show schedule to be entered and then saved to the database to run on-air and programming blocks on the site.
Version: 1.5
Author: Doug Bailey
Author URI: http://bonneville.com
License: GPL2
*/

class show_scheduler extends WP_Widget {

    private $basepath;
	protected $form;

    // constructor
    function show_scheduler() {
        parent::WP_Widget(false, $name = __('Show Scheduler', 'show_scheduler') );
        $this->basepath = plugin_dir_path(__FILE__);
	//ajax search in the admin
	//add_action('wp_ajax_listinator_search', array($this,'listinator_search'));
	//if (!defined('BONNEVILLE_FRAMEWORK') && is_admin()){
	//echo "A Penny For Your Thoughts Guvnah!";
    }
}


function scheduler_options_page() {
    
    echo load_javety_scripts();
    
    echo '<div class="wrap">
	
	<h2>Show Scheduler</h2>

	<form method="post" action="options.php" novalidate="novalidate">
	<!--<input type="hidden" name="option_page" value="show_scheduler" name="action" value="update">
	<input type="hidden" id="_wpnonce" name="_wpnonce" value="f209a77ef3">
	<input type="hidden" name="_wp_http_referer" value="/wp-admin/options-general.php">-->
	<table class="form-table" id="show-table">
	    <tbody><tr>';
	    echo input_form();
	    
    echo '</tbody></table>

	<a href="#" onclick="addRow(\'show-table\')">ADD ROW</a>
    
	<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p></form>

    </div>';

}

function load_javety_scripts(){
    
    $JSON = plugin_dir_path( __FILE__ ) . "schedules.json";
    
    $data = json_decode(file_get_contents($JSON));
    
    print_r($data);
    
    $outbody = '<script type="text/javascript">
    
    
    
    </script>';
    
    return $outbody;
    
}

function input_form(){
    $outbody = '
<th scope="row"><label for="showname">Show Title</label></th>
<td><input name="showname" type="text" id="showname" value="" class="regular-text"></td>
</tr>
<tr>
<th scope="row"><label for="showdescription">Show Description</label></th>
<td><input name="showdescription" type="text" id="showdescription" value="" class="regular-text">
<p class="description">In a few words, explain what this show is about.</p></td>
</tr>
<tr>
<th scope="row"><label for="showurl">Show URL</label></th>
<td><input name="showurl" type="url" id="showurl" value="" class="regular-text code"></td>
</tr>
<tr>
<th scope="row"><label for="showsocialsoup">Show Social URLs</label></th>
<td><input name="showsocialsoup" type="text" id="showsocialsoup" value="" class="regular-text">
<p class="description">Enter each social media url followed by a comma (,) the correct icons will be associated based on the url.</p></td>
</tr>
<tr>
<th scope="row"><label for="home">Host(s)</label></th>
<td><input name="hosts" type="text" id="hosts" value="" class="regular-text">
<p class="description">this will be interesting to do...</p></td>
</tr>
<tr>
<th scope="row">Membership</th>
<td> <fieldset><legend class="screen-reader-text"><span>Membership</span></legend><label for="users_can_register">
<input name="users_can_register" type="checkbox" id="users_can_register" value="1">
Anyone can register</label>
</fieldset></td>
</tr>';
    
    return $outbody;
}

//add the configs page
add_action('admin_menu', 'scheduler_admin_add_page');

function scheduler_admin_add_page() {
add_options_page('Program Schedule', 'Schedule', 'manage_options', 'show_scheduler', 'scheduler_options_page');
}