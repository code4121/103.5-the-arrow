<?php

namespace onDemand;

/*
Plugin Name: On Demand Audio Tools
Description: Connects multiple widgets for using on demand audio
Author: Nathan Knowles
Version: 1.0.0
Author URI: http://bonneville.com
License: GPL2
*/

define('ONDEMAND_VERSION', '0.6');
define('ONDEMAND_PATH', dirname(__FILE__));
define('ONDEMAND_URL', trailingslashit(plugins_url('', __FILE__)));
define('ONDEMAND_TEMPLATES', ONDEMAND_PATH . '/views/');
define('ONDEMAND_ASSETS', ONDEMAND_PATH . '/assets/');
define('ONDEMAND_LIB', ONDEMAND_PATH . '/lib/');
define('ONDEMAND_COMMON', ONDEMAND_PATH . '/common/');
define('ONDEMAND_NOONCE', '9999737158');

if (!defined('AUDIO_LIB_DB_USER')) {
    define('AUDIO_LIB_DB_USER', 'aurum');
}

if (!defined('AUDIO_LIB_DB_PASSWORD')) {
    define('AUDIO_LIB_DB_PASSWORD', 'nostrom');
}

if (!defined('AUDIO_LIB_DB_NAME')) {
    define('AUDIO_LIB_DB_NAME', 'az-audio');
}

if (!defined('AUDIO_LIB_DB_HOST')) {
    define('AUDIO_LIB_DB_HOST', 'mysql-master1.bonnint.net');
}

if (!defined('AUDIO_LIB_STATION_ID')) {
    define('AUDIO_LIB_STATION_ID', 1);
}
/**
 * Class Bootstrap
 *
 * @author Nathan Knowles <nknowles@bonneville.com>
 */
class Bootstrap
{

    public function __construct()
    {
        require_once ONDEMAND_COMMON . "daily-audio-roll.php";
        require_once ONDEMAND_COMMON . "player.php";
        require_once ONDEMAND_COMMON . "show-list.php";
        require_once ONDEMAND_COMMON . "all-stations-show-list.php";
        require_once ONDEMAND_COMMON . "section-audio.php";
        require_once ONDEMAND_COMMON . "section-audio-winners.php";
        //require_once ONDEMAND_COMMON . "hp-ondemand-audio.php";
        require_once ONDEMAND_COMMON . "on-demand-page.php";
        require_once ONDEMAND_COMMON . "hooks.php";
        require_once ONDEMAND_LIB . "relatedAudioXML.php";
        require_once ONDEMAND_COMMON . "show-page.php";
        require_once ONDEMAND_COMMON . "show-nav-menu.php";
        require_once ONDEMAND_COMMON . "podcastPlayer.php";
    }
}

// All of our widgets will require this..
require_once ONDEMAND_LIB . "OnDemand.php";
require_once ONDEMAND_LIB . "View.php";
require_once ONDEMAND_LIB . "Audio_library.php";
require_once ONDEMAND_LIB . "AudioLibraryREST.php";



$Bootstrap = new \onDemand\Bootstrap();

register_activation_hook(__FILE__, ['onDemand\Hooks','activation']);
