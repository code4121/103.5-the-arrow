<?php namespace onDemand;

/**
 * Class View
 *
 * @author Nathan Knowles <nknowles@bonneville.com>
 */
Class View
{

    // Perhaps this should be a config Option.
    protected $nonceID;

    public function __construct($nonceID)
    {
        $this->nonceID = $nonceID;
    }

    /*
     * Setter for templatePath
     */
    public function setTemplatePath($templatePath)
    {
        $viewPath = $templatePath;// . DIRECTORY_SEPARATOR;

        if (file_exists($viewPath)) {
            $this->templatePath = $viewPath;
            return $this;
        }

        wp_die( __( "Template Path ({$viewPath}) does not seem to exist" ) );
    }

    /*
     * Getter for templatePath
     */
    public function getTemplatePath()
    {
        return $this->templatePath;
    }

    /**
     * Render A given Template Part
     *
     * @param string $template The name of the template to render
     * @param mixed $vars One or more variables to use in the template
     * @since v0.1
     */
    public function render( $template_view, $vars ) {

        if (is_array( $vars )) {
            extract($vars);
        }

        include  $this->getTemplatePath() . $template_view . '.php';

    }

}



