<?php

namespace onDemand;

/**
 * Class OnDemand
 * @author Steele Nelson <snelson21@gmail.com>
 */

class OnDemand
{
    public $show_lookup_table = 'on_demand_show_lookup';
    private static $instance;
    public static function singleton()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct()
    {
        global $table_prefix;
        $this->show_lookup_table = $table_prefix . $this->show_lookup_table;
    }

    public function maybeCreateTable()
    {
        $sql = "CREATE TABLE %s.%s (
            term_id bigint(20) NOT NULL,
            show_id bigint(20) NOT NULL,
            PRIMARY KEY (term_id)
        );";

        dbDelta(sprintf($sql, DB_NAME, $this->show_lookup_table));
    }

    public function showIdForCategory($categoryId)
    {
        global $wpdb;
        $sql = "SELECT show_id FROM {$this->show_lookup_table} WHERE term_id = %d";
        $result = $wpdb->get_var($wpdb->prepare($sql, $categoryId), 0, 0);
        error_log("This is the showIdForCategory result ***: " . $result );
        return $result;

    }

}
