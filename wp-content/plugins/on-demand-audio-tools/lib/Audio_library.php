<?php namespace onDemand;

/**
 * Class Audio_library
 *
 * @author Nathan Knowles <nknowles@bonneville.com>
 */
Class Audio_library
{
    Protected $station_id;
    Protected $api_url = "http://az-audio-api.bonnint.com/";

    Protected $db;

    public function __construct($db_user, $db_password, $db_host, $db_name, $station_id)
    {
        $this->station_id = (int) $station_id;
    }

    public function getPlayList($show_id, $audio_id, $search_date)
    {
        $url = $this->api_url . "?type=playlist&id={$show_id}&audio_id={$audio_id}&search_date={$search_date}&station_id={$this->station_id}";
        $raw_data = bonneville_file_get_contents($url);
        $data = json_decode($raw_data);
        return $data;
    }

    public function getRelated($show_id, $audio_id, $search_date)
    {
        $url = $this->api_url . "?type=related&id={$show_id}&audio_id={$audio_id}&search_date={$search_date}&station_id={$this->station_id}";
        $raw_data = bonneville_file_get_contents($url);
        $data = json_decode($raw_data);
        return $data;
    }

    public function getShowList()
    {
        $url = $this->api_url . "?type=showlist";
        $raw_data = bonneville_file_get_contents($url);
        $data = json_decode($raw_data);
        return $data;
    }

    public function pullAudioList($show_id, $limit, $type, $tags)
    {
        switch ($type)
        {
            case 'fullshow':
                return $this->pullShowList($show_id, $limit);
                break;
            case 'showsegments':
                return $this->pullSegmentList($show_id, $limit);
                break;
            case 'showwithtags':
                return $this->pullShowTagsList($show_id, $tags, $limit);
                break;
            case 'tags':
                return $this->pullTagsList($show_id, $tags, $limit);
                break;
            default:
                return $this->pullShowList($show_id, $limit);
        }

    }

    public function pullAudioclips($audio_ids = array(), $limit = 1)
    {
        $clipList = implode(',', $audio_ids);

        $url = $this->api_url . "?type=clips%limit={$limit}&clipList={$clipList}";
        $raw_data = bonneville_file_get_contents($url);
        $data = json_decode($raw_data);
        return $data;
    }

    public function pullShowRange($show_id, $start, $end)
    {
        $url = $this->api_url . "?type=showrange&id={$show_id}&start={$start}&end={$end}&station_id={$this->station_id}";
        $raw_data = bonneville_file_get_contents($url);
        $data = json_decode($raw_data);
        return $data;
    }


    public function pullShowList($show_id, $limit)
    {
        $url = $this->api_url . "?type=fullshow&id={$show_id}&limit={$limit}&station_id={$this->station_id}";
        $raw_data = bonneville_file_get_contents($url);
        $data = json_decode($raw_data);
        return $data;
    }

    public function pullSegmentList($show_id, $limit)
    {
        $url = $this->api_url . "?type=showsegments&id={$show_id}&limit={$limit}&station_id={$this->station_id}";
        $raw_data = bonneville_file_get_contents($url);
        $data = json_decode($raw_data);
        return $data;
    }

    public function pullTagsList($show_id, $tags, $limit)
    {
        $url = $this->api_url . "?type=tags&id={$show_id}&limit={$limit}&tags={$tags}&station_id={$this->station_id}";
        $raw_data = bonneville_file_get_contents($url);
        $data = json_decode($raw_data);
        return $data;
    }

    public function pullShowTagsList($show_id, $tags, $limit)
    {
        $url = $this->api_url . "?type=showwithtags&id={$show_id}&limit={$limit}&tags={$tags}&station_id={$this->station_id}";
        $raw_data = bonneville_file_get_contents($url);
        $data = json_decode($raw_data);
        return $data;
    }


    public function getAudio($id)
    {
        $url = $this->api_url . "?type=audio&id={$id}&station_id={$this->station_id}";
        $raw_data = bonneville_file_get_contents($url);
        $data = json_decode($raw_data);
        return $data;
    }

    public function getBydate($date)
    {
    }

    public function getByLatest($limit, $offset)
    {
        $url = $this->api_url . "?type=getByLatest&limit={$limit}&offset={$offset}&station_id={$this->station_id}";
        $raw_data = bonneville_file_get_contents($url);
        $data = json_decode($raw_data);
        return $data;

    }
}
