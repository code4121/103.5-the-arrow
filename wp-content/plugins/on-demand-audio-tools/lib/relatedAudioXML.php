<?php //namespace onDemand;
global $View;
global $Audio;

use onDemand\View as View;
use onDemand\Audio_library as Audio;

//add_action('init', 'feed');

function feed() {
  global $View;
  global $Audio;

  if (defined('ONDEMAND_TEMPLATES')) {
    $View = new View('relatedxml');
      $View->setTemplatePath(ONDEMAND_TEMPLATES);
  } else {
      die('Unabled to setup the view.');
  }

  $Audio = new Audio(
     AUDIO_LIB_DB_USER,
     AUDIO_LIB_DB_PASSWORD,
     AUDIO_LIB_DB_HOST,
     AUDIO_LIB_DB_NAME,
     AUDIO_LIB_STATION_ID
  );


  $name = 'related-audio';
  $registered = false;

  add_feed($name, 'renderfeed');
  $rules = get_option( 'rewrite_rules' );
  $feeds = array_keys( $rules, 'index.php?&feed=$matches[1]' );
   foreach ( $feeds as $feed )
   {
       if ( false !== strpos( $feed, $name ) ) {
           $registered = true;
       }
   }

   // Feed not yet registered, so lets flush the rules once.
   if ( ! $registered )
       flush_rewrite_rules( true );

}

function renderfeed()
{
  global $Audio;
  global $View;
  $audio_id = getAudioID();
  if ($audio_id) {
    $audio = $Audio->getAudio($audio_id);
    $playlist = $Audio->getRelated($audio->showid, $audio->audioid, $audio->datepublished);
    header('Content-Type: application/xml; charset=utf-8');
    echo '<?xml version="1.0" encoding="UTF-8"?>';
    $View->render('Feed/related', array(
        'Audio' => $Audio,
        'playlist' => $playlist,
    ));
  }

}

function getAudioID()
{
  return !empty($_REQUEST['aid']) ? $_REQUEST['aid'] : false;
}
