<?php namespace onDemand;

/**
 * Class AudioLibraryREST
 *
 * @author Steele Nelson <snelson21@gmail.com>
 */

class AudioLibraryREST
{
    protected $ch;
    
    protected $baseUrl = "http://slc-audio-api.bonnint.com";


    public function __construct()
    {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
    }

    public function __destruct()
    {
        curl_close($this->ch);
    }

    protected function url($path)
    {
        return $this->baseUrl . $path;
    }
    /**
    * Retreives all active shows for all active stations.  Stations are ordered by their station id, and 
    * their shows are ordered according to their rank in a descending fashion.
    * @return Array    An array of stations containing their following:
    *                  'stationId'   Int   - identification number for the station
    *                  'stationName' Str   - the title of the station as it appears
    *                  'shows'       Array - array of shows and their own corresponding data.
    */
    public function getAllStationsShows()
    {

        $key = "AllStationsShowsKey";

        //If cache doesn't return false
        
        $result = wp_cache_get( $key, 'Audio_Lib_REST');

        if ( $result !== false) {
            return $result;
        }

        $url = $this->url('/stations/3/shows');
        curl_setopt($this->ch, CURLOPT_URL, $url);
        $result = curl_exec($this->ch);

        // Check for errors and display the error message
        if ($errno = curl_errno($this->ch)) {
            $error_message = curl_strerror($errno);
            error_log("cURL error ({$errno}):\n {$error_message}");
            return [];
        }

        $result = json_decode($result, true);

        $cache_time = (int) 180; //3 mins

        wp_cache_set( $key, $result, 'Audio_Lib_REST', $cache_time );

        return $result;
    }

    /**
     * Retreives the desired show that corresponds to the given showId.
     * @param Int $showId  A unique I.D. for a group of pods that belong to the same show
     * @return Array       An array of show data.
     */
    public function getShowFromId($showId)
    {
        $key = "ShowIdKey" . $showId;

        $result = wp_cache_get( $key, 'Audio_Lib_REST');

        if( $result !== false) {
            return $result;
        }

        $url = $this->url("/shows/$showId");
        curl_setopt($this->ch, CURLOPT_URL, $url);
        $result = curl_exec($this->ch);

        if ($errno = curl_errno($this->ch)) {
            $error_message = curl_sterror($errno);
            error_log("cURL error ({errno}):\n {$error_message}");
            return [];
        }
        $result = json_decode($result, true);

        $cache_time = (int) 180;

        wp_cache_set($key, $result, 'Audio_Lib_REST', $cache_time);
        //error_log("Show from AudioLibREST **** " . print_r($sresult));

        return $result;
    }
    /**
     * Retreives a single Pod from the API that corresponds to the designated audio id.  
     * @param  Int $audio_id The identifier for a single pod / audio file.
     * @return Array a single audio pod containing an array of data.
     */

    public function getPodById($audio_id) {

        $key = "PodByIdKey" . $audio_id;
        $result = wp_cache_get( $key, 'Audio_Lib_REST');

        if( $result !== false) {
            return $result;
        }

        $url = $this->url("/pods/{$audio_id}");
        curl_setopt(($this->ch), CURLOPT_URL, $url);
        $result = curl_exec($this->ch);

        if ($errno = curl_errno($this->ch)) {
            $error_message = curl_strerror($errno);
            error_log("cURL error ({errno}):\n {error_message}" );
            return [];
        }

        $result = json_decode($result, true);

        $cache_time = (int) 180;

        wp_cache_set($key, $result, 'Audio_Lib_REST', $cache_time);

        return $result;

    }

    /**
     * Retreives a playlist of pods that share a unique show id and date they are published on. 
     * IMPORTANT NOTE: The pod that belongs to the given $audio_id will NOT be included in the 
     * returned playlist array of 'pods'.
     * @param Int $showId        A unique I.D. for a group of pods that belong to the same show
     * @param Int $audio_id      The identifier for a single pod / audio file.
     * @param Str $datepublished The date at which the pod/audio was published.
     * @return Array             An array of individual playlist pods. They are Grouped by their published date.
     *                                'datepublished' Date - format: "l, F d, Y" 
     *                                'pods'          Array - contains mutliple pods with their data.
     */

    public function getPodPlaylist($showId, $audio_id, $datepublished) {

        $key = "PodPlaylistKey";
        $result = wp_cache_get( $key, 'Audio_Lib_REST');
        
        if( $result !== false) {
            return $result;
        }

        $url = $this->url("/shows/$showId/pods?filter=date:{$datepublished},ignore_aid:{$audio_id},asc:1");

        curl_setopt($this->ch, CURLOPT_URL, $url);
        $result = curl_exec($this->ch);

        if ($errno = curl_errno($this->ch)) {
            $error_message = curl_strerror($errno);
            error_log("cURL error ({$errno}):\n {$error_message}");
            return [];
        }

        $result = json_decode($result, true);

        $cache_time = (int) 180;

        wp_cache_set($key, $result, 'Audio_Lib_REST', $cache_time);

        return $result;

    }

    /**
     * Retreives the all the active pods contained in a given show.  These pods are grouped and ordered by
     * their published date in descending order.  This is used to display the list of pods for any given show 
     * in the category-podcast-results.php template.
     * @param  Int $showId A unique I.D. for a group of pods that belong to the same show
     * @param  Int $page   A number that represents the current page that is being viewed.
     * @return Array       The array of pod groups (by date published) containing their corresponding pods.
     *                         'datepublished' Date - format: "l, F d, Y" 
     *                         'pods'          Array - contains mutliple pods with their data.
     */
    public function getShowPodcasts($showId, $page)
    {

        $key = "ShowPodcastsKey" . $showId . $page;
        $result = wp_cache_get( $key, 'Audio_Lib_REST');

        if( $result !== false) {
            //error_log("RESULT IS **** : " . print_r($result, TRUE));
            return $result;
        }

        $offset=$page * 20;
       // error_log("Page is {$page}");
        $url = $this->url("/shows/{$showId}/pods?since=$offset");
        curl_setopt($this->ch, CURLOPT_URL, $url);
        $result = curl_exec($this->ch);

        //error_log("Get Show Podcasts raw result is {$result}");

        if ($errno = curl_errno($this->ch)) {
            $error_message = curl_strerror($errno);
            error_log("cURL error ({$errno}):\n {$error_message}");
            return [];
        }


        
        $result = json_decode($result, true);

        $cache_time = (int) 180;

        wp_cache_set($key, $result, 'Audio_Lib_REST', $cache_time);

        // error_log("RESULT From getShowPodcasts IS **** : " . print_r($result, TRUE));
        return $result;
    }

    public function getLastHighlightDate() 
    {
        $key = "LastHighlightDate";
        $result = wp_cache_get($key, 'Audio_Lib_REST');

        if( $result !== false) {
            error_log("WE CACHED!!");
            return $result;

        }
        $url = $this->url("/highlights/last-date-published");
        curl_setopt($this->ch, CURLOPT_URL, $url);
        $result = curl_exec($this->ch);

        if ($error = curl_errno($this->ch)) {
            $error_message = curl_strerror($errno);
            error_log("cURL error ({$errno}): \n {$error_message}");
            return false;
        }

        $result = json_decode($result, true);
        $cache_time = (int) 1800;
        $r = $result['last_date_published'];

        wp_cache_set($key, $r, 'Audio_Lib_REST', $cache_time);
        return $r;


    }


    public function getHighlights($date)
    {
        $key = "HighlightsKey" . $date;
        $result = wp_cache_get( $key, 'Audio_Lib_REST');

        if( $result !== false) {
            return $result;
        }

        error_log("get Highlights offset .. " . $date);
        //$url = $this->url("/highlights/");
        $date != 0 ? $url = $this->url("/highlights/?since={$date}&stationid=2") : $url = $this->url("/highlights/?stationid=2");

        curl_setopt($this->ch, CURLOPT_URL, $url);
        $result = curl_exec($this->ch);

        if ($error = curl_errno($this->ch)) {
            $error_message = curl_strerror($errno);
            error_log("cURL error ({errno}):\n {$error_message}");
            return [];
        }

        $result = json_decode($result, true);
        $cache_time = (int) 180;

        wp_cache_set($key, $result, 'Audio_Lib_REST', $cache_time);
        return $result;
    }

    //These are not used.
    /**
     * Creates a new paging query string from an original query.  The new query provides a default LIMIT, starting with
     * zero.
     * @param  Str $query               The original 'LIMITless' query string.
     * @param  Int $records_per_page    The number of records that will appear per retreival on any given page.
     * @return Str                      A new query string containing the 'LIMIT' information.
     */
     public function paging($query, $records_per_page)
    {
        $starting_position=0;
        if(isset($_GET["page_no"]))
        {
            $starting_position=($_GET["page_no"]-1)*$records_per_page;
        }
        $query2 = $query . " limit $starting_position, $records_per_page";
        return $query2;
    }
    /**
     * Prints the paging information in HTML strings to provide navigation to the user.  This is to be located at the bottom 
     * of the category-podcast-results.php template in the theme.
     * @param  Str $query               The original 'LIMITless' query string.
     * @param  Int $records_per_page    The number of records that will appear per retreival on any given page.
     */
    public function pagingLink($query,$records_per_page)
    {
        $self = $_SERVER['PHP_SELF'];

        $stmt = $this->db->prepare($query);
        $stmt->execute();

        $total_no_of_records = $stmt->rowCount();

        if($total_no_of_records > 0)
        {
            ?><tr><td colspan="3"><?php
            $total_no_of_pages = ceil($total_no_of_records/$records_per_page);
            $current_page = 1;
            if(isset($_GET["page_no"]))
            {
                $current_page = $_GET["page_no"];
            }
            if($current_page != 1)
            {
                $previous = $current_page-1;
                echo "<a href='". $self . "?page_no=1'>First</a>&nbsp;&nbsp;";
                echo "<a href='" . $self . "?page_no=" . $previous . "'>Previous</a>&nbsp;&nbsp;";
            }
            for($i=1; $i<=$total_no_of_pages;$i++)
            {
                if($i == $current_page)
                {
                    echo "<strong><a href='" . $self . "?page_no=" . $i . "' style= 'color:red;text-decoration:none'>" . $i . "</a></strong>&nbsp;&nbsp;";

                }
                else
                {
                    echo "<a href= '" . $self . "?page_no=" . $i . "'> " . $i . "</a> &nbsp;&nbsp;";
                }
            }

            if($current_page!=$total_no_of_pages)
            {
                $next=$current_page+1;
                echo "<a href= '" . $self . "?page_no=" . $next . "'>Next</a>&nbsp;&nbsp;";
                echo "<a href='" . $self . "?page_no=" . $total_no_of_pages . "'>Last</a>&nbsp;&nbsp;";

            }
            ?></td></tr><?php
        }
    }
}
