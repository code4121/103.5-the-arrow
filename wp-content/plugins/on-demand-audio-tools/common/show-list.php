<?php namespace onDemand;
use onDemand\View as View;
use onDemand\Audio_library as Audio;

Class Show_List extends \WP_Widget
{

    protected $View;
    protected $Audio;
	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
        if (defined('ONDEMAND_TEMPLATES')) {
            $this->View = new View(ONDEMAND_NOONCE);
            $this->View->setTemplatePath(ONDEMAND_TEMPLATES);
        } else {
            wp_die('Unabled to setup the view.');
        }

        try {
            $this->Audio = new Audio(
               AUDIO_LIB_DB_USER,
               AUDIO_LIB_DB_PASSWORD,
               AUDIO_LIB_DB_HOST,
               AUDIO_LIB_DB_NAME,
               AUDIO_LIB_STATION_ID
            );
        } catch (Exception $e) {
            var_dump($e);
        }

        parent::__construct(
        	'ShowList', // Base ID
			__( 'Show List', 'onDemand' ), // Name
			array( 'description' => __( 'Lists the audio shows', 'onDemand' ), ) // Args
        );

	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
    public function widget( $args, $instance ) {

        $showsPage = ! empty( $instance['showsPage'] ) ? $instance['showsPage'] : __( '', 'onDemand' );
        $show = $this->Audio->getShowList();

        $this->View->render('Widget/showList', array(
            'shows' => $show,
            'showsPage' => $showsPage
        ));
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
        // outputs the content of the widget
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( '', 'onDemand' );
        $form_id = ! empty( $instance['form_id'] ) ? $instance['form_id'] : __( '', 'onDemand' );
        $showsPage = ! empty( $instance['showsPage'] ) ? $instance['showsPage'] : __( '', 'onDemand' );

        $this->View->render('Form/showList', array(
            'title' => $title,
            'widget' => $this,
            'form_id' => $form_id,
            'showsPage' => $showsPage,
        ));
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ! empty( $new_instance['title'] ) ? $new_instance['title'] : __( '', 'onDemand' );
        $instance['form_id'] = ! empty( $new_instance['form_id'] ) ? $new_instance['form_id'] : __( '', 'onDemand' );
        $instance['showsPage'] = ! empty( $new_instance['showsPage'] ) ? $new_instance['showsPage'] : __( '', 'onDemand' );

        return $instance;
	}
}

add_action( 'widgets_init', function(){
     register_widget( 'onDemand\Show_List' );
});
