<?php namespace onDemand;
use onDemand\View as View;
use onDemand\Audio_library as Audio;

Class On_Demand_Page extends \WP_Widget
{

    protected $View;
    protected $Audio;
	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
        if (defined('ONDEMAND_TEMPLATES')) {
            $this->View = new View(ONDEMAND_NOONCE);
            $this->View->setTemplatePath(ONDEMAND_TEMPLATES);
        } else {
            wp_die('Unabled to setup the view.');
        }

        try {
            $this->Audio = new Audio(
               AUDIO_LIB_DB_USER,
               AUDIO_LIB_DB_PASSWORD,
               AUDIO_LIB_DB_HOST,
               AUDIO_LIB_DB_NAME,
               AUDIO_LIB_STATION_ID
            );
        } catch (Exception $e) {
            var_dump($e);
        }

        parent::__construct(
        	'OnDemandPage', // Base ID
			__( 'On Demand Page', 'onDemand' ), // Name
			array( 'description' => __( 'A Widget to display on demand page data', 'onDemand' ), ) // Args
        );

        add_filter( 'query_vars', array($this, 'add_query_vars_filter') );
        add_action( 'wp_enqueue_scripts', array($this, 'enqueue') );

	}

    public function enqueue($param)
    {

    }


	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
        global $wp;

        $pageNumber = get_query_var( 'pg',  0 );

        $show_id = ! empty( $instance['show_id'] ) ? $instance['show_id'] : __( '', 'onDemand' );
        $perPageCount = ! empty( $instance['stories_per_page'] ) ? $instance['stories_per_page'] : __( '', 'onDemand' );

        $player = isset($instance['player']) ? $instance['player'] : '/player';
        $analyticsUrl = isset($instance['analyticsUrl']) ? $instance['analyticsUrl'] : 'icestream.bonnint.net';

        $paged = $pageNumber * $perPageCount;

        $shows = $this->Audio->pullShowRange($show_id, $paged, $perPageCount);
        if ( 0 < count($shows) ) {
            $title = $shows[0]->showname;
            $image = $shows[0]->showimage;
            $description =$shows[0]->showdescription;
        }

        $next = home_url(add_query_arg('pg', $pageNumber + 1, $wp->request));
        $prev = home_url(add_query_arg('pg', $pageNumber - 1, $wp->request));

        $this->View->render('Widget/onDemandPage', array(
            'title' => $title,
            'description' => $description,
            'image' => $image,
            'player' => $player,
            'shows' => $shows,
            'pageNumber' => $pageNumber,
            'oldurl' => "icestream.bonnint.net",
            'analyticsUrl' => $analyticsUrl,
            'next' => $next,
            'prev' => $prev
        ));
	}

    public function add_query_vars_filter( $vars ){
      $vars[] = "pg";
      return $vars;
    }

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
        // outputs the content of the widget
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( '', 'onDemand' );
        $show_id = ! empty( $instance['show_id'] ) ? $instance['show_id'] : __( '', 'onDemand' );
        $perPageCount = ! empty( $instance['stories_per_page'] ) ? $instance['stories_per_page'] : __( '', 'onDemand' );
        $form_id = ! empty( $instance['form_id'] ) ? $instance['form_id'] : __( '', 'onDemand' );
        $analyticsUrl = isset($instance['analyticsUrl']) ? $instance['analyticsUrl'] : '';
        $player = isset($instance['player']) ? $instance['player'] : '';

        $this->View->render('Form/onDemandPage', array(
            'title' => $title,
            'show_id' => $show_id,
            'perPageCount' => $perPageCount,
            'analyticsUrl' => $analyticsUrl,
            'player' => $player,
            'widget' => $this,
            'form_id' => $form_id,
        ));
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ! empty( $new_instance['title'] ) ? $new_instance['title'] : __( '', 'onDemand' );
        $instance['show_id'] = ! empty( $new_instance['show_id'] ) ? $new_instance['show_id'] : __( '', 'onDemand' );
        $instance['stories_per_page'] = ! empty( $new_instance['stories_per_page'] ) ? $new_instance['stories_per_page'] : __( '', 'onDemand' );
        $instance['form_id'] = ! empty( $new_instance['form_id'] ) ? $new_instance['form_id'] : __( '', 'onDemand' );

        $instance['player'] = ! empty( $new_instance['player'] ) ? $new_instance['player'] : __( '', 'onDemand' );

        $instance['analyticsUrl'] = ! empty( $new_instance['analyticsUrl'] ) ? $new_instance['analyticsUrl'] : __( '', 'onDemand' );
        $instance['analyticsUrl'] = str_replace("http://", '', $instance['analyticsUrl']);
        $instance['analyticsUrl'] = str_replace('/', '', $instance['analyticsUrl']);

        return $instance;
	}
}

add_action( 'widgets_init', function(){
     register_widget( 'onDemand\On_Demand_Page' );
});
