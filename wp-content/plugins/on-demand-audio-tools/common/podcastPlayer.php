<?php //namespace onDemand;
use onDemand\View as View;
use onDemand\AudioLibraryREST as Audio;


Class PodcastPlayer extends \WP_Widget
{

    protected $View;
    protected $Audio;
	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
        if (defined('ONDEMAND_TEMPLATES')) {
            $this->View = new View(ONDEMAND_NOONCE);
            $this->View->setTemplatePath(ONDEMAND_TEMPLATES);
        } else {
            wp_die('Unabled to setup the view.');
        }

        try {
            $this->Audio = new Audio(
            );
        } catch (Exception $e) {
            var_dump($e);
        }

        parent::__construct(
        	'PodcastPlayer', // Base ID
			__( 'Podcast Audio Player', 'onDemand' ), // Name
			array( 'description' => __( 'A Widget to display the jwplayer for podcasts', 'onDemand' ), ) // Args
        );

        add_filter( 'query_vars', array($this, 'add_query_vars_filter') );
        add_action( 'wp_enqueue_scripts', array($this, 'enqueue') );

	}

    public function enqueue($param)
    {
        wp_enqueue_script(
	    	   'jwplayer',
    		   'http://jwpsrv.com/library/hpH9Jq1TEeSPjQp+lcGdIw.js',
           null,
           '2.6',
           false
    	  );

        wp_enqueue_style(
            'font-awesome',
            'http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css',
            null,
            '4.0.3',
            'all'
        );


    }

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
        global $wp;

	     $audio_id = get_query_var( 'a',  false );
        if (!$audio_id) {
          $audio_id = isset($instance['aid']) ? $instance['aid'] : '';

        }

        $audio_url = !empty($instance['audio_url']) ? $instance['audio_url'] : false;
       // error_log("THE audio_url VAR IS  : ****** " . print_r($audio_url, true));
        $audio_title = !empty($instance['audio_title']) ? $instance['audio_title'] : false;
        $audio_desc = !empty($instance['audio_desc']) ? $instance['audio_desc'] : false;

        if (!$audio_id and !$audio_url) {
            echo "<h3>Audio Not Found</h3>";
        }


        $analyticsUrl = isset($instance['analyticsUrl']) ? $instance['analyticsUrl'] : '';

        $image = isset($instance['image']) ? $instance['image'] : false;

        $current_url = home_url(add_query_arg('a', $audio_id, $wp->request));



        if ($audio_url) {
	        $audio = new \stdClass();
	        $audio->showname = '';
	        $audio->title = $audio_title;
	        $audio->hideDate = false;
	        $audio->fulldescription = $audio_desc;
            $audio->datepublished = '';
	        $audio->filename = $audio_url;
	        $audio->showimage = $image;
	        $playList_XML = '';
	        $inStory = true;
        } else {
          //error_log("I'm in here ********************************************");

          $audio = $this->Audio->getPodById($audio_id);

          //error_log("ARRAY is for AUDIO : ___ " . print_r($audio, true));
          //$playlist = $this->Audio->getPodPlaylist($audio->showid, $audio->audioid, $audio->datepublished);
          $playlist = $this->Audio->getPodPlaylist($audio['showid'], $audio['audioid'], $audio['datepublished']);
          // error_log("AUDIO PLAYLIST IS ".print_r($playlist, true));


          $playList_XML = get_site_url() . "/related-audio?aid={$audio_id}";
          $inStory = false;
        }

        $audio = json_decode(json_encode($audio),true);

        if (!empty($analyticsUrl)) {
            $audio['filename']= str_replace("icestream.bonnint.net", $analyticsUrl, $audio['filename']);

        }

        $audio['hideDate'] = false;
        if (stristr($audio['title'], 'hour') === false) {
            $audio['hideDate'] = true;
        }
        $logo = plugins_url('assets/img/logo.png', ONDEMAND_ASSETS);

      //  $playlist = $this->Audio->getRelated($audio->showid, $audio->audioid, $audio->datepublished);

        $this->View->render('Widget/podcastPlayer', array(
            'inStory' => $inStory,
            'current_url' => $current_url,
            'audio' => $audio,
            'analyticsUrl' => $analyticsUrl,
            'playlist' => $playlist,
            'feed' => $playList_XML,
            'image' => $image,
            'logo' => $logo
        ));
	}

    public function add_query_vars_filter( $vars ){
      $vars[] = "a";
      return $vars;
    }

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
        // outputs the content of the widget
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( '', 'onDemand' );
        $analyticsUrl = ! empty( $instance['analyticsUrl'] ) ? $instance['analyticsUrl'] : __( '', 'onDemand' );
        $form_id = ! empty( $instance['form_id'] ) ? $instance['form_id'] : __( '', 'onDemand' );

        $this->View->render('Form/podcastPlayer', array(
            'title' => $title,
            'analyticsUrl' => $analyticsUrl,
            'widget' => $this,
            'form_id' => $form_id,
        ));
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ! empty( $new_instance['title'] ) ? $new_instance['title'] : __( '', 'onDemand' );
        $instance['analyticsUrl'] = ! empty( $new_instance['analyticsUrl'] ) ? $new_instance['analyticsUrl'] : __( '', 'onDemand' );
        $instance['form_id'] = ! empty( $new_instance['form_id'] ) ? $new_instance['form_id'] : __( '', 'onDemand' );
        $instance['analyticsUrl'] = str_replace("http://", '', $instance['analyticsUrl']);
        $instance['analyticsUrl'] = str_replace('/', '', $instance['analyticsUrl']);

        return $instance;
	}
}


function PodcastPlayer_shortcode( $atts ) {

    $a = shortcode_atts( array(
        'aid' => false,
        'audio_url' => false,
        'audio_title' => false,
        'audio_desc' => false,
        'analyticsUrl' => 'audio.arizonasports.com',
        'image' => false,
    ), $atts );

    if (!$a['audio_url']) {
      $a['audio_url'] = get_post_meta(get_the_ID(), '_audio', true);
      $a['audio_title'] = get_post_meta(get_the_ID(), '_audio_title', true);
      $a['audio_desc'] = get_post_meta(get_the_ID(), '_audio_desc', true);

      if (!$a['audio_title']) {
          $a['audio_title'] = '';
      }

      if (!$a['audio_desc']) {
          $a['audio_desc'] = '';
      }
    }

    the_widget('PodcastPlayer', $a);

}
add_shortcode( 'audio_player', 'PodcastPlayer_shortcode' );

add_action( 'widgets_init', function(){
     register_widget( 'PodcastPlayer' );
});
