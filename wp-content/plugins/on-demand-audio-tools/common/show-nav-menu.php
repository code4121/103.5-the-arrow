<?php

namespace onDemand;

use onDemand\View as View;
use onDemand\AudioLibraryREST as Audio;

class ShowNavMenu extends \WP_Widget
{

    protected $View;
    protected $Audio;
    /**
     * Sets up the widgets name etc
     */
    public function __construct()
    {
        if (defined('ONDEMAND_TEMPLATES')) {
            $this->View = new View(ONDEMAND_NOONCE);
            $this->View->setTemplatePath(ONDEMAND_TEMPLATES);
        } else {
            wp_die('Unabled to setup the view.');
        }

        try {
            $this->Audio = new Audio();
        } catch (Exception $e) {
            var_dump($e);
        }

        parent::__construct(
            'ShowPage', // Base ID
            __('Show Page (Podcasts)', 'onDemand'), // Name
            array('description' => __('Creates a page and lists all the podcasts for a given showId.', 'onDemand'),) // Args
        );

    }

    /**
    * Outputs the content of the widget
    *
    * @param array $args
    * @param array $instance
    */
    public function widget($args, $instance)
    {
        $categoryId = get_cat_id(single_cat_title("", false));
        $showId = OnDemand::singleton()->showIdForCategory($categoryId);
        $pods = $this->Audio->getShowPodcasts($showId);
        $this->View->render('Widget/showPage', array(
            'pods' => $pods
        ));
    }

    /**
    * Outputs the options form on admin
    *
    * @param array $instance The widget options
    */
    public function form($instance)
    {
        // outputs the content of the widget
        $title = ! empty($instance['title']) ? $instance['title'] : __('', 'onDemand');
        $form_id = ! empty($instance['form_id']) ? $instance['form_id'] : __('', 'onDemand');
        $showsPage = ! empty($instance['showsPage']) ? $instance['showsPage'] : __('', 'onDemand');

        $this->View->render('Form/showList', array(
            'title' => $title,
            'widget' => $this,
            'form_id' => $form_id,
            'showsPage' => $showsPage,
        ));
    }

    /**
    * Processing widget options on save
    *
    * @param array $new_instance The new options
    * @param array $old_instance The previous options
    */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = ! empty($new_instance['title']) ? $new_instance['title'] : __('', 'onDemand');
        $instance['form_id'] = ! empty($new_instance['form_id']) ? $new_instance['form_id'] : __('', 'onDemand');
        $instance['showsPage'] = ! empty($new_instance['showsPage']) ? $new_instance['showsPage'] : __('', 'onDemand');

        return $instance;
    }
}

add_action('widgets_init', function() {
    register_widget('onDemand\ShowPage');
});
