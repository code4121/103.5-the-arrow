<?php

namespace onDemand;
//Whole class is no longer needed.
class Hooks
{
    public static function activation()
    {
        $onDemand = OnDemand::singleton();

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
        global $wpdb;

        //$onDemand->maybeCreateTable();
        //$tableName = $onDemand->show_lookup_table;
        
        //Creates the Podcasts Category if it does not exist.

        $podcastsCategoryId = category_exists('Podcasts');
        if($podcastsCategoryId === null) {
            $term = wp_insert_term('Podcasts', 'category', [
                    'slug' => 'podcasts'
            ]);
        }
        //Creates the Podcast Player Category if it does not exist
      
        $podcastPlayerCategoryId = category_exists('Podcast Player');
        if($podcastPlayerCategoryId === null) {
            $term = wp_insert_term('Podcast Player', 'category', [
                    'slug' => 'podcast_player'
            ]);
        }
        //Creates the Podcast Results Category if it does not exist
        
        $podcastResultsCategoryId = category_exists('Podcast Results');
        if($podcastResultsCategoryId === null) {
            $term = wp_insert_term('Podcast Results', 'category', [
                    'slug' => 'podcast_results'
            ]);
        }
        //Creates the Daily Roll Category if it does not exist.

        $dailyRollCategoryId = category_exists('Daily Roll');
        if($dailyRollCategoryId === null) {
            $term = wp_insert_term('Daily Roll', 'category', [
                    'slug' => 'daily_roll'
            ]);
        }
    
        
/*
        //get all shows
        $audio = new AudioLibraryREST();
        $stations = $audio->getAllStationsShows();

        //grabs each individual show
        foreach ($stations as $station) {
            foreach ($station['shows'] as $show) {
                $showCatName = $show['showname'] . ' Podcasts';
                $showSlug = $show['friendlyname'];
                $showId = $show['showid'];

                $categoryId = category_exists($showCatName);
                if ($categoryId === null) {
                    $term = wp_insert_term($showCatName, 'category', [
                        'parent' => $podcastsCategoryId,
                        'slug' => $showSlug
                    ]);

                    $categoryId = $term['term_id'];
                }
                
                $sql =
                    "INSERT INTO {$tableName} (
                        term_id, 
                        show_id
                    ) VALUES (
                        %d, 
                        %d
                    )
                    ON DUPLICATE KEY UPDATE show_id = %d;";
                $wpdb->query($wpdb->prepare($sql, $categoryId, $showId, $showId));
            }
        }
        */
    }

}
