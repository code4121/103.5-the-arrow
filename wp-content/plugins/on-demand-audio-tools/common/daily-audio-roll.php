<?php namespace onDemand;
use onDemand\View as View;
use onDemand\Audio_library as Audio;

Class Daily_Audio_Roll extends \WP_Widget
{

    protected $View;
    protected $Audio;
	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
        if (defined('ONDEMAND_TEMPLATES')) {
            $this->View = new View(ONDEMAND_NOONCE);
            $this->View->setTemplatePath(ONDEMAND_TEMPLATES);
        } else {
            wp_die('Unabled to setup the view.');
        }

        try {
            $this->Audio = new Audio(
               AUDIO_LIB_DB_USER,
               AUDIO_LIB_DB_PASSWORD,
               AUDIO_LIB_DB_HOST,
               AUDIO_LIB_DB_NAME,
               AUDIO_LIB_STATION_ID
            );
        } catch (Exception $e) {
            var_dump($e);
        }

        parent::__construct(
        	'DailyAudioRoll', // Base ID
			__( 'Daily Audio Roll', 'onDemand' ), // Name
			array( 'description' => __( 'A Widget to display the daily Audio Roll', 'onDemand' ), ) // Args
        );

        add_filter( 'query_vars', array($this, 'add_query_vars_filter') );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
        $countNum = get_query_var('count',0);

         $next = add_query_arg( array('count' => ($countNum+20) ));
        if (0 == $countNum) {
            $prev = false;
        } else {
            $prev = add_query_arg( array('count' => ($countNum-20) ));
        }

        if($countNum>0){
        	$offset=($countNum-20);
			$limit = "20";
		}else{
			$offset=0;
			$limit = $countNum = 20;
		}
		if($offset>0){$tomorrow = true;}

        $player = isset($instance['podcastPlayer']) ? $instance['podcastPlayer'] : get_permalink();

               /* Get By Latest */
        $this->View->render('Widget/dailyAudioRoll', array(
            'player' => $player,
            'next' => $next,
            'prev' => $prev,
            'audio' => $this->Audio->getByLatest($limit,$offset)
        ));
	}

    public function add_query_vars_filter( $vars ){
      $vars[] = "count";

      return $vars;
    }

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
        // outputs the content of the widget
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( '', 'onDemand' );
        $form_id = ! empty( $instance['form_id'] ) ? $instance['form_id'] : __( '', 'onDemand' );
        $station_id = ! empty( $instance['station_id'] ) ? $instance['station_id'] : __( '', 'onDemand' );
        $podcastPlayer = ! empty( $instance['podcastPlayer'] ) ? $instance['podcastPlayer'] : __( '', 'onDemand' );

        $this->View->render('Form/dailyAudioRoll', array(
            'title' => $title,
            'podcastPlayer' => $podcastPlayer,
            'widget' => $this,
            'form_id' => $form_id,
            'station_id' => $station_id,
        ));
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ! empty( $new_instance['title'] ) ? $new_instance['title'] : __( '', 'onDemand' );
        $instance['form_id'] = ! empty( $new_instance['form_id'] ) ? $new_instance['form_id'] : __( '', 'onDemand' );
        $instance['station_id'] = ! empty( $new_instance['station_id'] ) ? $new_instance['station_id'] : __( '', 'onDemand' );
        $instance['podcastPlayer'] = ! empty( $new_instance['podcastPlayer'] ) ? $new_instance['podcastPlayer'] : __( '', 'onDemand' );

        return $instance;
	}
}

add_action( 'widgets_init', function(){
     register_widget( 'onDemand\Daily_Audio_Roll' );
});
