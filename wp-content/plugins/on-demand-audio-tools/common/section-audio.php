<?php namespace onDemand;
use onDemand\View as View;
use onDemand\Audio_library as Audio;

Class Section_Audio extends \WP_Widget
{

    protected $View;
    protected $Audio;
	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
        if (defined('ONDEMAND_TEMPLATES')) {
            $this->View = new View(ONDEMAND_NOONCE);
            $this->View->setTemplatePath(ONDEMAND_TEMPLATES);
        } else {
            wp_die('Unabled to setup the view.');
        }

        try {
            $this->Audio = new Audio(
               AUDIO_LIB_DB_USER,
               AUDIO_LIB_DB_PASSWORD,
               AUDIO_LIB_DB_HOST,
               AUDIO_LIB_DB_NAME,
               AUDIO_LIB_STATION_ID
            );
        } catch (Exception $e) {
            var_dump($e);
        }

        parent::__construct(
        	'SectionAudio', // Base ID
			__( 'Section Audio', 'onDemand' ), // Name
			array( 'description' => __( 'A Widget to display the Audio Roll for a specific section', 'onDemand' ), ) // Args
        );

        add_filter( 'query_vars', array($this, 'add_query_vars_filter') );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {

        $player = isset($instance['podcastPlayer']) ? $instance['podcastPlayer'] : get_permalink();
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( '', 'onDemand' );
        $popup = ! empty( $instance['popup'] ) ? $instance['popup'] : false;
        $moreLink = ! empty( $instance['moreLink'] ) ? $instance['moreLink'] : __( '', 'onDemand' );
        $show_id = ! empty( $instance['showId'] ) ? $instance['showId'] : __( '', 'onDemand' );
        $count = ! empty( $instance['count'] ) ? $instance['count'] : __( '', 'onDemand' );
        $type = ! empty( $instance['type'] ) ? $instance['type'] : __( '', 'onDemand' );
        $tags = ! empty( $instance['tags'] ) ? $instance['tags'] : __( '', 'onDemand' );

        $date = ! empty( $instance['date'] ) ? $instance['date'] : __( '', 'onDemand' );
        $titleoff = ! empty( $instance['titleoff'] ) ? $instance['titleoff'] : __( '', 'onDemand' );
        $description = ! empty( $instance['description'] ) ? $instance['description'] : __( '', 'onDemand' );
        $image = ! empty( $instance['image'] ) ? $instance['image'] : __( '', 'onDemand' );

        $transient_key = $type . "_". $tags ."_". $show_id;
        if ( false === ( $list = get_transient( $transient_key ) ) ) {
            $list = $this->Audio->pullAudioList($show_id, $count, $type, $tags);

            set_transient( $transient_key, $list, 2 * MINUTE_IN_SECONDS );
        }


        $this->View->render('Widget/sectionAudio', array(
            'popup' => $popup,
            'player' => $player,
            'title' => $title,
            'moreLink' => $moreLink,
            'date' => $date,
            'titleoff' => $titleoff,
            'description' => $description,
            'display_image' => $image,
            'list' => $list
        ));
	}

    public function add_query_vars_filter( $vars ){
      $vars[] = "dt";
      return $vars;
    }

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
        // outputs the content of the widget
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( '', 'onDemand' );
        $popup = ! empty( $instance['popup'] ) ? $instance['popup'] : false;
        $form_id = ! empty( $instance['form_id'] ) ? $instance['form_id'] : __( '', 'onDemand' );
        $moreLink = ! empty( $instance['moreLink'] ) ? $instance['moreLink'] : __( '', 'onDemand' );
        $showId = ! empty( $instance['showId'] ) ? $instance['showId'] : __( '', 'onDemand' );
        $count = ! empty( $instance['count'] ) ? $instance['count'] : __( '', 'onDemand' );
        $type = ! empty( $instance['type'] ) ? $instance['type'] : __( '', 'onDemand' );
        $tags = ! empty( $instance['tags'] ) ? $instance['tags'] : __( '', 'onDemand' );
        $podcastPlayer = ! empty( $instance['podcastPlayer'] ) ? $instance['podcastPlayer'] : __( '', 'onDemand' );

        $date = ! empty( $instance['date'] ) ? $instance['date'] : __( '', 'onDemand' );
        $titleoff = ! empty( $instance['titleoff'] ) ? $instance['titleoff'] : __( '', 'onDemand' );
        $description = ! empty( $instance['description'] ) ? $instance['description'] : __( '', 'onDemand' );
        $image = ! empty( $instance['image'] ) ? $instance['image'] : __( '', 'onDemand' );

        $this->View->render('Form/sectionAudio', array(
            'title' => $title,
            'podcastPlayer' => $podcastPlayer,
            'moreLink' => $moreLink,
            'popup' => $popup,
            'showId' => $showId,
            'count' => $count,
            'type' => $type,
            'tags' => $tags,
            'widget' => $this,
            'form_id' => $form_id,
            'date' => $date,
            'titleoff' => $titleoff,
            'description' => $description,
            'image' => $image,

        ));
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ! empty( $new_instance['title'] ) ? $new_instance['title'] : __( '', 'onDemand' );
        $instance['popup'] = ! empty( $new_instance['popup'] ) ? $new_instance['popup'] : false;
        $instance['form_id'] = ! empty( $new_instance['form_id'] ) ? $new_instance['form_id'] : __( '', 'onDemand' );
        $instance['moreLink'] = ! empty( $new_instance['moreLink'] ) ? $new_instance['moreLink'] : __( '', 'onDemand' );
        $instance['showId'] = ! empty( $new_instance['showId'] ) ? $new_instance['showId'] : __( '', 'onDemand' );
        $instance['count'] = ! empty( $new_instance['count'] ) ? $new_instance['count'] : __( '', 'onDemand' );
        $instance['tags'] = ! empty( $new_instance['tags'] ) ? $new_instance['tags'] : __( '', 'onDemand' );
        $instance['type'] = ! empty( $new_instance['type'] ) ? $new_instance['type'] : __( '', 'onDemand' );
        $instance['date'] = ! empty( $new_instance['date'] ) ? $new_instance['date'] : __( '', 'onDemand' );
        $instance['titleoff'] = ! empty( $new_instance['titleoff'] ) ? $new_instance['titleoff'] : __( '', 'onDemand' );
        $instance['description'] = ! empty( $new_instance['description'] ) ? $new_instance['description'] : __( '', 'onDemand' );
        $instance['image'] = ! empty( $new_instance['image'] ) ? $new_instance['image'] : __( '', 'onDemand' );
        $instance['podcastPlayer'] = ! empty( $new_instance['podcastPlayer'] ) ? $new_instance['podcastPlayer'] : __( '', 'onDemand' );

        return $instance;
	}
}

add_action( 'widgets_init', function(){
     register_widget( 'onDemand\Section_Audio' );
});
