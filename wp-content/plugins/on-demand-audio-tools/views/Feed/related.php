<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss/">
  <channel>
    <?php foreach ($playlist as $key => $audio) : ?>
      <item>
        <title>
          <![CDATA[ <?php echo $audio->title; ?> ]]>
        </title>
        <description>
          <![CDATA[ <?php echo $audio->description; ?> ]]>
        </description>
        <link>
        <![CDATA[ http://ktar.com/player/?a=<?php echo $audio->audioid; ?> ]]>
        </link>
        <?php
          if (empty($audio->image) and !empty($audio->showimage)) {
            $audio->image = $audio->showimage;
          } else {
              $audio->image = '';
          }
        ?>

        <?php
          if (!empty($audio->image)) {
            $image = $audio->image;
          } else if (!empty($audio->showimage)) {
            $image = $audio->showimage;
          }
        ?>
        <media:thumbnail url="<?php echo $image; ?>"/>
        <?php $filename = str_replace("icestream.bonnint.net", "audio.ktar.com", $audio->filename); ?>
        <media:content url="<?php echo $filename; ?>" type="audio/mpeg"/>
      </item>
    <?php endforeach; ?>
  </channel>
</rss>
