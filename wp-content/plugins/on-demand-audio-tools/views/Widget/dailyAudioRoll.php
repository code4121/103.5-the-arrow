<style>
.titlelink{
font-size:9pt;
font-family:arial;
text-decoration:none;
color:#000;
}
.titlelink:hover{
color:#0d538e;
}
.desclink{
font-size:8pt;
font-family:arial;
text-decoration:none;
color:#000;
}
.desclink:hover{
color:#0d538e;
}

.desctitle{
font-size:8pt;
font-family:arial;
text-decoration:none;
color:#666;
}

ul.navver, .navver ul {
	margin: 2px;
	margin-right:15px;
	padding: 0px;
	cursor: default;
    list-style-type: none;
	}

.navver li {
	padding: 5px 2px 5px 2px;
	font-family:arial;
	font-size:9pt;
	border-bottom:1px solid #ccc;
	background:#fff;
	text-decoration:none;
}
.navver a {
	text-decoration:none;
}
</style>


<?php foreach ($audio as $a) : ?>
<div style="font-family:arial;font-size:12px; border-top:1px solid #130D0E;padding:10px 0;" class="dailyRollHour">
<b><a class="medlink" style="font-size:14px;" href="<?php echo add_query_arg( 'a', $a->audioid, $player ); ?>"><img src="<? if(!$a->image){ echo $a->showimage;}else{echo $a->image;}?>?filter=mynw/120wide" style="float:left;padding:10px 10px 10px 0; width:120px;" /> <i class="fa fa-play-circle"></i> <b><?php echo $a->showname ." - ". $a->title; ?></b></a></b><br />
        <?php echo $a->fulldescription; ?>
        <br/>
        <div style="clear:both;"></div>
</div>
<?php endforeach; ?>


<div style="margin: 20px 0;float:left;">
    <a href="<?php echo $next; ?>" style="background: #EEE;padding: 5px 8px;border: 1px solid #147;font-weight: bold;}">&#171; Next 20 Podcasts</a>
</div>
<?php if ($prev) : ?>
<div style="margin: 20px 0;float:right;text-align: right;">
    <a href="<?php echo $prev; ?>" style="background: #EEE;padding: 5px 8px;border: 1px solid #147;font-weight: bold;}">Prevous 20 Podcasts &#187;</a>
</div>
<?php endif; ?>
