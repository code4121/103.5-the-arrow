<div class="bottom_cluster">
    <?php if (!empty($moreLink)) : ?>
      <h3><a href="<?php echo $moreLink; ?>"><?php echo $title; ?></a></h3>
    <?php else : ?>
      <h3><?php echo $title; ?></h3>
    <?php endif; ?>
      <div class="heads_right_with_art">
        <ul>
        <?php foreach ($list as $key => $l) : ?>
          <?php
              $play_audio = $player . "/?a=" . $l->audioid;
              if ($popup) {
                  $play_audio = "javascript:void(window.open('{$play_audio}','KTAR','width=700,height=600,status=no,resizable=no,scrollbars=no'));";
                }
          ?>
            <li <?php if (0 == $key) : ?>class="first"<?php endif; ?>>
            <?php if (! 'true' == $display_image ) : ?>
            <?php $image = empty($l->image) ? $l->showimage : $l->image; ?>
            <a href="<?php echo $play_audio ?>" class="thumb"><img src="<?php echo $image; ?>"></a>
            <?php endif; ?>

            <a href="<?php echo $play_audio ?>">
                    <?php if (! 'true' == $titleoff ) : ?>
                    <b style="color:#000"><?php echo $l->title; ?></b>
                    <?php endif; ?>

                    <?php if (! 'true' == $date ) : ?>
                    <?php echo date("l F j", strtotime($l->datepublished)); ?>
                    <?php endif; ?>

                    <?php if (! 'true' == $description ) : ?>
                    <div class="highlight">
                     <?php echo substr( strip_tags($l->description), 0, 100); ?>
                    </div>
                    <?php endif; ?>
                </a>
            </li>
        <?php endforeach; ?>
        </ul>
        <?php if (!empty($moreLink)) : ?>
        <div style="margin:10px 0px 10px 0px;text-align:right;font-weight:bold;font-size:14px;">
            <a href="<?php echo $moreLink; ?>">More podcasts &#187;</a>
        </div>
        <?php endif; ?>
 </div>
 </div>
