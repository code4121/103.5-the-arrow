<?php
  $url = "http" . ((!empty($_SERVER['HTTPS'])) ? "s" : "") . "://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $url1 = urlencode($url);
?>

<?php $jplayer_var = "jp_{$audio_id}"; ?>

<?php if ($inStory) : ?>
  <script>
    jQuery('document').ready(function($) {
      $('.topAudio').on('click', '.jp-play.btn', function(e) {
        e.preventDefault();
        $(this).hide();
        $(this).parent().find('.jp-pause').show();
        player = $(this).data('player');
        console.log(player);
        eval(player).play();
      });

      $('.topAudio').on('click', '.jp-pause.btn', function(e) {
        e.preventDefault();
        $(this).hide();
        $(this).parent().find('.jp-play').show();
        player = $(this).data('player');
        eval(player).stop();
      });

    });
  </script>
<div id="player_column" style="margin:0 0 0 0; <?php echo $player_style; ?>">
        <div id="audioplayer" class="topAudio">
        <div class="jp-gui">
                <div class="jp-interface" <?php if ($player_inline) : ?>style="line-height: 2.1;"<?php endif; ?>>
                  <a href="#" onclick="" class="jp-play btn btn-default" data-player="<?php echo $jplayer_var; ?>" tabindex="1" style="display: block;"><i class="fa fa-play"></i></a>
                  <a href="#" onclick="" class="jp-pause btn btn-default" data-player="<?php echo $jplayer_var; ?>" tabindex="1" style="display: none;"><i class="fa fa-pause"></i></a>
                </div>
              <span id="listen_text" class="listenTitle "><?php echo $audio->title; ?></span>
              <div class="audioDesc"><?php echo $audio->fulldescription; ?></div>

              </div>

</div>
<?php else : ?>
<h1><?php echo $audio->showname; ?></h1>
<h2><?php echo $audio->title; ?>
<?php if ($audio->hideDate) : ?>
  <?php if (!empty($audio->showname)) : ?>
    with <?php echo $audio->showname; ?>
  <?php endif; ?>
  <?php if (!empty($audio->datepublished)) : ?>
    (<?php echo date("n/j",strtotime($audio->datepublished)); ?>)
  <?php endif; ?>
<?php endif; ?>
</h2>
<div style="margin:0 0 10px;"><?php echo $audio->fulldescription; ?></div>
<?php endif; ?>

<div id="adrectangle"></div>
  <div id="player-<?php echo $audio_id; ?>"></div><script type="text/javascript">
var <?php echo $jplayer_var; ?> = jwplayer("player-<?php echo $audio_id; ?>").setup({
file: "<?php echo $audio->filename; ?>",
<?php if ($image) : ?>
image: "<?php echo $image; ?>",
<?php elseif (!empty($audio->image)) : ?>
image: "<?php echo $audio->image; ?>",
<?php else : ?>
image: "<?php echo $audio->showimage; ?>",
<?php endif; ?>
primary:'HTML5',
debug: false,
skin: "five",
sharing: {
  heading:"Share Audio"
},
   related: {
       heading:"Related Audio",
       file: "<?php echo $feed; ?>",
       onclick: "link"
   },
<?php if ($inStory) : ?>
autostart:false,
<?php else : ?>
autostart:true,
<?php endif; ?>
logo: {
    file: '<?php echo $logo ?>',
    link: 'http://ktar.com'
},

width: "100%",
aspectratio: "16:9",
title: "Listen: <?php echo esc_attr($audio->title); ?>",
//sitecatalyst:{},
ga: {}
});
</script>
<?php if ($inStory) : ?>
</div>
<?php endif; ?>