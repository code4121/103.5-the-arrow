<style>
.titlelink{
font-size:9pt;
font-family:arial;
text-decoration:none;
color:#000;
}
.titlelink:hover{
color:#0d538e;
}
.desclink{
font-size:8pt;
font-family:arial;
text-decoration:none;
color:#000;
}
.desclink:hover{
color:#0d538e;
}

.desctitle{
font-size:8pt;
font-family:arial;
text-decoration:none;
color:#666;
}

ul.navver, .navver ul {
	margin: 2px;
	margin-right:15px;
	padding: 0px;
	cursor: default;
    list-style-type: none;
	}

.navver li {
	padding: 5px 2px 5px 2px;
	font-family:arial;
	font-size:9pt;
	border-bottom:1px solid #ccc;
	background:#fff;
	text-decoration:none;
}
.navver a {
	text-decoration:none;
}
.mainListShow{
	float:left;
	width:30%;
	min-width:192px;
	margin:0 3% 0 0;
	max-width: 196px;
	max-width: none !important;
}
</style>
<div style="width:98%;padding:10px 1%;">
    <span style="font-family:arial;font-size:19pt;letter-spacing:-1px;color:#666;"><b>Podcasts and Past Shows</b><br></span>
    <span style="font-family:arial;font-size:11pt;color:#666;">
    Now you can enjoy your favorite shows and stations on your schedule.  Choose a podcast below to find individual past shows and instructions on how to subscribe for continual updates.
    </span><br>
</div>

<div style="padding:10px;">
    <div id="podcasts" style="width:100%;">

        <?php foreach ($shows as $s) : ?>
        <div class="mainListShow" style="text-align:center;padding: 0 2%;">
        <a href="<?php echo home_url(add_query_arg('show', $s->pkShowID, $showsPage)); ?>"><img src="<?php echo $s->img; ?>"  width="190" height="100" style="padding:1px;border:1px solid #999;margin-bottom:2px;"></a>
            <br>
            <a href="<?php echo $s->podURL; ?>" class="titlelink"><b><?php echo $s->vcDisplayName; ?></b></a>
            <br>
            <a href="<?php echo $s->podURL; ?>" class="desclink"><?php echo substr($s->vcShowDesc, 0, 55); ?></a>
            <br>
        </div>
        <?php endforeach; ?>
		<div class="clear"></div>
    </div>
</div>
