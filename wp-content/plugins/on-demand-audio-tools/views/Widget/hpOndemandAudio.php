<div class="voice">
<h3><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h3>

    <?php foreach($shows as $key => $show) : ?>
    <div class="voices <?php if (0 === $key) : ?>first<?php endif; ?>">
        <div class="imagetop">
            <a href="<?php echo $player; ?>?a=<?php echo $show->audioid; ?>">
                <?php $currentImage = isset($image[$key]) ? $image[$key] : ''; ?>
                <?php if (! empty ($currentImage)) : ?>
                <?php echo get_image_tag( $currentImage, $showName[$key], '', '', '60host' ); ?>
                <?php endif; ?>
            </a>
        </div>

        <div class="text">
        <a href="<?php echo $player; ?>?a=<?php echo $show->audioid; ?>"><span class="blogtitle"><?php echo esc_attr($showName[$key]); ?></span><br>
            <?php echo esc_attr(wp_trim_words($show->description, 90, '&hellip;')); ?>
        </a>
        </div>
    </div>
    <?php endforeach; ?>
</div>
