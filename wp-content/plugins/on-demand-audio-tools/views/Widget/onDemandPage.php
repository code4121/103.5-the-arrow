<style>

#article #podr h3{
	font-size: 20px;
border: 0;
margin: 0;
padding: 0;}

.titlelink{
font-size:9pt;
font-family:arial;
text-decoration:none;
color:#000;
}
.titlelink:hover{
color:#0d538e;
}
.desclink{
font-size:8pt;
font-family:arial;
text-decoration:none;
color:#000;
}
.desclink:hover{
color:#0d538e;
}

.desctitle{
font-size:8pt;
font-family:arial;
text-decoration:none;
color:#666;
}

ul.navver, .navver ul {
	margin: 2px;
	margin-right:15px;
	padding: 0px;
	cursor: default;
    list-style-type: none;
	}

.navver li {
	padding: 5px 2px 5px 2px;
	font-family:arial;
	font-size:9pt;
	border-bottom:1px solid #ccc;
	background:#fff;
	text-decoration:none;
}
.navver a {
	text-decoration:none;
}
</style>

<div id="podr">
<h2><?php echo $title; ?> Podcasts and Past Shows</h2>
<div>
	<img src="<?php echo $image ; ?>?filter=mynw/133x86_cropped" style="padding:5px;border:1px solid #999;margin-bottom:2px;width:100%;max-width:620px;min-width:320px;">
</div>
  
            <br>

            <div style="font-family:arial;font-size:11pt;color:#666;">
                <?php echo esc_attr($description); ?>
            </div>
            <br>
    <?php $newdate = ''; ?>
    <?php foreach ($shows as $s) : ?>
    <?php $showdate = date("F j, Y", strtotime($s->datepublished)); ?>
    <?php if ($newdate != $showdate) : ?>
        <div style="padding:5px;margin-top:5px;background:#eee;"><h3 class="zeromargin"><?php echo $showdate; ?></h3></div>
    <?php endif; ?>
    <div style="margin-top:5px;padding:0 5px;">
        <h4 class="zeromargin">
            <a class="blacklink bold" href="<?php echo $player; ?>?a=<?php echo $s->audioid; ?>"><?php echo esc_attr($s->title); ?></a>
        </h4>
        <?php echo esc_attr($s->description); ?>
        <div style="font-size:8pt;">
            <a href=""<?php echo $player; ?>?a=<?php echo $s->audioid; ?>" class="redlink">Listen</a> | <a href="<?php echo str_replace($oldurl, $analyticsUrl, $s->filename); ?>" class="redlink">Download</a>
        </div>
    </div>
    <?php $newdate = $showdate; ?>
    <?php endforeach; ?>
    <div style="margin:40px 0">
        <a href="<?php echo $next; ?>" class="moreStories" style="float:right;">Older shows &raquo;</a>

        <?php if (0 < $pageNumber) : ?>
        <a href="<?php echo $prev; ?>" class="moreStories" style="float:left;">&laquo; Newer shows</a>
        <?php endif; ?>
    </div>

</div>
