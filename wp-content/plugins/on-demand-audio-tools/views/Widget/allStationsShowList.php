<div id="podcasts" style="width:100%;">

     <?php
    foreach ($stations as $station) {
     ?>
    <div class="stationname"><?php echo $station['stationname']; ?></div>
         <?php
        foreach ($station['shows'] as $show) {
            $showid=$show['showid'];
            $showname = $show['showname'];
            if(strlen(stripslashes($showname)) > 30){
                $showname = substr(stripslashes($showname),0,27) . '...';
            }else{
                $showname = stripslashes($showname);
            }
            //$relUrl = "/category/podcasts/{$show['friendlyname']}";
            $relUrl = "http://" . $_SERVER['HTTP_HOST'] . "/category/podcast_results/?sid={$showid}&n=" . urlencode($showname);


        ?>
            <li>
                <div id="<?php echo $showid; ?>" class="subscribe_box">Don't miss an episode of <b><?php echo $showname; ?></b>
                    <p />
                    <a href="<?php echo $show['itunesurl']; ?>" target="new"><i class="fa fa-apple" aria-hidden="true"></i> iTunes</a><br />
                    <a href="<?php echo $show['rssimport']; ?>" target="new"><i class="fa fa-rss" aria-hidden="true"></i> RSS</a>
                    <div class="closebox">
                        <a href="#" id="close" value="<?php echo $showid; ?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                    </div>
                </div>
                <a href="<?php echo $relUrl; ?>"><img src="<?php echo $show['podimage']; ?>"></a><br />
                <a href="<?php echo $relUrl; ?>" class="show"><?php echo $showname; ?></a>
                <ul class="allshowsnav">
                    <li><a href="<?php echo $relUrl; ?>">Episodes</a></li>
                    <li><a href="#" id="subscribe" value="<?php echo $showid; ?>">Subscribe</a></li>
                </ul>
            </li>
        <?php
        }
        ?>
    <?php
    }
    ?>
    <div class="clear"></div>
</div>
