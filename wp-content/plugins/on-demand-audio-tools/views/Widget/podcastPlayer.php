<?php
  $url = "http" . ((!empty($_SERVER['HTTPS'])) ? "s" : "") . "://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  $url1 = urlencode($url);
?>


<div id="play_wrapper">
<div id="play">
<div id="PodcastPlayer"></div>

<script type="text/javascript">
jwplayer("PodcastPlayer").setup({
file: "<?php echo $audio['filename']; ?>",
<?php if ($image) : ?>
image: "<?php echo $image; ?>",
<?php elseif (!empty($audio->image)) : ?>
image: "<?php echo $audio['image']; ?>",
<?php else : ?>
image: "<?php echo $audio['showimage']; ?>",
<?php endif; ?>
primary:'HTML5',
debug: false,
skin: "bekle",
sharing: {
  heading:"Share Audio"
},
  //  related: {
  //      heading:"Related Audio",
  //      file: "<?php echo $feed; ?>",
  //      onclick: "link"
  //  },
<?php if ($inStory) : ?>
autostart:false,
<?php else : ?>
autostart:true,
<?php endif; ?>

width: "100%",
aspectratio: "16:9",
stretching: "fill",
title: "Listen: <?php echo esc_attr($audio['title']); ?>",
ga: {}
});
</script>
</div>
</div>
