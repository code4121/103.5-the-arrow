<p>
<label for="<?php echo $widget->get_field_name( 'title' ); ?>">Title:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'title' ); ?>" name="<?php echo $widget->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'popup' ); ?>">Open the player in a pop up window:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'popup' ); ?>" name="<?php echo $widget->get_field_name( 'popup' ); ?>" type="checkbox" value="true" <?php if ($popup) : ?>checked<?php endif; ?>>
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'podcastPlayer' ); ?>">Player Page:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'podcastPlayer' ); ?>" name="<?php echo $widget->get_field_name( 'podcastPlayer' ); ?>" type="text" value="<?php echo $podcastPlayer; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'moreLink' ); ?>">More Link:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'moreLink' ); ?>" name="<?php echo $widget->get_field_name( 'moreLink' ); ?>" type="text" value="<?php echo $moreLink; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'showId' ); ?>">Show ID:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'showId' ); ?>" name="<?php echo $widget->get_field_name( 'showId' ); ?>" type="text" value="<?php echo $showId; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'count' ); ?>">List Count:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'count' ); ?>" name="<?php echo $widget->get_field_name( 'count' ); ?>" type="text" value="<?php echo $count; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'tags' ); ?>">tags:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'tags' ); ?>" name="<?php echo $widget->get_field_name( 'tags' ); ?>" type="text" value="<?php echo $tags; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'type' ); ?>">Type:</label>

<select class="widefat" id="<?php echo $widget->get_field_id( 'type' ); ?>" name="<?php echo $widget->get_field_name( 'type' ); ?>" type="text">
    <option value='fullshow' <?php echo ($type=='fullshow')?'selected':''; ?>>Full Show</option>
    <option value='showsegments' <?php echo ($type=='showsegments')?'selected':''; ?>>Show Segments</option>
    <option value='showwithtags' <?php echo ($type=='showwithtags')?'selected':''; ?>>Show with Tags</option>
    <option value='tags' <?php echo ($type=='tags')?'selected':''; ?>>Tags</option>
</select>
</p>

<div style="" id="">
<br>
<strong><i>Turn Off</i></strong>
<br>
<p>
<input id="<?php echo $widget->get_field_id( 'date' ); ?>" name="<?php echo $widget->get_field_name( 'date' ); ?>" type="checkbox" value="true" <?php echo 'true' == $date ? 'checked' : ''; ?>>&nbsp;Date<label for="<?php echo $widget->get_field_name( 'date' ); ?>"></label>&nbsp;&nbsp;
<input id="<?php echo $widget->get_field_id( 'titleoff' ); ?>" name="<?php echo $widget->get_field_name( 'titleoff' ); ?>" type="checkbox" value="true" <?php echo 'true' == $titleoff ? 'checked' : ''; ?>>&nbsp;Title<label for="<?php echo $widget->get_field_name( 'titleoff' ); ?>"></label>&nbsp;&nbsp;
<input id="<?php echo $widget->get_field_id( 'description' ); ?>" name="<?php echo $widget->get_field_name( 'description' ); ?>" type="checkbox" value="true" <?php echo 'true' == $description ? 'checked' : ''; ?>>&nbsp;Description<label for="<?php echo $widget->get_field_name( 'description' ); ?>"></label>&nbsp;&nbsp;
<input id="<?php echo $widget->get_field_id( 'image' ); ?>" name="<?php echo $widget->get_field_name( 'image' ); ?>" type="checkbox" value="true" <?php echo 'true' == $image ? 'checked' : ''; ?>>&nbsp;Image<label for="<?php echo $widget->get_field_name( 'image' ); ?>"></label>&nbsp;&nbsp;
</div>
