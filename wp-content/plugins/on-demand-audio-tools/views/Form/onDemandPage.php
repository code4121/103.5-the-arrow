<p>
<label for="<?php echo $widget->get_field_name( 'title' ); ?>">Title:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'title' ); ?>" name="<?php echo $widget->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'player' ); ?>">Player Url:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'player' ); ?>" name="<?php echo $widget->get_field_name( 'player' ); ?>" type="text" value="<?php echo $player; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'analyticsUrl' ); ?>">Analytics Url:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'analyticsUrl' ); ?>" name="<?php echo $widget->get_field_name( 'analyticsUrl' ); ?>" type="text" value="<?php echo $analyticsUrl; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'show_id' ); ?>">Show ID:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'show_id' ); ?>" name="<?php echo $widget->get_field_name( 'show_id' ); ?>" type="text" value="<?php echo $show_id; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'stories_per_page' ); ?>">Show How Many Per Page?</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'stories_per_page' ); ?>" name="<?php echo $widget->get_field_name( 'stories_per_page' ); ?>" type="text" value="<?php echo $perPageCount; ?>">
</p>
