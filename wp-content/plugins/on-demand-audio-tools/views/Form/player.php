<p>
<label for="<?php echo $widget->get_field_name( 'title' ); ?>">Title:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'title' ); ?>" name="<?php echo $widget->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'analyticsUrl' ); ?>">Analytics URL:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'analyticsUrl' ); ?>" name="<?php echo $widget->get_field_name( 'analyticsUrl' ); ?>" type="text" value="<?php echo $analyticsUrl; ?>">
</p>
