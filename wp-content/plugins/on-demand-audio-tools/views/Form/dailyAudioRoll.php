<p>
<label for="<?php echo $widget->get_field_name( 'title' ); ?>">Title:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'title' ); ?>" name="<?php echo $widget->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'station_id' ); ?>">Station ID:</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'station_id' ); ?>" name="<?php echo $widget->get_field_name( 'station_id' ); ?>" type="text" value="<?php echo $station_id; ?>">
</p>

<p>
<label for="<?php echo $widget->get_field_name( 'podcastPlayer' ); ?>">Podcast Player URL</label>
<input class="widefat" id="<?php echo $widget->get_field_id( 'podcastPlayer' ); ?>" name="<?php echo $widget->get_field_name( 'podcastPlayer' ); ?>" type="text" value="<?php echo $podcastPlayer; ?>">
</p>
