<?php

/**
 * @package Homepage Rotator
 * @version 1.6
 */
/*
Plugin Name: Bonneville Homepage Rotator	
Plugin URI: 
Description: This plugin uses the popular 'refineslider' to create a homepage rotator populated by a metabox on posts and timed with 'post expirator'.
Author: Doug Bailey
Version: 1.0.1
Author URI: http://bonneville.com
*/

function bonneville_rotator_fetch_post_data() {
    
global $wpdb;
 
$sql = "SELECT `post_id`, `meta_value`, `ID` FROM $wpdb->postmeta, $wpdb->posts WHERE `ID` = `post_id` AND meta_key = '_arrow_rotator' AND post_status = 'publish' ORDER BY post_id DESC"; 

$results = $wpdb->get_results($sql) or die(mysql_error());

    foreach( $results as $result ) {
	
	//echo "ID IN: " . $result->post_id . "<br>";
	
	if(bonneville_rotator_expired($result->post_id) != true){
	
	    $image_detail = bonneville_rotator_get_image_details( $result->meta_value );
	
	    $ids[] = array('post_id'=>$result->post_id, 'rotator_id'=>$result->meta_value, 'post_time'=>$result->post_time, 'image_details'=>$image_detail);
    
	}
    
    }
    
    return $ids;

}

function bonneville_rotator_get_image_details($image_id){
    
    global $wpdb;
    
    $sql = "SELECT `meta_value`, `post_excerpt` FROM $wpdb->postmeta, $wpdb->posts WHERE `post_id` = $image_id AND `meta_key` = '_wp_attached_file' AND `ID` = `post_id`";

    $results = $wpdb->get_results($sql);

    foreach($results as $result){
	
	$detail['url'] = $result->meta_value;
	
	if(isset($result->post_excerpt)){
	
	    $detail['caption'] = $result->post_excerpt;
	
	}
	
    }
    
    return $detail;
    
}

function bonneville_rotator_expired($id){
    
    global $wpdb;

    //echo "ID IN QUERY: $id<br>";
 
    $sql = "SELECT `meta_value` FROM $wpdb->postmeta WHERE `post_id` = ".$id." AND meta_key = '_arrow_rotatorexpire' LIMIT 1"; 

    $result = $wpdb->get_row($sql);
	
    //time + 7 hours...
	
	//echo(date("Y-m-d h:i:s",$current_timestamp));

	//return true;

	//does it have an expiration date? and if so has it expired? has it a publish time?	
    if(!empty($result->meta_value)){
		
	$current_timestamp = current_time('timestamp');
	
	$expire_time = strtotime($result->meta_value);
	
	if($expire_time < $current_timestamp){
	
	    //possibly remove the rotator at this point from the post_meta table
	    return true;
		
	}
	
	return false;
    
    }else{
	
        return false;
	
    }
 
}

/*function bonneville_rotator_is_expired($id,$timestamp){
   
    global $wpdb;
    
    if(isset($_REQUEST['debug_rotator'])){
		    
	echo "current expire time [".date('Y-m-d g:i:s a',$timestamp)."]<br>";
    
    }
    
    $sql = "SELECT `meta_value` FROM `wp_postmeta` WHERE post_id = " . $id . " AND meta_key = '_expiration-date' AND meta_value < NOW() ORDER BY post_id ASC"; 
    
    $results = $wpdb->get_results($sql);
    
    if(!$results){
	
	if(isset($_REQUEST['debug_rotator'])){
		    
	    echo "NO EXPIRATION: ".$id.", ";
	
	}
	 
        return false;
	
    }else{
    
	foreach( $results as $result ) {
    

	    
	    if(($result->meta_value-25200) < $timestamp){
	
		if(isset($_REQUEST['debug_rotator'])){
		    
		    echo "EXPIRED: ".$id.", ";
		    
		}
		    
	        return true;
	    
	    }else{
		if(isset($_REQUEST['debug_rotator'])){
		    
		    echo "EXPIRATION SET [".date('Y-m-d g:i:s a',$result->meta_value-25200)."] BUT NOT EXPIRED: ".$id.", ";
		
		}
		
		return false;
	    
	    }
	}
    }   
    
}

function bonneville_rotator_is_published($id,$timestamp){
 
    global $wpdb;
    
    if(isset($_REQUEST['debug_rotator'])){
	
	echo "current publish time [".date('Y-m-d g:i:s a',$timestamp)."]<br>";
   
    }
       
    $sql = "SELECT UNIX_TIMESTAMP(post_date) AS p_date FROM `wp_posts` WHERE ID = " . $id . " AND post_date < NOW()"; 
    
    $results = $wpdb->get_results($sql);
    
    if(!isset($results)){
	
	return false;
	
    }else{
    
	foreach( $results as $result ) {
	
	    if(($result->p_date - 25200) < $timestamp){
		
		if(isset($_REQUEST['debug_rotator'])){
		
		    echo "PUBLISHED: ".$id."<br><br>";
		
		}
		
		return true;
	    
	    }else{
	    
		if(isset($_REQUEST['debug_rotator'])){
		
		    echo "SCHEDULED [".date('Y-m-d g:i:s',$timestamp)."]: ".$id."<br><br>";
		
		}
		
		return false;
	    
	    }
    
	}
    
    }
   
}*/

function bonneville_rotator_output($data){

    $upload_dir = wp_upload_dir();

    $outbody = '<script>function preload(arrayOfImages) {
    jQuery(arrayOfImages).each(function(){
        jQuery(\'<img/>\')[0].src = this;
    });
}
preload([';
    foreach($data as $img){
	$image_list .= '"'. $upload_dir['baseurl'] . "/" . $img['image_details']['url'] . '",';
    }
    $outbody .= rtrim($image_list, ",");
$outbody .= ']);
</script>

	   <script>// Can also be used with $(document).ready()
jQuery(window).load(function() {
  jQuery(\'.flexslider\').flexslider({
    animation: "fade"
  });
});</script>
<div class="flexslider" style="background:rgba(0,0,0,0.4); border:2px solid rgba(255,255,255,0.05); max-height:360px;max-width:1140px;">
  <ul class="slides">';
    
    foreach($data as $d){
	
	$outbody .= '<li><a href="' . get_permalink($d['post_id']) . '"><img src="' . $upload_dir['baseurl'] . "/" . $d['image_details']['url'] . '" /></a><div>'.$d['image_details']['post_excerpt'].'</div></li>';
    
    }
    $outbody .= '</ul></div>';
	    
    return $outbody;

}

    function bonneville_rotator_shortcode ( ){
    
	$ids = bonneville_rotator_fetch_post_data( );
  
	$output = bonneville_rotator_output( $ids );

	return $output;

    }

add_shortcode( 'bonneville_rotator', 'bonneville_rotator_shortcode');

?>