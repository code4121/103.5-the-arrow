<?php
/**
 * @package Now_Playing
 * @version 1.6
 */
/*
Plugin Name: Now Playing
Plugin URI: 
Description: This is a plugin that pulls album art and other assets based on a the title of the song being played...right now!
Author: Doug Bailey
Version: 1.0.1
Author URI: http://bonneville.com
*/



//require_once('config.php');

function album_art_exists($file){

	$file_headers = @get_headers($file);
	
	if($file_headers[0] == 'HTTP/1.1 404 Not Found') {

	    $exists = false;

	} else {

	    $exists = true;

	}	
	
	return $exists;
	
}

function get_now_playing(){
    
   /*global $wpdb;
   
   $now_playing = $wpdb->get_row("SELECT * FROM `wp_playlist` WHERE `title` != 'ARROW TRAFFIC' AND `artist` != 'BC' AND `artist` != 'VC' AND `artist` != 'GT' ORDER BY playlist_id DESC LIMIT 1", ARRAY_A);
   
   $album_query = "SELECT medium_image, download_link FROM `wp_amazon_lookup` WHERE `title` = '" . mysql_escape_string($now_playing['title']) . "' AND `artist` = '" . mysql_escape_string($now_playing['artist']) . "' LIMIT 1";
   
   $album_info = $wpdb->get_row($album_query, ARRAY_A);*/
   
   $str = file_get_contents('http://wires.bonnint.com/slc/playlist.php?station=krsp&now_playing=1');
   
   /*if(album_art_exists($now_playing['album_art']['medium_image']) == false){
	$now_playing['medium_image'] = plugins_url( 'default_album_art/arrow-default-album-art-medium.jpg', __FILE__ );
	$now_playing['download_link'] = '/';
   }else{
	
	$now_playing['download_link'] = $album_info['download_link'];
	$now_playing['medium_image'] = $album_info['medium_image'];
	
   }*/
   
   die($str);
    
}

function now_playing_output(){
	$output = "<style>
		.truncate {
			width: 100%;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}
	</style>";
	
	$output .= "<script type='text/javascript'>";
	
	$output .= "jQuery(document).ready(function() { ";
	
	$ajax_url = admin_url( 'admin-ajax.php' );
	
	$output .= "jQuery.ajax({
				url:'" . $ajax_url . "',
				dataType:'json',
				method: 'POST',
				data: {
					action: 'bonnevilleNowPlaying'
				},
				success: function(data){
					console.log(data);
					process_playlist(data);
				},
				error: function(){
					console.log('no data retrieved from ".$ajax_url."');
				}
		});
		
	});";
	
	$output .= "function process_playlist(data){
	
		jQuery('#now-playing-title').text(data[0].title);
		jQuery('#now-playing-artist').text(data[0].artist);
		jQuery('#now-playing-download-link').attr('href', data[0].album[0].download_link);
		jQuery('#now-playing-album-art').attr('src', data[0].album[0].medium_image);
		jQuery('#now-playing-album-art').attr('alt','103.5 The Arrow - ' + data.title + ' - ' + data.artist);
		
		jQuery('#now_playing_container').fadeToggle('slow','linear');
	
	}";
	
	$output .= "</script>";
	
    $default_image = plugins_url( 'now-playing/default_album_art/arrow-default-album-art-medium.jpg');
    
    
    $output .= "<h4 class='listen-header truncate'><span id='now-playing-title'>Utah's CLassic Rock</span> - <span style='font-size:16px;' id='now-playing-artist'>103.5 The Arrow</span></h4>

<span id='now_playing_container' style='display:none;'><a id='now-playing-download-link' href='/' target='_BLANK'><img id='now-playing-album-art' style='height: 175px;' src='".$default_image."' alt='103.5 The Arrow' /></a></span>";

    return $output;
    
}

add_action( 'wp_ajax_bonnevilleNowPlaying', 'get_now_playing' );
add_action( 'wp_ajax_nopriv_bonnevilleNowPlaying', 'get_now_playing' );

add_shortcode( 'now_playing', 'now_playing_output');
//add_shortcode( 'now_playing_list', 'recently_played_page');


?>
