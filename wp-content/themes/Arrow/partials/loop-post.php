<?php
/**
 * This is the loop for the posts
 * We display each post differently depending on the post format
 */

 if(function_exists('get_imdb_connector_movie') && has_category('movies')){
					
	$imdb = get_imdb_connector_movie(get_the_title());
	
 }
 
 ?>

<article <?php post_class(); ?>   id="post-<?php the_ID(); ?>">	
	<div class="post-entry">
		<div class="right-post-column">
			<?php
			$comments_link = get_comments_link();
			if( is_single() )
				$comments_link = '#comments';
			?>
			<a title="<?php printf(__('Comment on %s', 'bd'), get_the_title()); ?>" class="comment-bubble-link scroll" href="<?php echo $comments_link; ?>"><?php echo get_comments_number(); ?></a>
			<?php
			if ( function_exists( 'wolf_share' ) ) {
		
				if ( is_single () ) : ?>
					<?php wolf_share( 'vertical' ); ?>
				<?php
				else :
					wolf_share( 'horizontal' );
				endif;
			}
			/*  Edit link
			/*---------------------------*/
			edit_post_link( __( 'Edit', 'wolf' ), '<span class="edit-link">', '</span>' ); 
			?>
		</div>
		<div class="left-post-column">
			<header class="entry-header" style="width:100%;">
				<div class="post-loop-thumbnail" id="homepage-title" <?php if (has_post_thumbnail() || isset($imdb['poster'])){ ?>style="float:right;width:75%;"<?php }else{ ?>style="float:right;width:100%;"<?php } ?>><?php wolf_post_title(); ?>
					<div class="entry-meta">
						<?php wolf_entry_date(); ?>
						<?php if ( comments_open() ) : ?>
						<span class="comments-link mobile-comment-count">
							<?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a comment', 'wolf' ) . '</span>', __( 'One comment so far', 'wolf' ), __( 'View all % comments', 'wolf' ) ); ?>
						</span><!-- .comments-link -->
						<?php endif; // comments_open() ?>
						<?php if(function_exists('get_imdb_connector_movie') && has_category('movies')){
					
							echo "<h3 style='margin:0px 0px 0px 0px;'>". $imdb['imdbrating']."/10<span style='font-size:18px;'>On IMDB</span></h4>";
							echo $imdb['year']. " | " . $imdb['rated'] ." | " . $imdb['runtime']['minutes'] . " Minutes<br>"; ?>
							<span style="text-align:justify;">
							<?php foreach($imdb['genres'] as $genre){
								echo $genre . " ";
							} ?>
							</span>
							<br>
							<span style="text-align:justify;">
							<?php foreach($imdb['actors'] as $actor){
								$actor_line .= $actor . " | ";
							}
							$actor_line = rtrim($actor_line, " | ");
							echo $actor_line;
							?>
							
							</span>
							<?php
							
							echo "<br>" . $imdb['plot'] . "<br>";
							
						} ?>
					</div>
				</div>
		
			<?php if ( ! get_post_format() || 'image' == get_post_format() || 'audio' == get_post_format() ) : ?>
			 
				<?php if (has_post_thumbnail()){ ?>
				<div class="post-loop-thumbnail" id="homepage-thumb" style="float:left;width:24%;"><?php wolf_post_thumbnail(); ?></div>
				<?php }elseif(!has_post_thumbnail() && function_exists('get_imdb_connector_movie') && isset($imdb['poster'])){ ?>
				<div class="post-loop-thumbnail" id="homepage-thumb" style="float:left;width:24%;">
				<div class="entry-thumbnail"><img width="540" height="800" src="<?php echo $imdb['poster']; ?>" class="attachment-default wp-post-image" alt="<?php $imdb['title']; ?>"></div></div>
				<?php } ?>
			<?php endif ?>
		</header>
			<div class="entry-content">
				<?php if(function_exists('get_imdb_connector_movie') && has_category('movies')){ ?>
				<hr>
				<?php } ?>
				<?php if(!is_single()){
					if(get_the_excerpt() != ""){
						the_excerpt();
					}else{
						the_content( wolf_more_text() );
					}
				}else{ ?>
					<?php the_content( wolf_more_text() ); ?>
					<?php wolf_share_mobile(); ?>
				<?php } ?>
			</div>
		
			<footer class="entry-meta">
				<?php //wolf_entry_meta(); ?>
				<br><br>
				<?php if(has_category('contests') && is_single()){ get_template_part( 'partials/content', 'contest' ); } ?>
			</footer>
		</div>
		
		
	</div>

	<?php if ( is_single() ) : ?>
	<hr>
	<?php wolf_post_nav(); ?>
	<?php endif ?>

</article>
<hr>