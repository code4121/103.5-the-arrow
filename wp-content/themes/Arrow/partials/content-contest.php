<?php
/**
 * This is the loop for the posts
 * We display each post differently depending on the post format
 */
 ?>
 
<?php if(is_single()){

       $post_meta = get_post_meta( $post->ID, '_arrow_contest_rules' );
       
       if(isset($post_meta[0])){
	      
	      $meta_values = get_post_meta($post_meta[0], '_wp_attached_file');
       
	      if(isset($meta_values[0]) && $meta_values[0]>0){
	
		     $contest_rules = "<a href='/wp-content/uploads/" . $meta_values[0] . "'>Contest Rules</a><br>";
       
	      }
       
       }//else{
	
	      //$contest_rules = 'Click <a href="/103-5-the-arrow-general-contest-rules/" target="_BLANK">HERE</a> for general contest rules.<br>';
	      
       //}
       
}?>

<div style="margin:0px 0px 20px 0px;color:#fff;">
       <?php if(isset($contest_rules)){ echo $contest_rules; }?><br>
       <hr>
</div>
<?php if(!is_single()){ ?>
<div style="color:#fff;">
       <a href="http://media.bonnint.net/birg/1/122/12266.gif">
	      <img src="http://media.bonnint.net/birg/1/122/12266.gif" style="float:left;width:100px;height:75px;padding:2px;border:1px solid #ddd; margin: 0px 10px 0px 0px;">
       </a>				
       <div style="font-weight:bold;font-size:16px; text-decoration: none;">
	      <a href="http://media.bonnint.net/birg/1/122/12266.gif" style="font-weight:bold;font-size:14px; text-decoration: none;">Prize Pickup</a>
       </div>
       <p>Did you win a prize? <b>Congratulations!</b><br>

When possible please wait 24 hours before picking up your prize to allow time for processing. </p>

<p>You can pick up your prize Monday - Friday, 8 a.m. - 5 p.m. at the Broadcast House, 55 North 300 West (Triad Center). Downtown SLC. Please remember to bring picture ID. </p>

</div>
<?php } ?>