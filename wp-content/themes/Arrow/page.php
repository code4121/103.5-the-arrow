<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 */
get_header(); 
wolf_page_before();
?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>><h1><?php echo get_the_title(); ?></h1>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
							<div class="entry-thumbnail">
								<?php the_post_thumbnail( 'extra-large' ); ?>
							</div>
						<?php endif; ?>
					</header>
					
					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'wolf' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'wolf' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				<?php // comments_template(); ?>
			<?php endwhile; ?>
			<!--Contest Web Section-->
			<?php if($_SERVER['REQUEST_URI'] == "/contests-2/"){ ?>

				<h4 style="text-align:center;" >Social Media</h4>
				<div id="aptivada_app" style="background: #ffffff url('https://cdn2.aptivada.com/images/iframeLoader.gif') no-repeat center; min-height: 500px;" data-app-id="165976433" data-app-type="contest_grid" data-mobile-redirect="0" data-height="1200">
				<iframe src="https://www.aptivada.com/apps/contest/all.php?account=165976433&amp;app_type=contest_grid&amp;new_embed=Y&amp;FAN=0&amp;parent=http%3A%2F%2Fthearrow.dev%2Fcontests-2%2F" id="aptivada_iframe" height="1200" width="100%" frameborder="0" style="width: 100%; height: 1200px; background-color: rgb(255, 255, 255);"></iframe></div>
				<br />
				<br />
				<h2>Prize Pickup</h2>
				<p>Did you win a prize? <em><strong>Congratulations!</strong></em><br \>
				 	When possible please wait 24 hours before picking up your prize to allow time for processing.</span><br>
				 	You can pick up your prize Monday – Friday, 8 a.m. – 5 p.m. at the KSL Broadcast House, 55 North 300 West (Triad Center). Downtown SLC.</span></p>
				<p><strong>Please remember to bring picture ID.</strong></p>

				<h2>NOTICE</h2>
				<p>Disclaimer: Due to delays in the stations online streaming of its broadcast signal, listeners to the online stream may not be able to participate in or be disadvantaged in participating in certain on air contests.</p></td>
			<!--END of Contest Web Section-->

			<?php } ?>

		

			<?php if($_SERVER['REQUEST_URI'] == "/events/category/community/"){ ?>
			
				<h3>Submit Your Event</h3>
				<?php echo do_shortcode('[gravityform id="11" name="Community Calendar Submission" title="false" description="false"]'); ?>
			
			<?php } ?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php

get_sidebar();
wolf_page_after(); 
get_footer(); 
?>