<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 */
?>	
	
		</div><!-- .wrap -->
	</section><!-- section#main -->

	<?php wolf_bottom_holder(); ?>
	
	<?php wolf_footer_before(); ?>
	
	<footer id="colophon" class="site-footer" role="contentinfo">
		<?php wolf_footer_start(); ?>
		
		<?php get_sidebar( 'footer_area' ); ?>

		<?php wolf_footer_end(); ?>

	</footer><!-- #colophon -->

	<div id="bottom-bar">
		<div class="wrap">
			<nav id="site-navigation-tertiary" class="navigation tertiary-navigation" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_id' => 'bottom-menu', 'menu_class' => 'nav-menu-tertiary', 'fallback_cb'  => '' ) ); ?>
			</nav><!-- #site-navigation -->
			<?php wolf_site_info(); ?>
		</div>
	</div>
	
	<?php wolf_footer_after(); ?>
</div><!-- #page .hfeed .site -->


<?php wolf_body_end(); ?>
<?php wp_footer(); ?>

<!--begin omniture tag-->
	<script type="text/javascript" language="javascript" src="/live_s_code.js"></script>
	<script type="text/javascript" language="javascript"><!--
		/* You may give each page an identifying name, server, and channel on
		the next lines. */
		s.pageName="<?php echo omniture_title(); ?>"
		s.server="e"
		s.channel=""
		s.pageType="<?php echo get_post_type( ); ?>"
		s.prop1=""
		s.prop2=""
		s.prop3=""
		s.prop4=""
		s.prop5=""
		s.prop11="<?php if(is_home()){ echo "007"; }else{ echo $post->ID; } ?>"
		s.prop12=""
		s.hier1="<?php echo the_breadcrumb( ); ?>"
		/* E-commerce Variables */
		s.campaign=""
		s.state=""
		s.zip=""
		s.events=""
		s.products=""
		s.purchaseID=""
		s.eVar1=""
		s.eVar2=""
		s.eVar3=""
		s.eVar4=""
		s.eVar5=""
		/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
		var s_code=s.t();if(s_code)document.write(s_code)//-->
	</script>
<!--end omniture tag-->

</body>
</html>