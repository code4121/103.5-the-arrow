<?php
/**
 * The Sidebar containing the main widget areas.
 */
?>
<aside id="secondary" class="widget-area" role="complementary">

	<div class="sidebar-inner">
		<?php /* if(is_dynamic_sidebar( 'right-top' )){
			
			dynamic_sidebar( 'right-top' );
			
		} */ ?>
		<?php 
		if ( wolf_is_woocommerce() ) {

			dynamic_sidebar( 'woocommerce' ); 

		} elseif ( function_exists( 'wolf_sidebar' ) ) {
			
			wolf_sidebar();

		} else {

			dynamic_sidebar( 'main' ); 
		}
		
		/*if(is_dynamic_sidebar( 'right-bottom' )){
		
			dynamic_sidebar( 'right-bottom' ); 
		
		}*/		
		
		?>
	</div>

</aside><!-- #secondary .sidebar-container -->
<?