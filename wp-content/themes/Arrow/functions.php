<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(), array( 'parent-style' ) );
}

function arrow_custom_sidebars() {
    

    register_sidebar( array(
        'name' => __( 'Header Banner Ads 728x90', 'header-banner-ad' ),
        'id' => 'header-banner-ad',
        'description' => __( 'Header Banner Ads 728x90', 'header-banner-ad' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s header-banner-ad">',
		'after_widget'  => '</div>',
        'before_title' => '<h2 class="header-titles" style="display:none;">',
        'after_title' => '</h2>',
    ) );
    register_sidebar( array(
        'name' => __( 'Header Banner Ads 300x250', 'header-banner-ad-reponsive' ),
        'id' => 'header-banner-ad-responsive',
        'description' => __( 'Header Banner Ads 300x250', 'header-banner-ad-responsive' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s header-banner-ad-responsive">',
		'after_widget'  => '</div>',
        'before_title' => '<h2 class="header-titles" style="display:none;">',
        'after_title' => '</h2>',
    ) );
}
add_action( 'widgets_init', 'arrow_custom_sidebars' );

//exclude categories from the homepage...
function wcs_exclude_cat( $query ) {
  if ( $query->is_home() && $query->is_main_query() ) {
    $query->set( 'cat', '-13,-60,-207' );
  }

}

add_action( 'pre_get_posts', 'wcs_exclude_cat', 1 );

function the_breadcrumb() {
	if (!is_home()) {
		//echo get_option('home');
		//echo ":";
		bloginfo('name');
		echo ":";
		
		if (is_category() || is_single()) {
		    $categories = get_the_category();
		    if($categories){
			foreach($categories as $category) {
			    $output .= $category->name.":";
			}
			echo trim($output, ":");
		    }

		    if (is_single()) {
			echo ":";
			the_title();
		    }
		} elseif (is_page()) {
			echo the_title();
		}
	}else{
	    echo "103.5 The Arrow:Home";
	}
}

function omniture_title() {
	/*if (!is_home()) {
	    if (is_category() || is_single()) {
		//echo single_cat_title( );
	        if (is_single()) {
		    the_title();
		}
	    } elseif (is_page()) {
		echo the_title();
	    } elseif (is_category()) {
		echo single_cat_title();
	    }elseif (is_tag()){
		echo single_tag_title();
	    }else{
		echo wp_title();
	    }
	}else{
	    echo "103.5 The Arrow";
	}*/
	
	$full_title = wp_title("",false);
	
	$title = str_replace(" - 103.5 The Arrow","",$full_title);
	
	echo $title;
	
}

/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://metabox.io/docs/registering-meta-boxes/
 */


add_filter( 'rwmb_meta_boxes', 'arrow_register_meta_boxes' );

/**
 * Register meta boxes
 *
 * Remember to change "your_prefix" to actual prefix in your project
 *
 * @param array $meta_boxes List of meta boxes
 *
 * @return array
 */
function arrow_register_meta_boxes( $meta_boxes )
{
	/**
	 * prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = '_arrow_';

	// 1st meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'standard',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Special Contest Rules', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		//'post_types' => array( 'post' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}contest_rules",
				'type' => 'file',
			)
		)
	);
	
	/*$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'homepage_rotator',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Homepage Rotator', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'post','page','tribe_events' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}rotator",
				'type' => 'file',
			)
		)
	);*/

	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'homepage_rotator',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Homepage Rotator', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'post','page','tribe_events' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}rotatorlink",
				'type' => 'text',
				'desc' => 'Rotator Link',
			),
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}rotatorexpire",
				'type' => 'datetime',
				'desc' => 'Rotator Expiration',
			),
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}rotator",
				'type' => 'file',
				'desc' => 'Upload Rotator',
			)
		)
	);
	
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'passport_link',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Passport Link', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'passport' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "fm100_passport_link", //at some point this needs to be revisited to show arrow not fm100. due to time restricitons this was brought over as is as a 1-1 copy.
				'type' => 'text',
				'desc' => 'Link to passport partner'
			)
		)
	);	
	
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'content_background',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Content Area Background', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'passport','post','page' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}content_background",
				'type' => 'file',
				'desc' => 'Upload Content Background (width 695px)',
			)
		)
	);
	
	/*$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'homepage_rotator_expire',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Homepage Rotator Expire', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'post','page','tribe_events' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}rotatorexpire",
				'type' => 'datetime',
			)
		)
	);*/
	//datetime
	return $meta_boxes;
}
/**
 * Register Listen-Live-Menu 
 * 
 */ 
add_action( 'init', 'register_listen_live_menu' );
function register_listen_live_menu() {
	register_nav_menus(
		array(
			'listen-live-menu' => __( 'Listen-Live Location' )
		)
	);
}
/**
 * Register Shortcode for listen-live-location
 */
add_shortcode('add_listen_live_menu', 'listen_live_menu_shortcode');
function listen_live_location_shortcode(){
	$value = "<?php wp_nav_menu( array( 'theme_location' => 'listen-live-menu' ) ); ?>";
	return $value;
}





function add_query_vars_filter( $vars ){
	$vars[] = "no-ga";
	return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

function add_my_rss_node() {
	global $post;
	if(has_post_thumbnail($post->ID)):
		$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID));
		echo('<media:content url="' . $thumbnail[0] . '" medium="image"/>');
		echo('<appimage>' . $thumbnail[0] . '</appimage>');
		echo('<appdescription><![CDATA[' . utf8_for_xml(html_entity_decode(get_the_excerpt($post->post_ID))) . ']]></appdescription>');
	endif;
}
add_action('rss2_item', 'add_my_rss_node');

/*remove invalid-for-xml utf8 strings*/
function utf8_for_xml($string)
{
	return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
}

/*<script type='text/javascript' src='http://1035thearrow.com/wp-content/themes/live/includes/features/wolf-refineslide/js/jquery.refineslide.min.js?ver=0.3'></script>
<link rel='stylesheet' id='refineslide-css'  href='http://1035thearrow.com/wp-content/themes/live/includes/features/wolf-refineslide/css/refineslide.css?ver=0.1' type='text/css' media='all' />
<link rel='stylesheet' id='refineslide-theme-dark-css'  href='http://1035thearrow.com/wp-content/themes/live/includes/features/wolf-refineslide/css/refineslide-theme-dark.css?ver=0.1' type='text/css' media='all' />*/


//function plugin_scripts() {
	//wp_enqueue_script( 'on_air', plugins_url( ) . '/on-air/js/on-air.js', array(), '1.0.1', true );
//}

//add_action( 'wp_enqueue_scripts', 'plugin_scripts' );

/*function arrow_scripts() {
	wp_enqueue_style( 'custom', get_template_directory() . "/custom.css" );
}

add_action( 'wp_enqueue_scripts', 'arrow_scripts' );*/