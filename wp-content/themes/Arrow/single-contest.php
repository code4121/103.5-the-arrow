<?php
/**
 * The Template for displaying all single posts.
 * Added support for IMDB plugin...
 *
 */
get_header(); 
wolf_page_before();
?>
<style>

/*! Typebase.less v0.1.0 | MIT License */
/* Setup */
html {
  /* Change default typefaces here */
  font-family: serif;
  font-size: 137.5%;
  -webkit-font-smoothing: antialiased;
}

/* Copy & Lists */
p {
  line-height: 1.5rem;
  margin-top: 1.5rem;
  margin-bottom: 0;
}
ul,
ol {
  margin-top: 1.5rem;
  margin-bottom: 1.5rem;
}
ul li,
ol li {
  line-height: 1.5rem;
}
ul ul,
ol ul,
ul ol,
ol ol {
  margin-top: 0;
  margin-bottom: 0;
}
blockquote {
  line-height: 1.5rem;
  margin-top: 1.5rem;
  margin-bottom: 1.5rem;
}
/* Headings */
h1,
h2,
h3,
h4,
h5,
h6 {
  /* Change heading typefaces here */
  font-family: sans-serif;
  margin-top: 1.5rem;
  margin-bottom: 0;
  line-height: 1.5rem;
}
h1 {
  font-size: 4.242rem;
  line-height: 4.5rem;
  margin-top: 3rem;
}
h2 {
  font-size: 2.828rem;
  line-height: 3rem;
  margin-top: 3rem;
}
h3 {
  font-size: 1.414rem;
}
h4 {
  font-size: 0.707rem;
}
h5 {
  font-size: 0.4713333333333333rem;
}
h6 {
  font-size: 0.3535rem;
}
/* Tables */
table {
  margin-top: 1.5rem;
  border-spacing: 0px;
  border-collapse: collapse;
}
table td,
table th {
  padding: 0;
  line-height: 33px;
}
/* Code blocks */
code {
  vertical-align: bottom;
}
/* Leading paragraph text */
.lead {
  font-size: 1.414rem;
}
/* Hug the block above you */
.hug {
  margin-top: 0;
}
a, a:visited {
  text-decoration:none;
  color: #f928d2;
}
img{
	border-style: solid;
	border-color: blue;
}
</style>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
			if(have_posts()):
				while(have_posts()): the_post(); ?>
				
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						
						<header class="entry-header">
							<a href="<?php echo get_permalink($post->ID);?>">
							<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
								
								<?php the_post_thumbnail( 'extra-large' ); ?>
							<?php else: ?>
								<img width="300" height="250" src="<?php echo do_shortcode('[default_contest_logo_url]');?>" alt="Default Contest Image"/>
							<?php endif; ?>
							</a>
							<!--Title -->
					 		<a href="<?php echo get_permalink($post->ID);?>"> <h3><?php echo get_the_title(); ?></h3> </a>


						</header> <hr />
							<!--Content-->
						<div class="entry-content">

							<?php the_content(); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'wolf' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
						</div><!-- .entry-content -->
						
						<footer class="entry-meta">
							<?php edit_post_link( __( 'Edit', 'wolf' ), '<span class="edit-link">', '</span>' ); ?>
						</footer><!-- .entry-meta -->
							
					</article><!-- #post -->
				
				<?php endwhile;
		
			
			else: //no posts found
				get_template_part('partials/content', 'none');
			
			endif; ?>
			
		</div><!-- #content -->
	</div><!-- #primary -->

<?php 
get_sidebar();
wolf_page_after(); // after page hook
get_footer(); 