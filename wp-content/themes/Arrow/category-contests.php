<?php
/**
 * The Contest Page Template file - Temporary bandaid for for the app.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */
get_header();

?>

<style>
ul.rig {
	list-style: none;
	font-size: 0px;
	margin-left: -2.5%; /* should match li left margin */
}
ul.rig li {
	display: inline-block;
	padding: 10px;
	margin: 0 0 2.5% 2.5%;
	background: #fff;
	border: 1px solid #ddd;
	font-size: x-small;
	
	vertical-align: top;
	box-shadow: 0 0 5px #ddd;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}
ul.rig li img {
	max-width: 100%;
	height: auto;
	margin: 0 0 10px;
}
ul.rig li h3 {
	margin: 0 0 5px;
	 
}
ul.rig li p {
	font-size: .9em!important;
	line-height: 1.5em;
	color: #999;
}
/* class for 2 columns */
ul.rig.columns-2 li {
	width: 47.5%; /* this value + 2.5 should = 50% */
}
/* class for 3 columns */
ul.rig.columns-3 li {
	width: 30.83%; /* this value + 2.5 should = 33% */
}
/* class for 4 columns */
ul.rig.columns-4 li {
	width: 22.5%; /* this value + 2.5 should = 25% */
}
 
@media (max-width: 480px) {
	ul.grid-nav li {
		display: block;
		margin: 0 0 5px;
	}
	ul.grid-nav li a {
		display: block;
	}
	ul.rig {
		margin-left: 0;
	}
	ul.rig li {
		width: 100% !important; /* over-ride all li styles */
		margin: 0 0 20px;
	}
}

@import "compass/css3";

/* Martin Wolf CodePen Standard */

@import url(http://fonts.googleapis.com/css?family=Open+Sans:400,600,700);

* {
  margin: 0;
  padding: 0;
  @include box-sizing(border-box);
}

body {
  padding: 3em 2em;
  font-family: 'Open Sans', Arial, sans-serif;
  color: #cf6824;
  background: #f7f5eb;
}

/* END Martin Wolf CodePen Standard */


$font-size: 26px;
$line-height: 1.4;
$lines-to-show: 3;

ul.rig li h3 {
  display: block; /* Fallback for non-webkit */
  display: -webkit-box;
  max-width: 400px;
  height: $font-size*$line-height*$lines-to-show; /* Fallback for non-webkit */
  margin: 0 auto;
  font-size: $font-size;
  line-height: $line-height;
  -webkit-line-clamp: $lines-to-show;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;

}
</style>

<?php
 
//wolf_page_before(); // before page hook
?>
	<div id="primary" >
		<div id="content" class="site-content" role="main">
			<h1 style="text-align:center;"> Contests </h1> <hr /> <br />
			<h2> On Air / Web Contests </h2> 
		<?php if ( have_posts() ) : ?>
			<ul class="rig columns-3"> 

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<li >	
				 		<!--thumbnail -->
				 	<a href="<?php echo get_permalink($post->ID);?>">
					<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						
						<?php the_post_thumbnail( 'extra-large' ); ?>
					<?php else: ?>
						<img width="300" height="250" src="<?php echo do_shortcode('[default_contest_logo_url]');?>" alt="Default Contest Image"/>
					<?php endif; ?>
					</a>
					 	<!--Title -->
					 <a href="<?php echo get_permalink($post->ID);?>"> <h3><?php echo get_the_title(); ?></h3> </a>
					 	<!--Content-->
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'wolf' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
					
					
				</li>
			<?php endwhile; ?>
 			</ul>

			<?php wolf_pagination(); ?>

		<?php else : ?>
			<?php //get_template_part( 'partials/content', 'none' ); ?>
		<?php endif; ?>
		<hr \>
		<!--Social Media/Aptivada Embed -->
		<h2> Social Media Contests </h2>
		<div id="aptivada_app" data-app-id="165976433" data-app-type="contest_grid" data-mobile-redirect="0" data-height="1200" style="background:#ffffff url(https://cdn2.aptivada.com/images/iframeLoader.gif) no-repeat center; min-height:500px;"></div>
			<script src="//www.aptivada.com/js/all.js"></script>
		<!-- END Social Media / Aptivada Embed -->
        <?php  ?>
        </div><!-- #content -->
	</div><!-- #primary -->

<?php 
get_sidebar();
wolf_page_after(); // after page hook
get_footer(); 
?>