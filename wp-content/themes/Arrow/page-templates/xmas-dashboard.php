<?php
/*
Template Name: Xmas Dashboard
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>103.5 The Arrow - Dashboard</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link rel="stylesheet" src="new-tab.css">
    <link rel="stylesheet" src="/wp-content/themes/Arrow/style.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,900' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="http://1035thearrow.com/wp-content/themes/Arrow/images/favicons/favicon.ico">
    <link rel="apple-touch-icon" href="http://1035thearrow.com/wp-content/themes/live/images/favicons/touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="http://1035thearrow.com/wp-content/themes/live/images/favicons/touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="http://1035thearrow.com/wp-content/themes/live/images/favicons/touch-icon-114x114.png">
    <meta property="og:url" content="http://1035thearrow.com/dashboard/" />
    <meta property="og:title" content="103.5 The Arrow's Dashboard" />
    <meta property="og:description" content="Listen live to 103.5 The Arrow, win tickets, watch our latest videos, and more!" />
    <meta property="og:image" content="http://1035thearrow.com/wp-content/themes/Arrow/images/dashboard/2016-logo.png" />

    <?php

    //check to see if we've passed in the no-ga URL parameter
    //this is to ensure that we don't count a pageview in the iframes
    if(get_query_var('no-ga', 0) == 0){ ?>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-58886804-3', 'auto', 'dashboardTracker');
            ga('dashboardTracker.send', 'pageview');

        </script>

        <script type='text/javascript'>
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            (function() {
                var gads = document.createElement('script');
                gads.async = true;
                gads.type = 'text/javascript';
                var useSSL = 'https:' == document.location.protocol;
                gads.src = (useSSL ? 'https:' : 'http:') +
                    '//www.googletagservices.com/tag/js/gpt.js';
                var node = document.getElementsByTagName('script')[0];
                node.parentNode.insertBefore(gads, node);
            })();
        </script>

        <script type='text/javascript'>
            googletag.cmd.push(function() {
                googletag.defineSlot('/75097902/arrow-dashboard-monitor', [300, 250], 'div-gpt-ad-1464721228849-0').addService(googletag.pubads());
                googletag.defineSlot('/75097902/Arrow-Video-Dashboard', [640, 480], 'div-gpt-ad-1470422653843-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
        </script>

    <?php } ?>


    <style type="text/css">

        @media only screen and (max-width: 1680px) and (min-width: 1680px) {
            .tag .tag-sponsor-image {
                left: -39px!important;
            }
            .jc-santa img {
                width: 67%!important;
            }
        }
        @media only screen and (max-width: 1440px) {
            .tag-sponsor-image {
                left: -34px!important;
            }

        }
        @media only screen and (max-width: 1280px) and (min-width: 1280px) {
            .tag .tag-sponsor-image {
                left: -30px!important;
            }
        }
        @media only screen and (max-device-width: 1024px) {
            .arrow-logo {
                width: 40%!important;
            }
            .tag {
                width: 75%!important;
                top: 9%!important;
                left: 26%!important;
                height: 0%!important;
            }
            .tag .tag-container-image {
                width: 80%!important;
            }
            .santa-bag {
                width: 60%!important;
                top: 30%!important;
                right: 13%!important;
            }
            .jc-santa {
                bottom: 0!important;
            }
            .snowdrift img{
                bottom: 0!important;
            }

        }
        @media only screen and (max-device-width: 750px) {

            .arrow-logo {
                width: 40%!important;
            }
            .tag {
                width: 75%!important;
                top: 9%!important;
                left: 26%!important;
                height: 0%!important;
            }
            .tag .tag-container-image {
                width: 80%!important;
            }
            .tag .tag-sponsor-image {
                width: 92.5%!important;
                left: -45px!important;
                top: -50px!important;
            }
            .santa-bag {
                width: 65%!important;
                top: 35%!important;
                right: 13%!important;
            }
            .click-here {
                top: 52%!important;
                right: 9%!important;
            }
        }
        html {
            background: url('<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/xmas/background.png') no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            overflow: hidden;
        }
        .arrow-logo {
            position: absolute;
            top: 0;
            left: 0;
            width: 20%;
        }
        .arrow-logo img {
            width: 100%;
        }
        .icon-row {
            position: absolute;
            right: 55px;
            height: 84px;
            top: 10px;
        }
        .icon {
            float: right;
        }
        .icon-row .icon:nth-child(n+2) {
            margin-right: 10px;
        }
        .icon img {
            filter: url("data:image/svg+xml;utf8,<svg version='1.1' xmlns='http://www.w3.org/2000/svg' height='0'><filter id='greyscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0' /></filter></svg>#greyscale");
            transition: .5s;
            -moz-transition: .5s; /* Firefox 4 */
            -webkit-transition: .5s; /* Safari and Chrome */
            -o-transition: .5s; /* Opera */
        }
        .icon img:hover {
            opacity: 1;
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
            filter: none;
            transition: .5s;
            -moz-transition: .5s; /* Firefox 4 */
            -webkit-transition: .5s; /* Safari and Chrome */
            -o-transition: .5s; /* Opera */
        }
        .jc-santa {
            position: absolute;
            bottom: 13%;
            left: 2%;
            width: 100%;
        }
        .jc-santa img {
            width: 77%;
        }
        .snowdrift {
            position: absolute;
            bottom: 0;
            width: 100%;
            min-height: 500px;
            overflow: hidden;
        }
        .snowdrift img {
            position: absolute;
            left: 0;
            bottom: 0px;
        }
        .tag {
            position: absolute;
            bottom: 55%;
            width: 37%;
            -webkit-transform: rotate(-7deg);
            -moz-transform: rotate(-7deg);
            -ms-transform: rotate(-7deg);
            -o-transform: rotate(-7deg);
            transform: rotate(-7deg);
        }
        .tag .tag-sponsor-image {
            position: absolute;
            width: 100%;
        }
        .bag-sponsor-image-container {
            position: absolute;
            text-align: center;
            top: 0%;
            margin-top: 0px;
            right: 0;
        }
        .bag-sponsor-image {
            width: 100%;
        }
        .santa-bag {
            position: absolute;
            bottom: 0;
            right: 0;
            width: 45%;
            min-height: 60%;
            overflow: hidden;
        }
        .santa-bag .bag-image {
            position: absolute;
            width: 100%;
            right: 0;
        }
        .click-here {
            position: absolute;
            bottom: 5%;
            right: 5%;
        }
        .play-bar {
            position: absolute;
            width: 100%;
            bottom: 0;
        }
        .play-bar img {
            position: absolute;
            width: 100%;
            bottom: 0;
        }
        img { text-decoration: none; border: 0; }
        footer#colophon { display: none; }
        #myatu_bgm_pin_it_btn, #myatu_bgm_img_group, footer#colophon { display: none; }

    </style>

</head>
<body>
<div class="dashboard-wrapper">
    <iframe style="border: 0px;margin:0px;padding:0px;width:100%;height:80px;display:none;" src="http://home.brandthunder.com/search?tid=kslbonneville"></iframe>

    <div class="arrow-logo">
        <a href="http://1035thearrow.com/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/xmas/arrow-logo.png" alt="103.5 The Arrow"></a>
    </div>

    <div class="jc-santa">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/xmas/jon-carter-santa.png" />
    </div>

    <div class="snowdrift">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/xmas/snowdrift.png" />
    </div>

    <div class="santa-bag">
        <img class="bag-image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/xmas/Santa-Bag-BKG.png" />
        <div class="bag-sponsor-image-container">
            <img class="bag-sponsor-image" />
        </div>
        <img class="bag-image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/xmas/Santa-Bag-FRONT.png" />
    </div>
    <div class="click-here">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/xmas/click-here.png" />
    </div>

    <div class="tag">
        <a target="_blank"><img class="tag-sponsor-image"></a>
    </div>

</div>

<div class="icon-row">
    <div class="icon"><a href="https://www.instagram.com/1035thearrow/" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Dashboard - Instagram'});"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/xmas/instagram-icon.png" alt="Check us out on Instagram!" title="Check us out on Instagram!"></a></div>
    <div class="icon"><a href="https://twitter.com/share?url=http://1035thearrow.com/dashboard/&text=Listen+live+to+103.5+The+Arrow%2C+win+tickets%2C+watch+our+latest+videos%2C+and+more%21" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Dashboard - Twitter Share'});window.open(this.href,'Share on Twitter','width=560,height=480');return false;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/xmas/twitter-icon.png" alt="Share this on Twitter!" title="Share this on Twitter!"></a></div>
    <div class="icon"><a href="http://www.facebook.com/sharer.php?u=http://1035thearrow.com/dashboard/" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Dashboard - Facebook Share'});window.open(this.href,'','width=560,height=480');return false;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/xmas/facebook-icon.png" alt="Share this on Facebook!" title="Share this on Facebook!"></a></div>
    <!--<div class="icon"><a href="http://home.brandthunder.com/1035thearrow/?extension" target="_blank" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Gear Icon for New Tab Theme'});"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/gear-icon.png" alt="Install this page as your New Tab theme" title="Install this page as your New Tab theme"></a></div>-->
</div>

<style type="text/css">
    #bottom-bar {display: none !important;}
    #playerbar { height: 0px!important;width:100%!important; }
</style>

<?php get_footer(); ?>

<script type="text/javascript">

    // add the corresponding santabag image to this array when you need to add a new client
    var bagImages = [
        "BAG-waldi.png",
        "BAG-carter.png",
        "BAG-kelley.png",
        "BAG-carter2.png",
        "BAG-michaels.png",
        "BAG-OdysseyDance.png",
        "BAG-OdysseyDance.png",
        "BAG-OdysseyDance.png",
        "BAG-OdysseyDance.png",
        "BAG-OdysseyDance.png",
        "BAG-OdysseyDance.png",
        "BAG-BMW.png",
        "BAG-BMW.png",
        "BAG-BMW.png",
        "BAG-BMW.png",
        "BAG-BMW.png",
        "BAG-BMW.png"
    ];
    // add the corresponding tag image to this array along with the outbound link when you need to add a new client
    var tagImages = [
        ['CARD-waldi.png','http://1035thearrow.com/musicjocks/mark-waldi/'],
        ['CARD-carter.png','http://1035thearrow.com/musicjocks/jon-carter/'],
        ['CARD-kelley.png','http://1035thearrow.com/musicjocks/sue-kelley/'],
        ['CARD-carter.png','http://1035thearrow.com/musicjocks/jon-carter/'],
        ['CARD-michaels.png','http://1035thearrow.com/musicjocks/gary-g-man-michaels/'],
        ['CARD-OdysseyDance.png','http://odysseydance.com/'],
        ['CARD-OdysseyDance.png','http://odysseydance.com/'],
        ['CARD-OdysseyDance.png','http://odysseydance.com/'],
        ['CARD-OdysseyDance.png','http://odysseydance.com/'],
        ['CARD-OdysseyDance.png','http://odysseydance.com/'],
        ['CARD-OdysseyDance.png','http://odysseydance.com/'],
        ['CARD-BMW.png','http://utahbmw.com'],
        ['CARD-BMW.png','http://utahbmw.com'],
        ['CARD-BMW.png','http://utahbmw.com'],
        ['CARD-BMW.png','http://utahbmw.com'],
        ['CARD-BMW.png','http://utahbmw.com'],
        ['CARD-BMW.png','http://utahbmw.com']
    ];

    $(document).ready(function () {

        $.preloadImages = function() {
            for (var i = 0; i < arguments.length; i++) {
                jQuery("<img />").attr("src", "<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/xmas/"+arguments[i]);
            }
        };

        //add both the santabag image and the card image for new sponsors here
        $.preloadImages(
            "BAG-waldi.png",
            "CARD-waldi.png",
            "BAG-carter.png",
            "CARD-carter.png",
            "BAG-kelley.png",
            "CARD-kelley.png",
            "BAG-carter2.png",
            "CARD-michaels.png",
            "BAG-michaels.png",
            "BAG-OdysseyDance.png",
            "CARD-OdysseyDance.png",
            "BAG-BMW.png",
            "CARD-BMW.png"
        );

        var randomStartImg = (Math.floor(Math.random() * bagImages.length));

        jQuery('.bag-sponsor-image').attr("src", "<?php echo get_stylesheet_directory_uri()."/images/dashboard/xmas/"; ?>" + bagImages[randomStartImg] + "");
        ga('dashboardTracker.send','event',{'eventCategory': 'Bag','eventAction': 'View','eventLabel': 'Christmas Dashboard - '+ bagImages[randomStartImg] + ''});
        jQuery('.tag-sponsor-image').attr("src", "<?php echo get_stylesheet_directory_uri()."/images/dashboard/xmas/"; ?>" + tagImages[randomStartImg][0] + "");
        jQuery('.tag a').attr("href", "" + tagImages[randomStartImg][1] + "");
        jQuery('.tag a').attr("onclick", "ga('dashboardTracker.send','event',{'eventCategory': 'Tag','eventAction': 'Click','eventLabel': 'Christmas Dashboard - "+ tagImages[randomStartImg][1] + "'});" );

    });

    var currentlyAnimating = false;
    var bagTimer = setInterval(function(){
        rotateBag();
    },60000);

    jQuery('.santa-bag, .click-here').click(function(){
        rotateBag();
        clearInterval(bagTimer);
        bagTimer = setInterval(function(){
            rotateBag();
        },60000);
    });

    function rotateBag(){

        if (currentlyAnimating) {
            return;
        }

        currentlyAnimating = true;

        jQuery('.bag-sponsor-image').animate({
            width: "0%",
            height: "0%",
            marginTop: "450px",
            opacity: "0"
        }, 1000, "linear", function(){

            //check the current sponsor, if the next loaded sponsor is the same, reload a new one
            //avoids loading the same sponsor twice in a row
            var currentImg = jQuery('.bag-sponsor-image').attr('src');
            var randomImg = (Math.floor(Math.random() * bagImages.length));
            if (currentImg.indexOf(""+bagImages[randomImg]+"") != -1){
                randomImg = (Math.floor(Math.random() * bagImages.length));
            }

            jQuery('.bag-sponsor-image').attr("src", "<?php echo get_stylesheet_directory_uri() . "/images/dashboard/xmas/"; ?>" + bagImages[randomImg] + "");
            ga('dashboardTracker.send', 'event', {
                'eventCategory': 'Bag',
                'eventAction': 'View',
                'eventLabel': 'Christmas Dashboard - ' + bagImages[randomImg] + ''
            });

            jQuery('.tag-sponsor-image').attr("src", "<?php echo get_stylesheet_directory_uri() . "/images/dashboard/xmas/"; ?>" + tagImages[randomImg][0] + "");
            jQuery('.tag a').attr("href", "" + tagImages[randomImg][1] + "");
            jQuery('.tag a').attr("onclick", "ga('dashboardTracker.send','event',{'eventCategory': 'Tag','eventAction': 'Click','eventLabel': 'Christmas Dashboard - " + tagImages[randomImg][1] + "});");

            jQuery('.bag-sponsor-image').fadeIn(500);
        } );

        jQuery('.tag-sponsor-image').fadeOut(200);

        jQuery('.bag-sponsor-image').animate({
            width: "100%",
            marginTop: "0px",
            opacity: "1"
        }, 1000, "linear", function() {
            jQuery('.tag-sponsor-image').fadeIn(200);
            currentlyAnimating = false;
        });

    }
</script>

</body>
</html>