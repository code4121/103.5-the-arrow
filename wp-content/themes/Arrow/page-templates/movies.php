<?php
/*
Template Name: Movies
*/
get_header(); 

wolf_page_before();
?>

	<div id="main-content" class="wrap">
		<div id="primary" class="site-content">
			<div id="content" role="main">
                            <h1><?php echo the_title(); ?></h1>
                            <h5>POWERED BY:</h5>
                            <a href="http://megaplextheatres.com"><img class="aligncenter size-full wp-image-941" src="http://arrow.d.bonnint.com/wp-content/uploads/2015/01/MegaplexLogoOfficialWhiteLettering.png" alt="Larry H Miller Megaplex" width="402" height="167" /></a><br><br><br>
				<?php
				$loop = new WP_Query("post_type=post&paged=$paged&category_name=movies");
				if($loop->have_posts()): 
					while ($loop->have_posts()) : $loop->the_post();
					global $more;
					$more = 0;
					get_template_part('partials/loop', 'movies');

					endwhile;
					?>
					<a href="<?php echo wolf_get_blog_url(); ?>" class="more-link"><?php _e('More', 'wolf'); ?></a>
					<?php
				else: 
					get_template_part('no-results', 'index'); 
	                                     		

	                                    	endif; ?>
	                                     
			</div><!-- #content -->
		</div><!-- #primary .site-content -->
		<?php get_sidebar(); ?>
	</div><!-- .wrap #main-content -->
<?php 
wolf_page_after();
get_footer(); 
?>