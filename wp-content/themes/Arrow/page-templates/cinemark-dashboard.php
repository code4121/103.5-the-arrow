<?php
/*
Template Name: Cinemark Dashboard
*/
?>

<?php
$sponsor = "Cinemark";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>103.5 The Arrow - Dashboard</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <link rel="stylesheet" src="new-tab.css">
    <link rel="stylesheet" src="/wp-content/themes/Arrow/style.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,900' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="http://1035thearrow.com/wp-content/themes/Arrow/images/favicons/favicon.ico">
    <link rel="apple-touch-icon" href="http://1035thearrow.com/wp-content/themes/live/images/favicons/touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="http://1035thearrow.com/wp-content/themes/live/images/favicons/touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="http://1035thearrow.com/wp-content/themes/live/images/favicons/touch-icon-114x114.png">
    <meta property="og:url" content="http://1035thearrow.com/dashboard/" />
    <meta property="og:title" content="103.5 The Arrow's Dashboard" />
    <meta property="og:description" content="Listen live to 103.5 The Arrow, win tickets, watch our latest videos, and more!" />
    <meta property="og:image" content="http://1035thearrow.com/wp-content/themes/Arrow/images/dashboard/2016-logo.png" />
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-58886804-3', 'auto', 'dashboardTracker');
        ga('dashboardTracker.send', 'pageview');

    </script>
    <script type='text/javascript'>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
        (function() {
            var gads = document.createElement('script');
            gads.async = true;
            gads.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            gads.src = (useSSL ? 'https:' : 'http:') +
                '//www.googletagservices.com/tag/js/gpt.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(gads, node);
        })();
    </script>

    <script type='text/javascript'>
        googletag.cmd.push(function() {
            googletag.defineSlot('/75097902/arrow-dashboard-monitor', [300, 250], 'div-gpt-ad-1464721228849-0').addService(googletag.pubads());
            googletag.defineSlot('/75097902/Arrow-Video-Dashboard', [640, 480], 'div-gpt-ad-1470422653843-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
    <!--[if lt IE 10]>
    <script>
        document.createElement('video');
    </script>
    <![endif]-->

    <style type="text/css">
        @media (max-height: 850px) {
            body {
                overflow: hidden;
            }
            html {
                -moz-transform: translate(-7%,-7%) scale(0.85);
                -webkit-transform: translate(-7%,-7%) scale(0.85);
                transform: translate(-7%,-7%) scale(0.85);
                width: 1600px;
            }
            .front-cover {
                top: -3px!important;
            }
            .front-cover img {
                width: 1600px;
            }
            .icon-row {
                right: 175px!important;
            }
            .mic {
                z-index: 99;
            }
        }
        @media (max-height: 799px) {
            body {
                overflow: hidden;
            }
            html {
                -moz-transform: translate(-9%,-9%) scale(0.81);
                -webkit-transform: translate(-9%,-9%) scale(0.81);
                transform: translate(-9%,-9%) scale(0.81);
                width: 1600px;
            }
            .front-cover {
                top: -3px!important;
            }
            .front-cover img {
                width: 1600px;
            }
            .icon-row {
                right: 175px!important;
            }
            .mic {
                z-index: 99;
            }
        }
        @media (max-height: 750px) {
            body {
                overflow: hidden;
            }
            html {
                -moz-transform: translate(-12%,-12%) scale(0.75);
                -webkit-transform: translate(-12%,-12%) scale(0.75);
                transform: translate(-12%,-12%) scale(0.75);
                width: 1600px;
            }
            .front-cover {
                top: -3px!important;
            }
            .front-cover img {
                width: 1600px;
            }
            .icon-row {
                right: 175px!important;
            }
            .mic {
                z-index: 99;
            }
        }
        @media (max-width: 1650px) and (min-width: 1600px) {
            body {
                overflow: hidden;
            }
            html {
                -moz-transform: translate(0%,-2%) scale(0.95);
                -webkit-transform: translate(0%,-2%) scale(0.95);
                transform: translate(0%,-2%) scale(0.95);
                width: 1600px;
            }
            .front-cover {
                top: -3px!important;
            }
            .front-cover img {
                width: 1600px;
            }
            .icon-row {
                right: 175px!important;
            }
            .mic {
                z-index: 99;
            }
        }
        @media (max-width: 1599px) and (min-width: 1500px) {
            body {
                overflow: hidden;
            }
            html {
                -moz-transform: translate(0%,-5%) scale(0.95);
                -webkit-transform: translate(0%,-5%) scale(0.95);
                transform: translate(0%,-2%) scale(0.95);
                width: 1600px;
            }
            .front-cover {
                top: -5px!important;
            }
            .front-cover img {
                width: 1600px;
            }
            .icon-row {
                right: 175px!important;
            }
            .mic {
                z-index: 99;
            }
        }
        @media (max-width: 1499px) and (min-width: 1400px) {
            body {
                overflow: hidden;
            }
            html {
                -moz-transform: translate(-4%,-4%) scale(0.9);
                -webkit-transform: translate(-4%,-4%) scale(0.9);
                transform: translate(-4%,-4%) scale(0.9);
                width: 1600px;
            }
            .front-cover {
                top: -5px!important;
            }
            .front-cover img {
                width: 1600px;
            }
            .icon-row {
                right: 175px!important;
            }
            .mic {
                z-index: 99;
            }
        }
        @media (max-width: 1399px) and (min-width: 1300px) {
            body {
                overflow: hidden;
            }
            html {
                -moz-transform: translate(-7%,-7%) scale(0.85);
                -webkit-transform: translate(-7%,-7%) scale(0.85);
                transform: translate(-7%,-7%) scale(0.85);
                width: 1600px;
            }
            .front-cover {
                top: -5px!important;
            }
            .front-cover img {
                width: 1600px;
            }
            .icon-row {
                right: 175px!important;
            }
            .mic {
                z-index: 99;
            }
        }
        @media (max-width: 1299px) and (min-width: 1200px) {
            body {
                overflow: hidden;
            }
            html {
                -moz-transform: translate(-10%,-10%) scale(0.8);
                -webkit-transform: translate(-10%,-10%) scale(0.8);
                transform: translate(-10%,-10%) scale(0.8);
                width: 1600px;
            }
            .front-cover {
                top: -5px!important;
            }
            .front-cover img {
                width: 1600px;
            }
            .icon-row {
                right: 175px!important;
            }
            .mic {
                z-index: 99;
            }
        }
        @media (max-width: 1199px) and (min-width: 1100px) {
            body {
                overflow: hidden;
            }
            html {
                -moz-transform: translate(-14%,-14%) scale(0.7);
                -webkit-transform: translate(-14%,-14%) scale(0.7);
                transform: translate(-14%,-14%) scale(0.7);
                width: 1600px;
            }
            .front-cover {
                top: -5px!important;
            }
            .front-cover img {
                width: 1600px;
            }
            .icon-row {
                right: 175px!important;
            }
            .mic {
                z-index: 99;
            }
        }
        @media (max-width: 1099px) and (min-width: 1000px) {
            body {
                overflow: hidden;
            }
            html {
                -moz-transform: translate(-18%,-18%) scale(0.62);
                -webkit-transform: translate(-18%,-18%) scale(0.62);
                transform: translate(-18%,-18%) scale(0.62);
                width: 1600px;
            }
            .front-cover {
                top: -5px!important;
            }
            .front-cover img {
                width: 1600px;
            }
            .icon-row {
                right: 175px!important;
            }
            .mic {
                z-index: 99;
            }
        }
        @media (max-width: 999px) and (min-width: 900px) {
            body {
                overflow: hidden;
            }
            html {
                -moz-transform: translate(-20%,-20%) scale(0.58);
                -webkit-transform: translate(-20%,-20%) scale(0.58);
                transform: translate(-20%,-20%) scale(0.58);
                width: 1600px;
            }
            .front-cover {
                top: -5px!important;
            }
            .front-cover img {
                width: 1600px;
            }
            .icon-row {
                right: 175px!important;
            }
            .mic {
                z-index: 99;
            }
        }
        @font-face {
            font-family: 'MyFont';
            src: url('<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/fonts/steelfishrg.ttf');
        }
        body {
            margin: 0 auto;
            background-color: black;
        }
        .dashboard-wrapper{
            position: relative;
            width: 1600px;
            height: 900px;
            margin: 0 auto;
            overflow: hidden;
            background: url('<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/full-bg.png');
            background-repeat: no-repeat;
            background-position:top center;
            -webkit-background-size: contain;
            -moz-background-size: contain;
            -o-background-size: contain;
            background-size: contain;
        }

        .overlay {
            position: absolute;
            z-index: 100;
        }

        video { display: block; }
        video#bgvid {
            position: absolute;
            top: -85px;
            left: 0;
            min-width: 1600px;
            min-height: 753px;
            max-width: 1600px;
            max-height: 753px;
            width: auto;
            height: auto;
            z-index: -100;
        }


        .icon-row {
            position: absolute;
            right: 55px;
            height: 84px;
            top: 10px;
        }
        .icon {
            float: right;
        }
        .icon-row .icon:nth-child(n+2) {
            margin-right: 10px;
        }
        .icon img { width: 35px; }
        .icon img {
            opacity: .8;
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
            filter: gray;
            filter: url("data:image/svg+xml;utf8,<svg version='1.1' xmlns='http://www.w3.org/2000/svg' height='0'><filter id='greyscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0' /></filter></svg>#greyscale");
            transition: .5s;
            -moz-transition: .5s; /* Firefox 4 */
            -webkit-transition: .5s; /* Safari and Chrome */
            -o-transition: .5s; /* Opera */
        }
        .icon img:hover {
            opacity: 1;
            -webkit-filter: grayscale(0%);
            filter: none;
            transition: .5s;
            -moz-transition: .5s; /* Firefox 4 */
            -webkit-transition: .5s; /* Safari and Chrome */
            -o-transition: .5s; /* Opera */
        }
        .cinemark-logo {
            width: 1600px;
            text-align: center;
            position: absolute;
            margin-top: 84px;
        }
        .arrow-logo {
            width: 1600px;
            text-align: center;
            position: absolute;
            margin-top: 220px;
        }
        .arrow-logo img {
            width: 185px;
            transition: .1s;
            -moz-transition: .1s; /* Firefox 4 */
            -webkit-transition: .1s; /* Safari and Chrome */
            -o-transition: .1s; /* Opera */
        }
        .arrow-logo img:hover {
            width: 190px;
            transition: .1s;
            -moz-transition: .1s; /* Firefox 4 */
            -webkit-transition: .1s; /* Safari and Chrome */
            -o-transition: .1s; /* Opera */
        }
        .mic {
            position: absolute;
            left: 0;
            top: 0;
        }
        .mic img { width: 420px; }
        .lava-lamp {
            position: relative;
            width: 125px;
            margin-top: 195px;
            margin-left: 45px;
        }
        .lava-lamp img { height: 388px; }
        .monitor-video {
            position: relative;
            margin-top: -349px;
            margin-left: 155px;
            width: 496px;
        }
        .monitor-ad-slot {
            position: relative;
            width: 300px;
            height: 250px;
            margin-top: -247px;
            margin-left: 915px;
        }
        .now-playing-container {
            width: 250px;
            height: 250px;
            background-size: contain;
            margin: 0 auto;
            color: white;
            background-color: black;
        }
        .now-playing-container img {
            width: 281px!important;
            height: 281px!important;
        }
        .now-playing-img {
            position: relative;
            width: 281px;
            margin-left: 1234px;
            margin-top: -332px;
        }
        .now-playing-container .listen-header { display: none; }
        #tickets-hand {
            position: absolute;
            width: 300px;
            margin-left: 200px;
            margin-top: 115px;
            transition: .5s;
            -moz-transition: .5s; /* Firefox 4 */
            -webkit-transition: .5s; /* Safari and Chrome */
            -o-transition: .5s; /* Opera */
            overflow: hidden;
        }
        #tickets-hand img { width: 300px; }
        #tickets-hand:hover {
            margin-top: 50px;
            transition: .5s;
            -moz-transition: .5s; /* Firefox 4 */
            -webkit-transition: .5s; /* Safari and Chrome */
            -o-transition: .5s; /* Opera */
        }
        #tickets-popup {
            transition: opacity 1s 0s ease;
            -moz-transition: opacity 1s 0s ease; /* Firefox 4 */
            -webkit-transition: opacity 1s 0s ease; /* Safari and Chrome */
            -o-transition: opacity 1s 0s ease; /* Opera */
            display: none;
            position: absolute;
            bottom: 150px;
            left: 480px;
            background: #9f1c20;
            font-family: 'MyFont',sans-serif;
            padding: 20px;
            font-size: 35px;
            color: white;
        }
        #tickets-popup a{
            text-decoration: none;
            color: white;

        }
        #tickets-hand:hover + #tickets-popup {
            display:block;
            opacity: 1;
            transition: .5s;
            -moz-transition: .5s; /* Firefox 4 */
            -webkit-transition: .5s; /* Safari and Chrome */
            -o-transition: .5s; /* Opera */
        }
        #tickets-popup:hover {
            display:block;
        }
        #tickets-popup:hover > #tickets-hand {
            margin-top: 50px;
        }
        #middle-graphic {
            position: absolute;
            text-align: center;
            margin-top: -70px;
            margin-left: 635px;
        }
        #middle-graphic img {
            width:270px;
            transition: .1s;
            -moz-transition: .1s; /* Firefox 4 */
            -webkit-transition: .1s; /* Safari and Chrome */
            -o-transition: .1s; /* Opera */
        }
        #middle-graphic img:hover {
            transform: rotate(5deg);
            -ms-transform: rotate(5deg);
            -webkit-transform: rotate(5deg);
            transition: .1s;
            -moz-transition: .1s; /* Firefox 4 */
            -webkit-transition: .1s; /* Safari and Chrome */
            -o-transition: .1s; /* Opera */
        }
        #red-phone {
            position: absolute;
            width: 335px;
            margin-top: 50px;
            margin-left: 1225px;
        }
        #red-phone img { width: 335px; }
        #phone-number-popup {
            display: none;
            position: absolute;
            height: 125px;
            width: 270px;
            margin-top: 112px;
            margin-left: 935px;
            padding: 0 8px;
            background: #9f1c20;
            font-family: 'MyFont',sans-serif;
        }
        #phone-number-popup:hover { display: block; }
        #phone-number-popup .call-text {
            color: white;
            font-size: 40px;
            -webkit-font-smoothing: antialiased;
        }
        #phone-number-popup .number {
            color: white;
            font-size: 80px;
            line-height: 70px;
            -webkit-font-smoothing: antialiased;
        }
        #phone-number-popup img {
            position: absolute;
            width: 75px;
            margin-left: 10px;
            margin-top: -41px;
        }
        #red-phone:hover + #phone-number-popup { display: block; }
        #carter-chair {
            position: absolute;
            right: -620px;
            top: 680px;
        }
        .play-bar {
            position: absolute;
            width: 100%;
            bottom: 0;
        }
        .play-bar img {
            position: absolute;
            width: 100%;
            bottom: 0;
        }
        .left-wing {
            position: absolute;
            top: 0;
            width: 160px;
            overflow: hidden;
        }
        .left-wing img {
            height: 900px;
            margin-left: -297px;
        }
        .right-wing {
            position: absolute;
            top: 0;
            right: 0;
            width: 100px;
            overflow: hidden;
        }
        .right-wing img {
            height: 900px;
        }
        .front-cover {
            position: absolute;
            top: -130px;
            z-index: 99999;
        }
        .pd-flag {
            position: absolute;
            margin-left: 400px;
            margin-top: -800px;
        }

        img {text-decoration: none; border: 0px}

        @media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
            /* IE10+ specific styles go here */
            #phone-number-popup .call-text {
                color: white;
                font-size: 2em !important;
                -webkit-font-smoothing: antialiased;
            }
            #phone-number-popup .number {
                color: white;
                font-size: 2.6em !important;
                line-height: 90px !important;
                -webkit-font-smoothing: antialiased;
            }
        }

    </style>
    <!--[if IE]>
    <style>

        #phone-number-popup .call-text {
            color: white;
            font-size: 2em !important;
            -webkit-font-smoothing: antialiased;
        }
        #phone-number-popup .number {
            color: white;
            font-size: 2.6em !important;
            line-height: 90px !important;
            -webkit-font-smoothing: antialiased;
        }
    </style>
    <![endif]-->
</head>
<body>
<div class="front-cover">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/cinemark/cinemark-curtain-revised.jpg">
</div>

<div class="dashboard-wrapper">
    <!--<div class="overlay" onclick="$(this).hide();">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/overlay.png">
    </div>-->
    <video autoplay loop id="bgvid">
        <source src="/wp-content/uploads/2016/05/stage-alt1-slower.mp4" type="video/mp4">
    </video>

    <div class="pd-flag">
        <img class="animated" interval="50" max="120" pattern="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/cinemark/flag-images/Flag_#.png" />
    </div>
    <div class="cinemark-logo">
        <a href="http://www.cinemark.com"><img src="/wp-content/themes/Arrow/images/dashboard/cinemark/cinemark-logo.png" alt="Cinemark"></a>
    </div>
    <div class="arrow-logo">
        <a href="http://1035thearrow.com/" target="_blank"><img src="/wp-content/themes/Arrow/images/dashboard/2016-logo.png" alt="103.5 The Arrow"></a>
    </div>
    <div class="icon-row">
        <div class="icon"><a href="https://twitter.com/share?url=http://1035thearrow.com/dashboard/&text=Listen+live+to+103.5+The+Arrow%2C+win+tickets%2C+watch+our+latest+videos%2C+and+more%21" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Dashboard - Twitter Share - <?php echo $sponsor; ?>'});window.open(this.href,'Share on Twitter','width=560,height=480');return false;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/twitter-icon.png" alt="Share this on Twitter!" title="Share this on Twitter!"></a></div>
        <div class="icon"><a href="http://www.facebook.com/sharer.php?u=http://1035thearrow.com/dashboard/" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Dashboard - Facebook Share - <?php echo $sponsor; ?>'});window.open(this.href,'','width=560,height=480');return false;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/facebook-icon.png" alt="Share this on Facebook!" title="Share this on Facebook!"></a></div>
        <div class="icon"><a href="http://home.brandthunder.com/1035thearrow/?extension" target="_blank" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Dashboard - Gear Icon for New Tab Theme - <?php echo $sponsor; ?>'});"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/gear-icon.png" alt="Install this page as your New Tab theme." title="Install this page as your New Tab theme."></a></div>
    </div>

    <div class="mic">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/cinemark/mic.png" usemap="#micmap" alt="<?php echo $sponsor; ?>">
    </div>
    <div style="height:84px;"></div>
    <div class="lava-lamp">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/lamp.gif">
    </div>
    <div class="monitor-video">
        <iframe width="496" height="280" src="https://www.youtube.com/embed/Zm-ElNTO2mc" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="monitor-ad-slot">
        <!--<a href="https://www.americafirst.com/loans/home-equity-loans/no-closing-costs-home-loan.cfm?cid=arrowdashhl0316" target="_blank" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Dashboard - Monitor Ad - <?php echo $sponsor; ?>'});"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/america-first-credit-union-300x250.png" alt="<?php echo $sponsor; ?>"></a>-->
        <!-- /75097902/arrow-dashboard-monitor -->
        <div id='div-gpt-ad-1464721228849-0' style='height:250px; width:300px;'>
            <script type='text/javascript'>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1464721228849-0'); });
            </script>
        </div>
    </div>
    <div class="now-playing-img">
        <div class="now-playing-container">
            <a href="http://player.listenlive.co/35841"><?php echo do_shortcode('[on_air]'); ?></a>
        </div>
    </div>
    <div id="red-phone">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/red-phone.png" alt="Give us a call at 801-570-1035" title="Give us a call at 801-570-1035">
    </div>
    <div id="phone-number-popup">
        <span class="call-text">GIVE US A RING</span><br />
        <span class="number">801-570-1035</span>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/right-arrow.png">
    </div>
    <div id="carter-chair">
        <a href="http://1035thearrow.com/cinemark/" target="_blank" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Dashboard - Jon Carter Rocket Chair - <?php echo $sponsor; ?>'});"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/cinemark/carter-chair.gif" alt="Jon Carter Luxury Lounger Rocket Chair"></a>
    </div>
    <div id="tickets-hand">
        <a href="http://1035thearrow.com/cinemark/" target="_blank" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Dashboard - Tickets Hand - <?php echo $sponsor; ?>'});"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/cinemark/tickets.png" alt="Click Here to Win Tickets" title="Click here to win tickets!"></a>
    </div>

    <div id="tickets-popup">
        <a href="http://1035thearrow.com/cinemark/">
            Click to win!
        </a>
    </div>
    <div id="middle-graphic">
        <!--<a href="" target="_blank" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Headphones'});"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/headphones.png"></a>-->
        <img id="image1" src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/cinemark/pop1.png" alt="Cinemark Luxury Lounger!">
        <img id="image2" src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/cinemark/pop2.png" alt="Cinemark Luxury Lounger!" style="display:none;">
        <img id="image3" src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/cinemark/pop3.png" alt="Cinemark Luxury Lounger!" style="display:none;">
        <img id="image4" src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/cinemark/pioneer-day-popcorn.png" alt="Cinemark Luxury Lounger!" style="display:none;">
    </div>

    <div class="play-bar">
        <style type="text/css">
            #bottom-bar {display: none; !important}
            #playerbar { height: 0px!important;width:100%!important; }
        </style>
        <!--<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/play-bar.png">-->
    </div>


    <?php get_footer(); ?>
</div>
<div class="left-wing">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/cinemark/cinemark-wing-L.jpg">
</div>
<div class="right-wing">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dashboard/cinemark/cinemark-wing-R.jpg">
</div>
<map name="micmap">
    <area shape="poly" href="http://www.cinemark.com/" onclick="ga('dashboardTracker.send','event',{'eventCategory': 'Click','eventAction': 'Dashboard - Mic Boom - <?php echo $sponsor; ?>'});" coords="76,0,159,56,121,128,84,169,1,118,31,72" target="_blank">
</map>
<script type="text/javascript">
    $(document).ready(function() {
        var timer = setTimeout(function() { jQuery(".front-cover").click(); }, 2000);
        jQuery(".front-cover").click(function(){
            clearTimeout(timer);
            jQuery(this).animate(
                {'marginTop' : "-950px"}, 5000 );
        });
    });

    $("#image1").click(function ( event ) {
        $(this).hide();
        $("#image2").show();
    });
    $("#image2").click(function ( event ) {
        $(this).hide();
        $("#image3").show();
    });
    $("#image3").click(function ( event ) {
        $(this).hide();
        $("#image4").show();
    });
    $("#image4").click(function ( event ) {
        $(this).hide();
        $("#image1").show();
    });


    function animateCarter() {
        function phase1() {
            var carter = $('#carter-chair');

            carter.animate({'right' : '1650px'}, 7000, 'linear', phase2);
        }
        function phase2() {
            $('#carter-chair').css({'right' : '-620px'});
            setTimeout(phase1,180000);
        }
        phase1();
    }

    setTimeout(animateCarter, 6000);

</script>

<script type="text/javascript">
    function animateImage(Index) {
        $("img.animated:eq(" + Index + ")").each(function (Index) {
            if (!$(this).is(":visible")) {
                var intervalID = parseInt($(this).attr("intervalid"));
                clearInterval(intervalID);
            } else {
                var max = parseInt($(this).attr("max"));
                var pattern = $(this).attr("pattern");
                var index = parseInt($(this).attr("index"));
                index++;
                if (index > max) {
                    index = 1;
                }
                var src = pattern.replace("#", index);
                $(this).attr("index", index);
                $(this).attr("src", src);
            }
        });
    }

    function initialiseAnimatedImages() {
        $("img.animated").each(function (Index) {
            var interval = $(this).attr("interval");
            $(this).attr("index", "0");
            var intervalID = setInterval(function () { animateImage(Index); }, interval);
            $(this).attr("intervalid", intervalID);
        });
    }

    $(document).ready(function () {
        initialiseAnimatedImages();
    });
</script>

</body>
</html>
