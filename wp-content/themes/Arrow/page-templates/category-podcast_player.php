<?php
/**
 * Template Name: Podcast Player
 * Displays the PodcastPlayer widget along with the description of the podcast to be played.
 *
 * *Note Requires the On Demand Plugin and Show all stations widget to run.
 *
 * @package WordPress
 * @subpackage Unity
 * @since Unity 1.0
 */
?>
<?php
//CSS Podcast Style Sheet - within Assets
wp_enqueue_style('podcasts');
get_header();

function cleanit($showname){
    $showname = str_replace("'","",$showname);
    $showname = preg_replace("/[^a-zA-Z0-9]+/", "_", $showname);
    return $showname;
}

/***************************************
//ON-DEMAND-AUDIO-TOOLS-PLUGIN REQUIRED
****************************************/

include_once ( ABSPATH . 'wp-admin/includes/plugin.php');
//include_once ( ABSPATH . 'wp-content/themes/mynorthwest/resources/podcast_sharebar.php');
if( is_plugin_active( 'on-demand-audio-tools/ondemand.php') ) {
    include_once ( WP_PLUGIN_DIR . '/on-demand-audio-tools/lib/AudioLibraryREST.php');
    include_once ( WP_PLUGIN_DIR . '/on-demand-audio-tools/lib/OnDemand.php');
}
    $Audio = new OnDemand\AudioLibraryREST();
?>
<div id="wrapper">
    <div id="mainbody">
    <?php bucket("sponsor_ad"); ?>
        <article itemscope itemtype="http://schema.org/Article" class="pad">
        <?php bucket("alerts"); ?>
            <header class="article_header">
                <h4 class="sectiontopper">Enjoy your favorite shows on your schedule</h4>
                <h1 itemprop="name headline" class="sectiontitle">Podcasts</h1>
                <div class="mobilemenu">
                  <ul>
                    <li id="mobilemenu"><a href="#">☰&nbsp; menu</a></li>
                  </ul>
                </div>
                <div class="shownavigation" style="padding-bottom:0px;">
                    <?php wp_nav_menu( //Adding The Navigation Menu located at the top of the Category.
                        array(
                            'container_class' => 'shownavigation',
                            'theme_location' => 'podcasts-nav'
                        )
                    ); ?>
                </div>
                <?php
                if (apply_filters('section_header', false, 0)) : ?>
                <?php get_template_part('section_header'); ?>
                <?php endif; ?>
                <?php bucket("section_header");?>
            </header>
            <div id="article">
            <div id="copy_container">
                <h4 style-"margin-bottom:5px;">
                <?php
                $showid = get_query_var('sid', false);
                $showname = stripslashes(get_query_var('n', false));
                $aid = get_query_var('a', false);
                $instance = "a={$aid}";
                $pod = $Audio->getPodById($aid);
                $relUrl = "http://" . $_SERVER['HTTP_HOST'] . "/category/podcast_results/?sid={$showid}&n={$showname}";
                $filename = $pod['filename'];
                $title = $pod['title'];

                    ?>
                    <a href="<?php echo $relUrl; ?>"> &lt; <?php  echo $showname; ?> </a>
                </h4>
                <?php
                $dateFormatOut = "l, F d, Y";
                $date_published = date($dateFormatOut, strtotime($pod['datepublished']));
                $fulldescription = $pod['fulldescription'];
                $duration = $pod['duration'];
                $runtime = $pod['runtime'];
                ?>
            </div>
            <?php
                $widgetName = "PodcastPlayer";
                the_widget( $widgetName, $instance);
            ?>
            <div id="subplay_wrapper">
                <div id="subplaycontent">
                    <a href="<?php echo $relUrl; ?>" style="font-weight:600;font-size:13px;"><?php echo(strtoupper($showname)); ?></a>
                    <h1><?php echo($title); ?></h1>
                    <?php echo($fulldescription); ?>
                    <p>
                    <span class="runtime">Published: <?php echo($date_published); ?> 
                    <?php if($duration>0){ ?>
                    &nbsp; | &nbsp; Runtime: <?php echo($runtime); ?>
                    <?php } ?>
                    </span>
                    <p />
                    <div class="share" style="padding-bottom: 20px;">
                        <?php if (comments_open()) :  ?>
                        <ul class="article_comments">
                            <li id="comments"><a href="#commentsbox"><i class="fa fa-comment fonticon"></i> <span>Comments</span></a></li>
                        </ul>
                        <?php endif; ?>
                        <?php comments_template(); ?>
                        <?php
                            the_podcast_sharebar($filename);
                        ?>
                    </div>
                </div>
            </div>
            </div>
            <?php get_template_part( 'partials/column', 'right' ); ?>
        </article>
</div>
</div>


<div id="bottom_mobile_heads">
    <?php bucket("left_head_nav"); ?>
</div>
<?php bucket("more_headlines"); ?>
<?php get_footer("podcasts"); ?>
<?php get_footer(); ?>
