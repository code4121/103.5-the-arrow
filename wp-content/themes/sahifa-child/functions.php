<?php
/**
* Enqueues child theme stylesheet, loading first the parent theme stylesheet.
**/
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://metabox.io/docs/registering-meta-boxes/
 */

add_filter( 'rwmb_meta_boxes', 'arrow_register_meta_boxes' );

/**
 * Register meta boxes
 *
 * Remember to change "your_prefix" to actual prefix in your project
 *
 * @param array $meta_boxes List of meta boxes
 *
 * @return array
 */
function arrow_register_meta_boxes( $meta_boxes )
{
	/**
	 * prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = '_arrow_';

	// 1st meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'standard',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Special Contest Rules', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		//'post_types' => array( 'post' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}contest_rules",
				'type' => 'file',
			)
		)
	);
	
	/*$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'homepage_rotator',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Homepage Rotator', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'post','page','tribe_events' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}rotator",
				'type' => 'file',
			)
		)
	);*/

	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'homepage_rotator',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Homepage Rotator', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'post','page','tribe_events' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}rotatorlink",
				'type' => 'text',
				'desc' => 'Rotator Link',
			),
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}rotatorexpire",
				'type' => 'datetime',
				'desc' => 'Rotator Expiration',
			),
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}rotator",
				'type' => 'file',
				'desc' => 'Upload Rotator',
			)
		)
	);
	
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'passport_link',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Passport Link', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'passport' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "fm100_passport_link", //at some point this needs to be revisited to show arrow not fm100. due to time restricitons this was brought over as is as a 1-1 copy.
				'type' => 'text',
				'desc' => 'Link to passport partner'
			)
		)
	);	
	
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'content_background',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Content Area Background', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'passport','post','page' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}content_background",
				'type' => 'file',
				'desc' => 'Upload Content Background (width 695px)',
			)
		)
	);
	
	/*$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'homepage_rotator_expire',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => __( 'Homepage Rotator Expire', 'meta-box' ),

		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'post','page','tribe_events' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'side',

		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',

		// Auto save: true, false (default). Optional.
		'autosave'   => true,

		// List of meta fields
		'fields'     => array(
			// FILE UPLOAD
			array(
				'name' => __( '', 'meta-box' ),
				'id'   => "{$prefix}rotatorexpire",
				'type' => 'datetime',
			)
		)
	);*/
	//datetime
	return $meta_boxes;
}
/**
 * Register Listen-Live-Menu 
 * 
 */ 
add_action( 'init', 'register_listen_live_menu' );
function register_listen_live_menu() {
	register_nav_menus(
		array(
			'listen-live-menu' => __( 'Listen-Live Location' )
		)
	);
}
/**
 * Register Shortcode for listen-live-location
 */
add_shortcode('add_listen_live_menu', 'listen_live_menu_shortcode');
function listen_live_location_shortcode(){
	$value = "<?php wp_nav_menu( array( 'theme_location' => 'listen-live-menu' ) ); ?>";
	return $value;
}

function add_my_rss_node() {
	global $post;
	if(has_post_thumbnail($post->ID)):
		$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID));
		echo('<media:content url="' . $thumbnail[0] . '" medium="image"/>');
		echo('<appimage>' . $thumbnail[0] . '</appimage>');
		echo('<appdescription><![CDATA[' . utf8_for_xml(html_entity_decode(get_the_excerpt($post->post_ID))) . ']]></appdescription>');
	endif;
}
add_action('rss2_item', 'add_my_rss_node');

/*remove invalid-for-xml utf8 strings*/
function utf8_for_xml($string)
{
	return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
}


add_filter( 'acm_ad_tag_ids', 'filter_ad_tag_ids_add_footerboard' );
function filter_ad_tag_ids_add_footerboard( $ids ) {

	$ids[] = array(
		'tag'       => '728x90_footerboard',
		'url_vars'  => array(
			'tag'       => '728x90_footerboard',
			'width'     => '728',
			'height'    => '90',
		),
		'enable_ui_mapping' => true,
	);

	return $ids;
}

add_filter( 'acm_ad_tag_ids', 'filter_ad_tag_ids_add_leaderboard' );
function filter_ad_tag_ids_add_leaderboard( $ids ) {

	$ids[] = array(
		'tag'       => '728x90_leaderboard',
		'url_vars'  => array(
			'tag'       => '728x90_leaderboard',
			'width'     => '728',
			'height'    => '90',
		),
		'enable_ui_mapping' => true,
	);

	return $ids;
}

add_filter( 'acm_ad_tag_ids', 'filter_ad_tag_ids_add_block_atf' );
function filter_ad_tag_ids_add_block_atf( $ids ) {

	$ids[] = array(
		'tag'       => '300x250_ATF',
		'url_vars'  => array(
			'tag'       => '300x250_ATF',
			'width'     => '300',
			'height'    => '250',
		),
		'enable_ui_mapping' => true,
	);

	return $ids;
}

add_filter( 'acm_ad_tag_ids', 'filter_ad_tag_ids_add_block_btf' );
function filter_ad_tag_ids_add_block_btf( $ids ) {

	$ids[] = array(
		'tag'       => '300x250_BTF',
		'url_vars'  => array(
			'tag'       => '300x250_BTF',
			'width'     => '300',
			'height'    => '250',
		),
		'enable_ui_mapping' => true,
	);

	return $ids;
}

add_filter( 'acm_ad_tag_ids', 'filter_ad_tag_ids_add_300x600' );
function filter_ad_tag_ids_add_300x600( $ids ) {

	$ids[] = array(
		'tag'       => '300x600_monster',
		'url_vars'  => array(
			'tag'       => '300x600_monster',
			'height'    => '600',
			'width'     => '300',
		),
		'enable_ui_mapping' => true,
	);

	return $ids;
}

add_filter( 'query_vars', 'add_query_vars_filter' );
function add_query_vars_filter( $vars ) {
  $vars[] = "sid";
  $vars[] = "n";
  $vars[] = "page";
  return $vars;
}
function customPodcastShowNavMenuOverride($atts, $item, $args) {

    $attsStr = print_r($atts, true);
    //error_log("GOT Atts {$attsStr}");

    $itemStr = print_r($item, true);
    //error_log("GOT item {$itemStr}");

    $argsStr = print_r($args, true);
    //error_log("ARGS VARIABLE: ** {$argsStr}");

    $catItem = $item->post_title;

    if (strtolower($catItem) == 'subscribe via itunes') {
      $catid = get_query_var('cat'); //Current Category
      $category = get_category($catid);

      $audioLib = new onDemand\AudioLibraryREST();
      $showId = onDemand\OnDemand::singleton()->showIdForCategory($catid);
      $show = $audioLib->getShowFromId($showId);

      if( isset( $show['itunesurl'] ) ){
        $atts['href'] = $show['itunesurl'];
      }
    }

    if (strtolower($catItem) == 'subscribe via rss') {
      $atts['href'] .= "?s=" . get_query_var('sid');
    }

    return $atts;
}

function addPodcastSearchBar($items, $args) {
    if ($_GET['showsearch']) {
      if (strpos($args->theme_location, 'podcasts') !== FALSE) {
          $items .= '<li class="audioSearch"><div><span class="searchTitle">Search:</span><input type="text" autocomplete="off" id="podSearch" name="podSearch" onkeyup="podcast_lookup(this.value);" class="req" /></div>';
          $items .= '<ul id="elexlist_ajax"></ul></li>';
      }
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'addPodcastSearchBar', 10, 3);

/**
 * Registers the names of Nav Menus
 */
function register_menus() {
  register_nav_menus(
    array(
      'podcasts-nav' => __( 'Podcasts Nav Menu' ),
      'podcast-results-nav' => __( 'Podcast Show Nav Menu'),
    )
  );
  add_filter( 'nav_menu_link_attributes', 'customPodcastShowNavMenuOverride', 'podcast-results-nav',  10, 3);
}
add_action( 'init', 'register_menus');

