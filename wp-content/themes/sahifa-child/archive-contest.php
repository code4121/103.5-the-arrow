<?php
/**
 * The Contest Page Template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */
get_header();

?>

<style>
ul.rig {
	list-style: none;
	font-size: 0px;
	margin-left: -2.5%; /* should match li left margin */
}
ul.rig li {
	display: inline-block;
	padding: 10px;
	margin: 0 0 2.5% 2.5%;
	background: gray;
	font-size: x-small;
	
	vertical-align: top;
	box-shadow: 0 0 20px black; 
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}
ul.rig li img {
	max-width: 100%;
	height: auto;
	margin: 0 0 10px;
}
ul.rig li h3 {
	margin: 0 0 5px;
	 
}
ul.rig li p {
	font-size: .9em!important;
	line-height: 1.5em;
	color: #999;
}
/* class for 2 columns */
ul.rig.columns-2 li {
	width: 47.5%; /* this value + 2.5 should = 50% */
}
/* class for 3 columns */
ul.rig.columns-3 li {
	width: 30.83%; /* this value + 2.5 should = 33% */
}
/* class for 4 columns */
ul.rig.columns-4 li {
	width: 22.5%; /* this value + 2.5 should = 25% */
}
 
@media (max-width: 480px) {
	ul.grid-nav li {
		display: block;
		margin: 0 0 5px;
	}
	ul.grid-nav li a {
		display: block;
	}
	ul.rig {
		margin-left: 0;
	}
	ul.rig li {
		width: 100% !important; /* over-ride all li styles */
		margin: 0 0 20px;
	}
}

ul.rig li h3 {
  display: block; /* Fallback for non-webkit */
  display: -webkit-box;
  max-width: 400px;
  margin: 0 auto;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;

}
ul.rig li a {
    color: #fdc583!important;
    }
</style>

	<div class="content">
	<article class="post-listing post post-797 page type-page status-publish  fancy-gallery-content-unit" id="the-post">
		<div class="post-inner">
			<div class="entry">
			
			<h3 class="name post-title entry-title">On Air / Web Contests</h3><br>
		<?php if ( have_posts() ) : ?>
			<ul class="rig columns-3"> 

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<li >	
				 		<!--thumbnail -->
				 	<a href="<?php echo get_permalink($post->ID);?>">
					<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						
						<?php the_post_thumbnail( 'extra-large' ); ?>
					<?php else: ?>
						<img width="300" height="250" src="<?php echo do_shortcode('[default_contest_logo_url]');?>" alt="Default Contest Image"/>
					<?php endif; ?>
					</a>
					 	<!--Title -->
					 <a href="<?php echo get_permalink($post->ID);?>"> <h3><?php echo get_the_title(); ?></h3> </a>
					
				</li>
			<?php endwhile; ?>
 			</ul>

		<?php else : ?>
			<?php echo "There are currently no on air/web contests, please check back soon!"; ?>
		<?php endif; ?>
		<hr \>
		<!--Social Media/Aptivada Embed -->
		<h3 class="name post-title entry-title">Social Media Contests</h3><br>
		<div id="aptivada_app" data-app-id="165976433" data-app-type="contest_grid" data-mobile-redirect="0" data-height="800" style="background:gray url(https://cdn2.aptivada.com/images/iframeLoader.gif) no-repeat center; min-height:500px;"></div>
			<script src="//www.aptivada.com/js/all.js"></script>
		<!-- END Social Media / Aptivada Embed -->
        

			<!-- end entry -->        
        	</div>

        <!-- end post-inner -->
        </div>
<style>
        div#aptivada_app #aptivada_iframe .html .body{
            background-color: gold!important;
        }
        </style>
    </article>
    
    <!-- end content -->
    </div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>