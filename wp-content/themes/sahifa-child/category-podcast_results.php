<?php
/**
 * Template Name: Podcast Results
 * A list of pods for a given podcast show.  They Grouped and ordered according to their date publishd.
 *
 * Important Note* This template requires the activation of the On-Demand-Audio-Tools plugin to function.  It
 * is included below.
 *
 * @package WordPress
 * @subpackage Unity
 * @since Unity 1.0
 */


?>
<?php
//CSS Podcast Style Sheet - within Assets
wp_enqueue_style('podcasts');
get_header();

/***************************************
//ON-DEMAND-AUDIO-TOOLS-PLUGIN REQUIRED
****************************************/

include_once ( ABSPATH . 'wp-admin/includes/plugin.php');
if( is_plugin_active( 'on-demand-audio-tools/ondemand.php') ) {
    include_once ( WP_PLUGIN_DIR . '/on-demand-audio-tools/lib/AudioLibraryREST.php');
    include_once ( WP_PLUGIN_DIR . '/on-demand-audio-tools/lib/OnDemand.php');
}
    $Audio = new OnDemand\AudioLibraryREST();
?>
<style>
.stationname{
    font-weight: bold;
    background: #eee;
    padding: 10px !important;
}
.titlelink{
font-size:21px;
font-family:'Roboto';
line-height: 110%;
color:#fff;
font-weight: bold;
}
.titlelink:hover{
color:red;
}

.mainListShow{
    font-size: 14px;
    overflow: hidden;
    padding: 20px 0;
    margin-bottom: 0;
    border-bottom: 1px solid #eee;
    max-width: none !important;
}
.mainListShow img{
    float:left;
    border: none;
}
.mainListShow .nugget{
    margin-left: 220px;
}
.mainListShow p {
    padding-top:5px;
}
.underBannerBar {
    background: #2d2d2d;
    box-shadow: inset -1px -5px 0px -1px #393939;
    height:100px;
    padding:10px;
}
.sectiontitle {
    color: #fff;
}
@media (max-width: 600px) {
    .mainListShow .nugget{
        margin-left: 0px !important;
    }
    .mainListShow img{
        width:150px;
    }
}

</style>
<div id="wrapper">
    <div id="mainbody">
    <?php //bucket("sponsor_ad"); ?>
    <article itemscope itemtype="http://schema.org/Article" class="pad">
    <?php //bucket("alerts"); ?>
        <header class="article_header">
            <div class="underBannerBar">
                <h4><a href="/category/podcasts/">Podcasts</a></h4>
            
            <?php



            $showid = get_query_var('sid', false);
            $page = get_query_var('page', 0) + 0;

            $pods = $Audio->getShowPodcasts($showid, $page);
            $show = $Audio->getShowFromId($showid);

            $id = $show['showid'];
            $showname = (stripslashes($show['showname']));
            
            $podimage = $show['podimage'];

            $categoryid = $show['categoryid'];
            $categoryname = (stripslashes($show['categoryname']));
            $description = (stripslashes($show['showdescription']));
            $stationid = $show['stationid'];
            $stationname = (stripslashes($show['stationname']));
            
            $rssimport = $show['rssimport'];
            $itunesurl = $show['itunesurl'];

            ?>
            <h1 style="margin:10px;"class="sectiontitle"><?php echo $showname ?></h1>
            </div>
            <!--
            <div class="mobilemenu">
              <ul>
                <li id="mobilemenu"><a href="#">☰&nbsp; menu</a></li>
              </ul>
            </div>
            -->
            <?php
            //Navigation Menu - located at the top of the Show Category.
            /*wp_nav_menu(
                array(
                    'container_class' => 'shownavigation',
                    'theme_location' => 'podcast-results-nav'
                )
            ); */?>

            <?php

            if (apply_filters('section_header', false, 0)) : ?>
            <?php get_template_part('section_header'); ?>
            <?php endif; ?>
            <?php //bucket("section_header");?>

        </header>

        <div id="copy_container">
            <div id="article">
            <div id="podcast_wrapper" style="overflow:hidden;">
                <br />
                <div id="podcast_content">
                    <div id="uppercontent">
                        <img src="<?php echo($podimage); ?>" class="podimage">
                        <h1 style="padding:20px;"><?php echo($showname); ?></h1>
                        <?php echo($description); ?>
                        <p />
                        <div class="subscribe_show" style="padding:10px;">
                            <a href="<?php echo($itunesurl); ?>" target="new"><i class="fa fa-apple" aria-hidden="true"></i> iTunes</a> &nbsp; &middot; &nbsp; 
                            <a href="<?php echo($rssimport); ?>" target="new"><i class="fa fa-rss" aria-hidden="true"></i> RSS</a>
                        </div>
                        <p />
                    </div>
                </div>
                <div id="episode_wrapper">
                    <div id="episodes">
                        <h2 class="stationname" style="color:#000;">Episodes: </h2>
                        <ol class="podresults">
                        <?php 
                        foreach ($pods as $podsDateGroup) { 
                            foreach ($podsDateGroup['pods'] as $row) {
                                $count++;
                                $aurl = $pod['filename'];   
                                $audio_id = $row['audioid'];
                                $buildurl = "http://" . $_SERVER['HTTP_HOST'] . "/category/podcast_player?a={$audio_id}&sid={$showid}&n=" . urlencode($showname);
                                $date_published = date("n-j-Y",strtotime($row['datepublished']));
                                $category_id = $row['categoryid'];
                                $showid = $row['showid'];
                                $title = (stripslashes($row['title']));
                                $shortdescription = (stripslashes($row['description']));
                                $fulldescription = (stripslashes($row['fulldescription']));
                                $metadata = (stripslashes($row['metadata']));
                                if($row['image']){
                                    $image = $row['image'];
                                }else{
                                    $image = $podimage;
                                }
                                if(strlen($shortdescription)>280){
                                    $shortdescription = substr(($shortdescription),0,280).'...';
                                }
                                $duration = $row['duration'];
                                $runtime = $row['runtime'];
                                $showhour = $row['hour'];
                                
                                if($duration>0){
                                    $runtime = ' &nbsp; ('. $runtime .')';
                                }else{
                                    $runtime = '';
                                } ?>

                                <li class="mainListShow">
                                    <a href="<?php echo $buildurl ?>"> 
                                        <div href="<?php echo $buildurl ?>">  
                                            <ul>
                                                <li> <p class="titlelink" style="float:left; padding:5px"> <?php echo $title ?> </p> </li>
                                                <li> <p style="float:right; padding:5px"> <?php echo $date_published ?> </p> </li>
                                                <li> <p style="float:right; padding: 8px"> <?php echo $shortdescription ?> </p> </li>
                                            </ul>
                                        </div>
                                    </a> 
                                </li>
                                <?php
                                //echo ('<tr class="clickable-row" data-url="'.$buildurl.'"><td>'. $title . $runtime .'</td><!--<td><a href="#" id="info" value="'.$audio_id.'"><i class="fa fa-info-circle" aria-hidden="true"></i><div class="infobox">'.$shortdescription.'</div></a></td>--><td class="datepublished" nowrap align=right>'.$date_published.'</td></tr><tr class="clickable-row" data-url="'.$buildurl.'"><td colspan=2 class="td_description">'.$shortdescription.'</td></tr>');
                            }
                        }
                        ?>
                        </ol>
                    </div>
                </div>
            </div>

            <div style="padding:10px;text-align:center;">
                <?php
                    //Paging
                    $next_page = $page +1;
                    //error_log("count is :: **** __  "  . $count);
                    if( $count >= 20){
                ?>
                    <a href="<?php echo "http://" . $_SERVER['HTTP_HOST'] . "/category/podcast_results/?sid={$showid}&n={$showname}&page={$next_page}";?>" style="font-size:14pt;">
                        <b>More shows »</b>
                    </a>
                <?php
                    }
                ?>
            </div>
            </div>
            <?php get_template_part( 'partials/column', 'right' ); ?>
        </div>

        <div id="article_footer">
            <?php //bucket("also_topstories"); ?>
            <?php //bucket("footer_ads"); ?>
        </div>
    </article>
    </div>
</div>

<div id="bottom_mobile_heads">
<?php //bucket("left_head_nav"); ?>
</div>
<?php //bucket("more_headlines"); ?>
<?php get_footer('podcasts'); ?>
<?php get_footer(); ?>
