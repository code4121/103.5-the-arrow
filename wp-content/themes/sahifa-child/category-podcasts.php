<?php
/**
 * Template Name: Podcasts
 * Displays the list of podcast shows that are sorted by radio station .
 *
 * @package WordPress
 * @subpackage Unity
 * @since Unity 1.0
 */
?>
<?php
//CSS Podcast Style Sheet - within Assets
wp_enqueue_style('podcasts');
get_header();
//require_once(ABSPATH . '/wp-content/themes/arizonasports/resources/podcast_search_box.php');

/***************************************
//ON-DEMAND-AUDIO-TOOLS-PLUGIN REQUIRED
****************************************/
?>
<div id="wrapper">
    <div id="mainbody">
    <?php //("sponsor_ad"); ?>
    <article itemscope itemtype="http://schema.org/Article" class="pad">
    <?php //("alerts"); ?>
        <header class="article_header">
            <div class=underBannerBar >

                <h4 class="sectiontopper">Enjoy your favorite shows, interviews and highlights on your schedule</h4>
                <h1 itemprop="name headline" style="margin:10px; "class="sectiontitle">Podcasts/On-Demand</h1>
             </div>
            <!--
            <div class="mobilemenu">
            	<ul>
            		<li id="mobilemenu"><a href="#">☰&nbsp; menu</a></li>
            	</ul>
            </div>
            -->


            <?php
            if (apply_filters('section_header', false, 0)) : ?>
            <?php //get_template_part('section_header'); ?>
            <?php endif; ?>
            <?php //("section_header");?>

        </header>

        <div id="copy_container">
            <div id="article">
            <div id="podcast_wrapper" style="overflow:hidden;">
                <br />
                <div id="podcast_content">
                    <ul class="allshows">
                        <?php the_widget('onDemand\AllStationsShowList'); ?>
                    </ul> 
                </div>
            </div><br><br>

            <div class="queue" style="margin-top:0;">
                <ul>
                <?php while ( have_posts() ) : the_post(); ?>
                    <li>
                        <?php// get_template_part('headline'); ?>
                    </li>
                <?php endwhile; ?>
                </ul>
            </div>
            </div>
            <?php //get_template_part( 'partials/column', 'right' ); ?>
        </div>

        <div id="article_footer">
            <?php //("also_topstories"); ?>
            <?php //("footer_ads"); ?>
        </div>
    </article>
    </div>
</div>

<div id="bottom_mobile_heads">
<?php //("left_head_nav"); ?>
</div>
<?php //("more_headlines"); ?>
<?php get_footer('podcasts'); ?>
<?php get_footer(); ?>
