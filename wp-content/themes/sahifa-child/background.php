<?php
/*
Template Name: Background Image
*/
get_header(); 
//wolf_page_before();

$meta = get_post_meta(get_the_ID());

$background_url = wp_get_attachment_url($meta['_arrow_content_background'][0]);


?>

	<div id="primary" class="content-area" style="background:url('<?php echo $background_url; ?>') top center no-repeat; min-height:770px;" >
		<div id="content" role="main" style="margin-top:375px;">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>><!--<h1><?php echo get_the_title(); ?></h1>-->
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
							<div class="entry-thumbnail">
								<?php the_post_thumbnail( 'extra-large' ); ?>
							</div>
						<?php endif; ?>
					</header>
					
					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'wolf' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'wolf' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				<?php // comments_template(); ?>
			<?php endwhile; ?>
			<?php if($_SERVER['REQUEST_URI'] == "/events/category/community/"){ ?>
			
				<h3>Submit Your Event</h3>
				<?php echo do_shortcode('[gravityform id="11" name="Community Calendar Submission" title="false" description="false"]'); ?>
			
			<?php } ?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php

get_sidebar();
//wolf_page_after(); 
get_footer(); 
?>