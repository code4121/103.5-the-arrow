<link rel='stylesheet' id='unity-font-awesome-css'  href='http://arizonasports.com/wp-content/themes/arizonasports/assets/font-awesome-4.2.0/css/font-awesome.min.css?ver=20140822' type='text/css' media='all' />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<script>

$(document).ready(function(){
$( "#station" ).change(function() {
        location.href = '?s='+jQuery(this).val();
    });
$(".clickable-row").click(function() {
        window.document.location = $(this).data('url');
    });
    
$("a#menu_link").click(function(){
    $('#menu').toggle("slide", { direction: "up" }, 300);
    return false;
});

$("a#subscribe").click(function(){
    var v = $(this).attr('value');
    $('#'+v).show("slide", { direction: "down" }, 300);
    return false;
});

$("a#close").click(function(){
    var v = $(this).attr('value');
    $('#'+v).hide("slide", { direction: "down" }, 300);
    return false;
});

$("a#categories_link").click(function(){
    $('#categories').show("slide", { direction: "up" }, 300);
    return false;
});


$( "a#category_select" ).click(function() {
        location.href = '?c='+$(this).attr('value')+'&s=<?php echo($station); ?>';
    });

$("a#stations_link").click(function(){
    $('#stations').show("slide", { direction: "down" }, 300);
    return false;
});


$( "a#station_select" ).click(function() {
        location.href = '?s='+$(this).attr('value');
    });

    
});

</script>


<style>
body, #uppercontent {margin:0;padding:0;}
/*a { color:#1651a8; text-decoration:none;}*/

.seattle { background:#c00; }
.slc { background:#000; }

h1 {font-size: 2em;}
h2 {display:block;}
.white {color:#fff;}
.grey {color:#ccc;}
.large {font-size:45px;}
.gold {color:#fdc583;}


#header{margin:0 auto;background:#fff;opacity:0.95;width:100%;position:fixed;border-bottom:5px solid #ddd;font-size:19px;font-weight:bold;margin-bottom:20px;color:#666;letter-spacing:4px;z-index:9999;height:85px;text-align:center;}
#header a {font-family:'exo';text-transform:uppercase; color:#666; font-weight:800;}
#header img {max-height:50px; margin:0; padding:14px; margin-left:80px;}
#header_wrapper {text-align:left;margin:0 auto;}

#header .menulink {float:left;height:100%;width:80px; text-align:center;border-right:1px solid #ccc; position:absolute; left:0; top:0;}
#header .sociallinks {float:right;padding:20px;}

#footer{width:auto;padding:0;font-size:13px;font-weight:bold;margin:0 auto; margin-bottom:20px;color:#666; text-align:center;}
#footer ul {list-style-type:none; margin:0 auto; width:auto; padding:0; overflow:hidden; text-align:center;}
#footer ul li {display:inline-block;padding:20px;}
#footer ul li a {display:block;}


/**
* Pod Grid
**/
#podcast_wrapper{overflow:hidden; margin:0 auto; width:100%;}

#uppercontent {margin:0 auto;text-align:center; max-width: 980px; padding:0 50px; /*padding-top:95px;*/ position:relative; font-size:120%;}


#podcast_content {margin:0 auto;text-align:center;padding-bottom:50px;}

.inactive {background:#c00;color:#fff !important;}
.activelink {display:block; padding:10px; background: #c00; color:#fff; text-decoration:none; border-radius: 5px;  text-align:center;}
.inactivelink {display:block; padding:10px; background: #259712; color:#fff; text-decoration:none; border-radius: 5px;  text-align:center;}

/**
* Podcast 
**/
#podcast_content ul.allshows{list-style-type:none;margin:0 auto;padding:0;max-width: 980px; }
#podcast_content ul.allshows li{position:relative;background:#a00303;border:1px solid #ccc; margin:10px; padding:0;overflow:hidden;font-size:15px;font-weight:700; display: inline-block; height:310px; width:270px; overflow:hidden; text-align:center;}
#podcast_content ul.allshows li img{width:240px;height:240px;border:0px solid #ccc; margin-bottom:5px; margin-top:5px;}
#podcast_content ul.allshows li a{color:#000;}
#podcast_content ul.allshows li .subscribe_box{position:absolute;top:0;left:0;display:none;z-index:999;background:#ddd; padding:40px 20px 0px 20px;overflow:hidden;font-size:15px;font-weight:700; height:290px; width:250px; text-align:center;}
#podcast_content ul.allshows li .subscribe_box a{background:#333;padding:10px;display:block;text-decoration:none;color:#fff;}
#podcast_content ul.allshows li .closebox{width:30px;border-radius:5px;position:absolute;bottom:10px;right:10px;}
#podcast_content ul.allshows li img.clipimage{width:240px;height:160px;border:0px solid #ccc; margin-bottom:10px; margin-top:10px;}
#podcast_content ul.allshows li .description{font-size:12px;margin-top:10px;color:#666;}
#podcast_content ul.allshows li .datepublished{font-size:12px;margin-bottom:10px;color:#666;}

#podcast_content ul.allclips{list-style-type:none;margin:0 auto;padding:0;max-width: 1010px; }
#podcast_content ul.allclips li{position:relative;background:#fff;border:1px solid #ccc; margin:10px; padding:10px;overflow:hidden;font-size:15px;font-weight:700; display: inline-block; height:310px; width:260px; overflow:hidden; text-align:left;}
#podcast_content ul.allclips li img{width:240px;height:240px;border:0px solid #ccc; margin-bottom:10px;}
#podcast_content ul.allclips li a{font-family:'helvetica',sans-serif; font-size:17px; font-weight:700;}
#podcast_content ul.allclips li img.clipimage{width:270px;height:170px;border:0px solid ##fdc583; margin-bottom:10px;}
#podcast_content ul.allclips li .description{font-size:12px;margin-top:10px;color:#666;}
#podcast_content ul.allclips li .datepublished{font-size:12px;margin-bottom:10px;color:#666;}
#podcast_content ul.allclips li a.smlink{font-size:12px !important; margin-bottom:10px; color:#666;}

#podcast_content ul.allshowsnav{list-style-type:none;margin:0 auto;padding:0;width: 270px; position:relative; bottom:0;}
#podcast_content ul.allshowsnav li{background:#000;border:1px solid #888; margin:5px; padding:0px;overflow:hidden;font-size:11px;font-weight:500; display: inline-block; height:auto; width:44%; overflow:hidden; text-align:center;}
#podcast_content ul.allshowsnav li a{color:#fff; display:block; padding:7px; text-decoration:none; text-transform:uppercase;}

.subscribe_show a{color:#fdc583; text-decoration:none; font-size:13px;}





.screencover {position:fixed;top:90px;left:0;width:100%;height:100%;background:#fff;z-index:99999;display:none;}
.screencover ul {list-style-type:none;margin:0 auto;padding:0;}
.screencover ul li a{display:block;font-size:40px;padding:20px; border-bottom:1px solid #f5f5f5;}
.screencover ul li:hover{background:#f5f5f5;}


.closebig {display:none;width:auto;border-radius:5px;position:absolute;top:10px;right:10px;}
.closebig a {background:#fff;border:1px solid #666;padding:10px;display:block;text-decoration:none;color:#000;font-size:20px;}


.podimage {width:200px;height:200px;border:1px solid #ccc;margin-top:10px;}

#episode_wrapper{overflow:hidden; margin:0 auto;width:100%;border-top:1px solid #333; padding-top:20px;}
#episodes {margin:0 auto;}
#episodes a {color:#c4c4c4;}
#episodes table{list-style-type:none;margin:0 auto;padding:0;max-width: 1010px; border-spacing:0;position:relative;}
#episodes table tr{cursor: pointer;}

#episodes table td {padding:15px 15px 5px 15px; margin:0; font-size:17px; font-weight:600;}
#episodes table td.td_description{padding:10px 15px 15px 15px; border-bottom:1px solid #ccc;  font-size:13px; font-weight:400;}
/*#episodes .infobox {width:300px;position:absolute;right:50px;background:#f5f5f5;border:1px solid #ccc;padding:20px;display:none;}*/

#play_wrapper{overflow:hidden; margin:0 auto; background:#eee; width:100%;border-top:1px solid #333; padding-top:40px;}
#play {margin:0 auto;text-align:center;}

#PodcastPlayer {width:560px !important;}
#subplay_wrapper {padding-top:10px;overflow:hidden; margin:0 auto; background:#eee; width:100%;}
#subplaycontent {margin:0 auto;text-align:center; max-width: 1010px; padding:0 50px; position:relative; font-size:120%;}

.runtime {font-weight:600; font-size: 0.9em;}

.datenav{text-align:left;}
.datenav li.left, .datenav li.right {width: 47% !important;}


@media only screen and (max-width: 768px) {
#header{padding: 0; height:60px;}
h2 {padding:5px; margin:5px; }
#header img {max-height:40px;}

.large {font-size:35px;}

#header img { padding:8px;  margin-left:60px;}

#header .menulink {width:60px;}
#header .sociallinks {padding:10px;}


#uppercontent {padding-top:80px;}


.screencover {top:65px;}

#podcast_content {margin:0 auto;width:100%;}
#podcast_content ul.allshows li{margin-bottom:20px; width:80%;}
#podcast_content ul.allshowsnav{width:90%;}
#podcast_content ul.allshowsnav li{margin-bottom:10px;width:45%;}
#podcast_content ul.allshows li .subscribe_box{height:310px; width:90%; }


#episodes table td {font-size:14px;}
#episodes table td.td_description{font-size:13px;}
#episodes table td.datepublished{font-size:11px;width:70px;}

.screencover ul li a{font-size:30px;padding:20px;}

.left_art{display:none;}
.right_form{width:90%;float:none;}
h1{font-size:27px;}
.runtime {font-size:12px;}

#PodcastPlayer {width:100% !important;}
#play_wrapper{padding-top:65px;}
}
 .lazy-enabled #main-content img { 
    opacity:1!important;
 }
.underBannerBar {
    background: #2d2d2d;
    box-shadow: inset -1px -5px 0px -1px #393939;
    height:100px;
    padding:10px;
}
.sectiontitle {
    color: #fff;
}
</style>